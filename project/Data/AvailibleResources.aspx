﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AvailibleResources.aspx.cs" Inherits="FinFinancialPlan.AvailibleResources" MasterPageFile="~/Planmasterpage.Master" %>

<asp:Content ID="cover" ContentPlaceHolderID="masterpagecontent" runat="server">

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico" />

        <title>Fincart</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/" />

        <!-- Bootstrap core CSS -->
        <link href="css/planpage2.css" rel="stylesheet" />
         <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- Custom styles for this template -->
        <link href="css/pagedashboard.css" rel="stylesheet" />
        <script src="js/session.js"></script>
        <style>
            .background {
                background-image: url('imagess/Backforplan.png');
                background-repeat: no-repeat;
                /* Full height */
                height: 100%;
                /* Center and scale the image nicely */
                background-repeat: no-repeat;
                background-size: cover;
            }

            .heading {
                text-align: center;
                color: black;
            }


            .img {
                margin-left: 350px;
                border: 0px;
                height: 620px;
            }

            #rptrLinkInvst {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border: 1px solid #ddd;
                width: 100%;
            }

                #rptrLinkInvst tr {
                    border: 1px solid #ddd;
                    padding: 0px;
                
                    text-align: left;
                    color: black;
                    text-align: center;
                }

                    #rptrLinkInvst tr th {
                        border: 1px solid #ddd;
                        padding: 0px;
                     
                       width:10%;
                       background-color: #4CAF50;
                        color: white;
                        text-align: center;
                    }

                    #rptrLinkInvst tr td {
                        border: 1px solid #ddd;
                        padding: 0px;
                  
                        text-align: left;
                        color: black;
                        text-align: center;
                    }
        </style>
    </head>
    <body>





        <div>
            <h1 class="heading"><u><b>Available Resources</b></u></h1>

            <asp:Repeater ID="rptrLinkInvstm" runat="server">
                <HeaderTemplate>
                    <table id="rptrLinkInvst">
                        <tr>
                            <th colspan="5">&nbsp;&nbsp;&nbsp;
                                Highly Liquid Financial Investments</th>
                        </tr>
                        <tr>
                            <th>Investments</th>
                            <th>Current Valuation</th>
                            <th>Liquidity</th>

                        </tr>
                </HeaderTemplate>
                <ItemTemplate>

                    <tr>

                        <td style="text-align:left;"><%#DataBinder.Eval(Container,"DataItem.Name")%></td>
                        <td style="text-align:right;"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.Amount")))%></td>
                        <td><%#DataBinder.Eval(Container,"DataItem.liquidity")%></td>

                    </tr>


                </ItemTemplate>




                <FooterTemplate>
                    </table>
                </FooterTemplate>

            </asp:Repeater>

            <asp:Repeater ID="Mediumliqidity" runat="server">
                <HeaderTemplate>
                    <table id="rptrLinkInvst">
                        <tr>
                        </tr>

                        <tr>
                            <th colspan="3">Less Liquid Financial Investments</th>
                        </tr>
                        <tr>
                            <th>Investments</th>
                            <th>Current Valuation</th>
                            <th>Liquidity</th>

                        </tr>
                </HeaderTemplate>
                <ItemTemplate>

                    <tr>

                        <td style="text-align:left;"><%#DataBinder.Eval(Container,"DataItem.Name")%></td>
                        <td style="text-align:right;"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.Amount")))%></td>
                        <td><%#DataBinder.Eval(Container,"DataItem.liquidity")%></td>

                    </tr>


                </ItemTemplate>




                <FooterTemplate>
                    </table>
                </FooterTemplate>

            </asp:Repeater>
            &nbsp;<asp:Repeater ID="lowliquid" runat="server">
                <HeaderTemplate>
                    <table id="rptrLinkInvst">
                        <tr>
                        </tr>

                        <tr>
                            <th colspan="3">Other Assets</th>
                        </tr>
                        <tr>
                            <th>Investments</th>
                            <th>Current Valuation</th>
                            <th>Liquidity</th>

                        </tr>
                </HeaderTemplate>
                <ItemTemplate>

                    <tr>

                        <td style="text-align:left;"><%#DataBinder.Eval(Container,"DataItem.Name")%></td>
                        <td style="text-align:right;"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.Amount")))%></td>
                        <td><%#DataBinder.Eval(Container,"DataItem.liquidity")%></td>

                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>

            </asp:Repeater>
            <br />
            <br />
            <asp:Repeater ID="totalinvest" runat="server">
                <HeaderTemplate>
                    <table id="rptrLinkInvst">
                        <tr>
                        </tr>

                        <tr>
                            <th style="width:10%;">Total Investment</th>
                            <th><%=formatter.IndianCurrency(totalinvestment.ToString())%></th>
                </HeaderTemplate>
                <ItemTemplate>

                    <tr>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>

            </asp:Repeater>




        </div>


    </body>
    </html>
</asp:Content>
