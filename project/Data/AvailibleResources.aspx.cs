﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinFinancialPlan;
using data.model;
using System.Data;
using System.Configuration;
using System.Web.Configuration;



namespace FinFinancialPlan
{
    public partial class AvailibleResources : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
        DataSet dtCashOutFlow;
        DataTable highinv;
        DataTable lowinv;
        DataTable mediuminvs;
        DataTable dISEXPENSE;
        DataTable sAVEXPENSE;
        Dictionary<string, string> clintdic;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
          public string strPlanId;
        public string totalin;
        public string totalfixedexpence;
        public object sumObjecthgh;
        public object sumObjectmed;
        public object sumObjectlow;
        public string totaldisexpence;
        public int totalmedium;
        public int totalhighinv;
        public int totalowinv;
        public int totalinvestment;




        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];


                strUserId = clintdic["userid"];

                strBasicId = clintdic["basicid"];

                strPlanId = clintdic["planid"];
            }
            else { Response.Redirect("notfound.html"); }



            // SESSION POPUP
            Session["Reset"] = true;
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);
						
						

            
            dtCashOutFlow = data.getAllAvailres(strBasicId , strPlanId ,strUserId);
            if(dtCashOutFlow.Tables[0].Rows.Count != 0)
            {
                highinv = dtCashOutFlow.Tables[0];
                sumObjecthgh =  highinv.Compute("Sum(Amount)", string.Empty);
                totalhighinv = Int32.Parse(sumObjecthgh.ToString());
                rptrLinkInvstm.DataSource = dtCashOutFlow.Tables[0];
            
                rptrLinkInvstm.DataBind();
            }


            dtCashOutFlow = data.getAllAvailres(strBasicId, strPlanId, strUserId);
            
            if (dtCashOutFlow.Tables[1].Rows.Count != 0)
            {
                mediuminvs = dtCashOutFlow.Tables[1];
                sumObjectmed = highinv.Compute("Sum(Amount)", string.Empty);
                totalmedium = Int32.Parse(sumObjecthgh.ToString());
                Mediumliqidity.DataSource = dtCashOutFlow.Tables[1];

                 Mediumliqidity.DataBind();
            
            }
            
            dtCashOutFlow = data.getAllAvailres(strBasicId, strPlanId, strUserId);


            if (dtCashOutFlow.Tables[2].Rows.Count != 0)
            {
                mediuminvs = dtCashOutFlow.Tables[1];
                sumObjectlow = highinv.Compute("Sum(Amount)", string.Empty);
                totalowinv = Int32.Parse(sumObjecthgh.ToString());
                lowliquid.DataSource = dtCashOutFlow.Tables[2];
                lowliquid.DataBind();
            }

            if(totalhighinv != null || totalmedium != null || totalowinv != null )
            {
                totalinvestment = totalhighinv + totalmedium + totalowinv;
                totalinvest.DataSource = dtCashOutFlow.Tables[0];
                totalinvest.DataBind();

            }



        }

        protected void retirement_Click(object sender, EventArgs e)
        {
            Response.Redirect("Riskprotection.aspx");
        }
    }
}