﻿<%@ Page Language="C#" AutoEventWireup="true"   CodeBehind="CashIn.aspx.cs" Inherits="FinFinancialPlan.CashIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  
   <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="css/tabs.css" rel="stylesheet" />
    <script src="js/tabCustom.js"></script>
    <script src="js/validation.js"></script>
      <link href="css/repeaterstype.css" rel="stylesheet" />
    <link href="css/timeout-dialog.css" rel="stylesheet" />
    <script src="js/timeout-dialog.js"></script>
       <script src="js/session.js"></script>
</head>
<body>

<div class="container">
   <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-1" type="button" class="btn btn-default btn-circle" disabled="disabled">1</a>
                    <p><small>Basic Details</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-2" type="button" class="btn btn-default btn-success btn-circle">2</a>
                    <p><small>Income</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p><small>Expense</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p><small>NetAssets Entry</small></p>
                </div>
                 <div class="stepwizard-step col-xs-2">
                    <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                    <p><small>Goals Summary</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                    <p><small>NetAssets Allocation</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-7" type="button" class="btn btn-default btn-circle" disabled="disabled">7</a>
                    <p><small>NetCashFlow Allocation</small></p>
                </div>
            </div>
        </div>




 <form id="form1" runat="server" role="form">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>


        <div class="panel panel-primary setup-content" id="step-2">
            <div class="panel-heading"><h3 class="panel-title">CashIn Flow</h3></div>
           
            
            <div class="panel-body">
                 <div class="col-lg-12">
                    <div class="col-lg-4">
                     
                    
                        <div class="form-group">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>

                            <label class="control-label">Inflow From *</label>
                                <asp:DropDownList runat="server" ID="ddlInflowType" CssClass="form-control" OnSelectedIndexChanged="ddlInflowType_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>


                                        <div class="form-group">
                            <label class="control-label">Amount*</label>
                             <asp:TextBox runat="server" CssClass="form-control" ID="txtAmount" MaxLength="10"></asp:TextBox>    
                        </div>

                                <asp:Panel ID="pnlOtherInflow" runat="server" >
                                       <label class="control-label">Name to Identify(Inflow)</label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="txtInflowName"></asp:TextBox> 
                               
                                </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Frequency</label>
                             <asp:DropDownList runat="server" ID="ddlFrequency" CssClass="form-control"></asp:DropDownList>
                        </div>
                       <asp:Button Text="Save" OnClick="Save_Click" runat="server" id ="btnSave" />
                       <asp:Button Text="Update" OnClick="Update_Click" runat="server" id ="btnUpdate" Visible="false" />
                        <asp:Label ID="lblMsg" Visible="false" Font-Size="Small" runat="server"></asp:Label>
                    </div>
                    <div >
                     <%--------------RIGHT DIV--------------%>
            <div id="rightDv"  >
                <div>
            <div id="invoice" class="col-lg-8">
               <div style="overflow-x:scroll">
                   <asp:Repeater ID="rptrCashFlowIn" runat="server" >
                   <HeaderTemplate>
                        <table id="ulRptOverflowx">
                            <tr>
                                                            
                                    <th>Inflow From</th>
                                    <th>Name of Inflow</th>
                                   <th>Amount</th>
                                    <th>Frequency</th>
                                   <th>Delete</th>
                                   <th>Edit</th> 


                                   
                                </tr>
                            
                    </HeaderTemplate>
                    <ItemTemplate>
                        
                            <tr>
                               
                                    <td><%#DataBinder.Eval(Container,"DataItem.type_name")%></td>
                                    <td><%#DataBinder.Eval(Container,"DataItem.inflow_type_name")%></td>
                                    <td style="text-align:right;" ><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.Income"))) %></td>
                                    <td><%#DataBinder.Eval(Container,"DataItem.frequency_text")%></td>
                                    <td><asp:LinkButton ID="lnkDelete" Text="Delete" runat="server" OnClick="OnDelete" OnClientClick="return confirm('Do you want to delete this row?');"  /></td>
                                    <td><asp:LinkButton ID="lnkUpdate" Text="Edit" runat="server" OnClick="OnEdit" /></td>
                                    <asp:Label ID="lblId" runat="server" Text='<%# Eval("id") %>' Visible = "false" />
                                 </tr>
                                
                            </tr>
                        
                        </ItemTemplate>
                    <FooterTemplate>
                       </table>
                    </FooterTemplate>    
                    
                </asp:Repeater>
               </div>
                
            </div>
        </div>
                </div>
                </div>
                </div>
                
                <div class="col-lg-12">
                    &nbsp;
                </div>
                 <div class="col-lg-12">
                   
                 <asp:Button Text="Next >>" runat="server" id ="Nexttocashout" OnClick="Nexttocashout_Click" class="btn btn-primary nextBtn pull-right"  type="button" />
                <asp:Button Text="<< Back" runat="server" id ="prevToBasicDetails" OnClick="prevToBasicDetails_Click" class="btn btn-primary nextBtn pull-right"  type="button" />
                </div>
                
            </div> 

         </div>
        </form>
</div>
 

</body>
</html>
