﻿<%@ Page Language="C#" AutoEventWireup="true"   CodeBehind="CashOutMap.aspx.cs" Inherits="FinFinancialPlan.CashOutMap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

    <link href="css/timeout-dialog.css" rel="stylesheet" />
    <script src="js/timeout-dialog.js"></script>
    <link href="css/tabs.css" rel="stylesheet" />
    <link href="css/repeaterstype.css" rel="stylesheet" />
     <script src="js/tabCustom.js"></script>
     <script src="js/session.js"></script>

 
   


     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
     <script src="js/validation.js"></script>
     <script type="text/javascript">
         $(document).ready(function () {
             var prm = Sys.WebForms.PageRequestManager.getInstance();
             prm.add_initializeRequest(InitializeRequest);
             prm.add_endRequest(EndRequest);

             // Place here the first init of the autocomplete
             SetAutoComplete();
         });

         function InitializeRequest(sender, args) {
         }

         function EndRequest(sender, args) {
             // after update occur on UpdatePanel re-init the Autocomplete
             SetAutoComplete();
         }
         $(function () {
             SetAutoComplete();
         });



         function SetAutoComplete() {
             $("#<%=txtSchemeName.ClientID %>").autocomplete({
             source: function (request, response) {
                 $.ajax({
                     url: 'CashOutMap.aspx/getSchemes',
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: "{'text':'" + document.getElementById("<%=txtSchemeName.ClientID %>").value + "'}",
                     success: function (data) {
                         debugger;
                         if (!data.d) {
                             var result = [
                                 {
                                     label: 'No matches found',
                                     value: response.term
                                 }
                             ];
                             $("#<%=hdnSchemeid.ClientID %>").val("");
                             response(result);
                         }
                         else {

                             var dtParsed = $.parseJSON(data.d);
                             response($.map(dtParsed, function (item) {
                                 var itm = item;

                                 return {
                                     label: item.schemeName,
                                     value: item.schemeName,
                                     id: item.id
                                 };
                             }));
                         }
                     },

                 })
             },
             select: function (event, ui) {
                 debugger;
                 $("#<%=hdnSchemeid.ClientID %>").val(ui.item.id);

             }
         });
         }
 </script>
     <script >
         $(function () {
             $("#S_Date_loan").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd" });
             $("#E_Date_loan").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd" });
             $("#txtPolicyIssueDate").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd" });
             $("#txtSipStartDate").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd" });


         });
      </script>
     <script type="text/javascript">
         $(document).ready(function () {
             Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

             function EndRequestHandler(sender, args) {
                 $("#S_Date_loan").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd" });
                 $("#E_Date_loan").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd" });
                 $("#txtPolicyIssueDate").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd" });
                 $("#txtSipStartDate").datepicker({ changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd" });
             }

         });
    </script>
</head>


<body>
<div class="container">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-1" type="button" class="btn btn-default btn-circle" disabled="disabled">1</a>
                    <p><small>Basic Details</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p><small>Income</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p><small>Expense</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-4" type="button" class="btn btn-default btn-success btn-circle">4</a>
                    <p><small>NetAssets Entry</small></p>
                </div>
                 <div class="stepwizard-step col-xs-2">
                    <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                    <p><small>Goals Summary</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                    <p><small>NetAssets Allocation</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-7" type="button" class="btn btn-default btn-circle" disabled="disabled">7</a>
                    <p><small>NetCashFlow Allocation</small></p>
                </div>
            </div>
        </div>



 <form id="form1" runat="server" role="form">
      <input type="hidden" id="hdnSchemeid" runat="server" value="" />
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <div class="panel panel-primary setup-content" id="step-4">
                 <div class="panel-heading"><h3 class="panel-title">Available Resources Entry</h3></div>
              
                    <div class="panel-body">
                       

                        <div class="col-lg-12">

                            <asp:Label ID="lblMsg" Visible="true" Font-Size="Small" ForeColor="Green" runat="server" Text=""></asp:Label>
                            <div class="col-lg-6">
                                

                                <div class="form-group">
                                    <label class="control-label">Select Asset Type</label>
                                     <asp:DropDownList runat="server" CssClass="form-control" ID="Entry_type" AutoPostBack="true" OnSelectedIndexChanged="Entry_type_SelectedIndexChanged" required="required" placeholder="Enter First Name" >
                                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                                        <asp:ListItem Value="Loan_Entry">Loan</asp:ListItem>
                                        <asp:ListItem Value="Other_Asset">Other Assets</asp:ListItem>
                                        <asp:ListItem Value="Mutual_Fund">Mutual Fund</asp:ListItem>   
                                        <asp:ListItem Value="Policy">Insurance</asp:ListItem>                                       
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6"></div>
                        </div>

                         <div class="col-lg-12">
                             <asp:UpdatePanel runat="server" ID="updatepanel1">
                                <ContentTemplate>
                             <%--*** ENTRY FORMS ***--%>
                             <div class="col-lg-6">
                                 
                                 <%--LOAN FORM--%>
                                 <div id="Loan_typetd" class="col-lg-12" runat ="server" visible="false">
                                      
                                     <div class="form-group">
                                        <label class="control-label">Loan Type</label>
                                          <asp:TextBox ID="txtLoanName" runat="server" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">Principal Amount</label>
                                          <asp:TextBox  ID="Principal_amt" runat="server"  MaxLength="10" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">Interest(%)</label>
                                          <asp:TextBox  ID="Intesest_loan" runat="server"  MaxLength="5" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                     <div class="form-group">
                                        <label class="control-label">Start Date</label>
                                         <asp:TextBox ID="S_Date_loan"  CssClass="form-control" runat="server"></asp:TextBox>
                                      </div>
                                     <div class="form-group">
                                        <label class="control-label">End Date</label>
                                         <asp:TextBox ID="E_Date_loan"  CssClass="form-control" runat="server"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">EMI</label>
                                         <asp:TextBox ID="EMI_txt" MaxLength="10"  CssClass="form-control" runat="server"></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">Pending Loan Amount</label>
                                         <asp:TextBox ID="txtPaidValue" MaxLength="10" CssClass="form-control" runat="server"></asp:TextBox>
                                      </div>

                                     <div class="form-group">
                                         <label class="control-label">Transaction Source</label>
                                         <asp:DropDownList runat="server" CssClass="form-control" ID="ddlLoanTrxnSource" required="required"  >
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                            <asp:ListItem Value="Fincart">Fincart</asp:ListItem>
                                            <asp:ListItem Value="Extrernal">External</asp:ListItem>
                                         </asp:DropDownList>
                                     </div>

                                     <asp:Button ID="saveloanentry" runat="server" OnClick="saveloanentry_Click" Text="Save" />
                                     <asp:Button ID="updateLoanEntry" runat="server" OnClick="updateLoanEntry_Click" Text="Update" Visible="false" />
                                     <asp:Button ID="btnLoanCancel" runat="server" OnClick="btnLoanCancel_Click" Text="Cancel" Visible="false" />
                                 </div>
                                 <%--INSURANCE FORM--%>
                                 <div id="policy_typetd" class="col-lg-12" runat ="server" visible="false">
                                      
                                     <div class="form-group">
                                         <label class="control-label">Insurance type</label>
                                         <asp:DropDownList runat="server" CssClass="form-control" ID="ddlInsurType" AutoPostBack="true" OnSelectedIndexChanged="ddlInsurType_SelectedIndexChanged" required="required"  >
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                            <asp:ListItem Value="24">Health Insurance</asp:ListItem>
                                            <asp:ListItem Value="14">ULIP</asp:ListItem>
                                            <asp:ListItem Value="11">Term Insurance</asp:ListItem>
                                            <asp:ListItem Value="2">Other Insurance</asp:ListItem> 
                                         </asp:DropDownList>
                                       </div>
                                      <div class="form-group" >
                                         <label class="control-label" runat="server" id="lblHlvLabelText"></label>
                                          <asp:Label ID="lblHLVAmount" runat="server" Text="0" CssClass="labelBlue"></asp:Label>
                                      
                                       </div>
                                      <div class="form-group">
                                         <label class="control-label">Final (HLV)</label>
                                          <asp:TextBox  ID="txtFinalHlv" runat="server" MaxLength="10" CssClass="form-control" ></asp:TextBox>
                                       </div>
                                     <div class="form-group">
                                         <label class="control-label">Insurance Policy Name</label>
                                          <asp:TextBox  ID="txtPolicyName" runat="server" CssClass="form-control" ></asp:TextBox>
                                       </div>
                                     <div class="form-group">
                                        <label class="control-label">Policy number</label>
                                          <asp:TextBox  ID="txtPolicy_no" runat="server" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                     <div class="form-group">
                                        <label class="control-label">Policy Provider</label>
                                           <asp:DropDownList runat="server" CssClass="form-control" ID="ddlPolicyProvider" required="required" >
                                         </asp:DropDownList>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">Issue Date</label>
                                           <asp:TextBox  ID="txtPolicyIssueDate" runat="server" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                     <div class="form-group">
                                        <label class="control-label">Premium Amount</label>
                                          <asp:TextBox  ID="txtPremium_amt" runat="server" MaxLength="10" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">Policy Term</label>
                                          <asp:TextBox  ID="txtPolicyTerm" runat="server" MaxLength="5" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                     <div class="form-group">
                                        <label class="control-label">Cover / Sum Assured</label>
                                          <asp:TextBox  ID="txtPolicyCover" MaxLength="10" runat="server" CssClass="form-control" ></asp:TextBox>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label">Policy Owner</label>
                                           <asp:DropDownList runat="server" CssClass="form-control" ID="ddlPolicyOwner"  required="required"  >
                                         </asp:DropDownList>
                                      </div>

                                      <div class="form-group" id="insCurrValPnl" runat="server" visible="false">
                                        <label class="control-label">Current Valuation</label>
                                          <asp:TextBox  ID="txtInsurCurrVal" runat="server" MaxLength="10" CssClass="form-control" Visible="false" ></asp:TextBox>
                                      </div>
                                     <div class="form-group" id="insLockDurationPnl" runat="server" visible="false">
                                        <label class="control-label">LockIn Duration (years)</label>
                                          <asp:TextBox ID="txtInsurLockDuration" runat="server" MaxLength="5" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      <div class="form-group" id="insLockInterestPnl" runat="server" visible="false">
                                        <label class="control-label">LockIn Interest (%)</label>
                                          <asp:TextBox ID="txtInsurLockInterest" runat="server" MaxLength="4" CssClass="form-control" ></asp:TextBox>
                                      </div>


                                      <div class="form-group">
                                         <label class="control-label">Is Active Policy</label>
                                         <asp:DropDownList runat="server" CssClass="form-control" ID="ddlIsActivePolicy"  required="required"  >
                                            <asp:ListItem Value="-1">--Select--</asp:ListItem>
                                            <asp:ListItem Value="0">Discontinue</asp:ListItem>
                                            <asp:ListItem Value="1">Continue</asp:ListItem>
                                            
                                         </asp:DropDownList>
                                       </div>

                                      <div class="form-group">
                                         <label class="control-label">Transaction Source</label>
                                         <asp:DropDownList runat="server" CssClass="form-control" ID="ddlInsurTrxnSource" required="required"  >
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                            <asp:ListItem Value="Fincart">Fincart</asp:ListItem>
                                            <asp:ListItem Value="Extrernal">External</asp:ListItem>
                                         </asp:DropDownList>
                                     </div>
                                     <asp:Button ID="btnSave_poli" runat="server" Text="Save" OnClick="btnSave_poli_Click" />
                                     <asp:Button ID="btnUpdate_Policy" runat="server" Text="Update" OnClick="btnUpdate_Policy_Click" Visible="false" />
                                     <asp:Button ID="btnInsurCancel" runat="server" OnClick="btnInsurCancel_Click" Text="Cancel" Visible="false" />

                                 </div>

                                 <%--OTHER ASSET'S FORM--%>
                                  <div id="other_assetstd" class="col-lg-12" runat ="server" visible="false">
                                      
                                      <div class="form-group">
                                        <label class="control-label">Asset Type</label>
                                          <asp:DropDownList runat="server" CssClass="form-control" ID="ddlAssettype" required="required" >
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                            <asp:ListItem Value="21">EPF</asp:ListItem>
                                            <asp:ListItem Value="22">PPF</asp:ListItem>
                                            <asp:ListItem Value="15">Fixed Deposit</asp:ListItem>
                                            <asp:ListItem Value="19">CASH IN BANK</asp:ListItem>
                                            <asp:ListItem Value="25">Recurring Deposit</asp:ListItem>
                                              <asp:ListItem Value="26">Other</asp:ListItem>
                                         </asp:DropDownList>
                                      </div>
                                       <div class="form-group">
                                        <label class="control-label">Liquidity Type</label>
                                          <asp:DropDownList runat="server" CssClass="form-control" ID="ddlLiquidityType" required="required" >
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                            <asp:ListItem Value="High">High</asp:ListItem>
                                            <asp:ListItem Value="Low">Low</asp:ListItem>
                                         
                                         </asp:DropDownList>
                                      </div>
                                       <div class="form-group">
                                        <label class="control-label">Asset Name</label>
                                          <asp:TextBox  ID="txtAssetname" runat="server" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">LockIn Duration (years)</label>
                                          <asp:TextBox  ID="txtLockInDuration" runat="server" MaxLength="5" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">LockIn Interest (%)</label>
                                          <asp:TextBox  ID="txtLockInInterest" runat="server" MaxLength="4" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">Current Value</label>
                                          <asp:TextBox  ID="txtCurntvalue" runat="server" MaxLength="10" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      
                                      <div class="form-group">
                                         <label class="control-label">Transaction Source</label>
                                         <asp:DropDownList runat="server" CssClass="form-control" ID="ddlAssetTrxnSource" required="required"  >
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                            <asp:ListItem Value="Fincart">Fincart</asp:ListItem>
                                            <asp:ListItem Value="Extrernal">External</asp:ListItem>
                                         </asp:DropDownList>
                                     </div>


                                      <asp:Button ID="btnOtherAssetSave" runat="server" Text="Save" OnClick="btnOtherAssetSave_Click" />
                                      <asp:Button ID="btnOtherAssetUpdate" runat="server" Text="Update" OnClick="btnOtherAssetUpdate_Click" Visible="false" />
                                       <asp:Button ID="btnOtherAssetCancel" runat="server" OnClick="btnOtherAssetCancel_Click" Text="Cancel" Visible="false" />
                                  </div>

                                 <%--MUTUAL FUND FORM--%>
                                  <div id="Transaction_typetd" class="col-lg-12" runat ="server" visible="false">
                                       <div class="form-group">
                                            <label class="control-label">Investment Type</label>
                                            <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTransType" required="required" >
                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                <asp:ListItem Value="2">SIP</asp:ListItem>
                                                <asp:ListItem Value="1">Lumpsum</asp:ListItem>
                                           </asp:DropDownList>
                                        </div>

                                      <div class="form-group">
                                        <label class="control-label">SchemeName</label>
                                          <asp:TextBox  ID="txtSchemeName" runat="server" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                     
                                       <div class="form-group">
                                        <label class="control-label">transaction amount</label>
                                          <asp:TextBox  ID="txtTrxnAmount" runat="server" MaxLength="10" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">FolioNo</label>
                                          <asp:TextBox  ID="txtFolioNo" runat="server" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      <div class="form-group">
                                        <label class="control-label">Current Valuation</label>
                                          <asp:TextBox  ID="txtCurrValuation" runat="server" MaxLength="10" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                      <div class="form-group" runat="server" id="divSipStDate" >
                                        <label class="control-label">Sip (Start Date)</label>
                                          <asp:TextBox  ID="txtSipStartDate" runat="server" CssClass="form-control" ></asp:TextBox>
                                      </div>
                                       <div class="form-group" runat="server" id="divSipMonthDay" >
                                        <label class="control-label">Sip (Day of month)</label>
                                           <asp:DropDownList runat="server" CssClass="form-control" ID="ddlSipMDate" required="required" >
                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                <asp:ListItem Value="3">3</asp:ListItem>
                                               <asp:ListItem Value="4">4</asp:ListItem>
                                               <asp:ListItem Value="5">5</asp:ListItem>
                                               <asp:ListItem Value="6">6</asp:ListItem>
                                               <asp:ListItem Value="7">7</asp:ListItem>
                                               <asp:ListItem Value="8">8</asp:ListItem>
                                               <asp:ListItem Value="9">9</asp:ListItem>
                                               <asp:ListItem Value="10">10</asp:ListItem>
                                               <asp:ListItem Value="11">11</asp:ListItem>
                                               <asp:ListItem Value="12">12</asp:ListItem>
                                               <asp:ListItem Value="13">13</asp:ListItem>
                                               <asp:ListItem Value="14">14</asp:ListItem>
                                               <asp:ListItem Value="15">15</asp:ListItem>
                                               <asp:ListItem Value="16">16</asp:ListItem>
                                               <asp:ListItem Value="17">17</asp:ListItem>
                                               <asp:ListItem Value="18">18</asp:ListItem>
                                               <asp:ListItem Value="19">19</asp:ListItem>
                                               <asp:ListItem Value="20">20</asp:ListItem>
                                               <asp:ListItem Value="21">21</asp:ListItem>
                                               <asp:ListItem Value="22">22</asp:ListItem>
                                               <asp:ListItem Value="23">23</asp:ListItem>
                                               <asp:ListItem Value="24">24</asp:ListItem>
                                               <asp:ListItem Value="25">25</asp:ListItem>
                                               <asp:ListItem Value="26">26</asp:ListItem>
                                               <asp:ListItem Value="27">27</asp:ListItem>
                                               <asp:ListItem Value="28">28</asp:ListItem>
                                               <asp:ListItem Value="29">29</asp:ListItem>
                                               <asp:ListItem Value="30">30</asp:ListItem>
                                               <asp:ListItem Value="31">31</asp:ListItem>

                                           </asp:DropDownList>
                                      </div>
                                      <div class="form-group">
                                         <label class="control-label">Transaction Source</label>
                                         <asp:DropDownList runat="server" CssClass="form-control" ID="ddlMfTrxnSource" required="required"  >
                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                            <asp:ListItem Value="Fincart">Fincart</asp:ListItem>
                                            <asp:ListItem Value="Extrernal">External</asp:ListItem>
                                         </asp:DropDownList>
                                     </div>

                                        <asp:Button ID="btnMfSave" runat="server" Text="Save" OnClick="btnMfSave_Click" />
                                        <asp:Button ID="btnMfUpdate" runat="server" Text="Update" OnClick="btnMfUpdate_Click" Visible="false" />
                                       <asp:Button ID="btnMfCancel" runat="server" OnClick="btnMfCancel_Click" Text="Cancel" Visible="false" />
                                  </div>
                            </div>
                             
                            
                             <%--******* REPEATER TO SHOW ENTRY *******--%>
                                  
                        <div style="float:right;" class="col-lg-6" >
                                 <div style="overflow-x:scroll">
                                 <asp:Repeater ID="rptrAllLoans" runat="server" Visible="false">
                                    <HeaderTemplate>
                                        <table id="ulRptOverflowx">
                                            <tr>                                               
                                                    <th >Loan</th>
                                                    <th >Principal</th>
                                                    <th >interest</th>
                                                    <th >Emi</th>
                                                <th>Delete</th>
                                                <th>Edit</th>
                                                  
                                                
                                            </tr>
                                    </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    
                                         <td ><%#DataBinder.Eval(Container,"DataItem.Loan_name")%></td>
                                         <td style="text-align:right" ><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.principal_amt")))%></td>
                                         <td ><%#DataBinder.Eval(Container,"DataItem.loan_interest")%></td>
                                        <td style="text-align:right"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.emiAmount"))) %></td>
                                
                                         <td><asp:LinkButton ID="lnkLoanpDelete" Text="Delete" runat="server" OnClick="lnkLoanpDelete_Click" OnClientClick="return confirm('Do you want to delete this row?');"  /></td>
                                          <td><asp:LinkButton ID="lnkEdit" Text="Edit" runat="server" OnClick="lnkEdit_Click" /></td>
                                         <asp:Label ID="lblId" runat="server" Text='<%# Eval("id")%>' Visible = "false" />
                                       
                                  
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                              </table>
                            </FooterTemplate>    
                    
                        </asp:Repeater>
                                 <asp:Repeater ID="rptrAllInsurance" runat="server" Visible="false">
                                    <HeaderTemplate>
                                        <table id="ulRptOverflowx">
                                            <tr>       <th>Insurance</th>
                                                    <th>Provider</th>
                                                    <th style="min-width:100px;">Type</th>
                                                    <th  style="text-align:right;">Premium</th>
                                                    <th  style="text-align:right;">Cover</th>
                                                       <th>Suggested/Continue</th>
                                                <th>Delete</th>
                                                <th>Edit</th>

                                                     
                                                </tr>
                                    </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                        <td><%#DataBinder.Eval(Container,"DataItem.assetName")%></td>
                                        <td><%#DataBinder.Eval(Container,"DataItem.policyPartner")%></td>
                                        <td><%#DataBinder.Eval(Container,"DataItem.assetType")%></td>
                                        <td  style="text-align:right;"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.premiumAmount")))%></td>
                                        <td  style="text-align:right;"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.sumAssured")))%></td>
                                        <td><%#DataBinder.Eval(Container,"DataItem.IsActivePolicy")%></td>

                                        <td><asp:LinkButton ID="lnkInsuranceDelete" Text="Delete" runat="server" OnClick="lnkInsuranceDelete_Click" OnClientClick="return confirm('Do you want to delete this row?');"  /></td>
                                        <td><asp:LinkButton ID="lnkInsuranceEdit" Text="Edit" runat="server" OnClick="lnkInsuranceEdit_Click" /></td>
                                        <asp:Label ID="lblInsurId" runat="server" Text='<%# Eval("id")%>' Visible = "false" />
                                       
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>    
                    
                        </asp:Repeater>
                                 <asp:Repeater ID="rptrAllOtherAssets" runat="server" Visible="false">
                                    <HeaderTemplate>
                                        <table id="ulRptOverflowx">
                                            <tr>
                                                    <th>AssetName
                                                    <th>Type</th>
                                                    <th  style="text-align:right;">Current Value</th>
                                                    <th>LockIn Duration</th>
                                                    <th>LockIn Interest</th>
                                                        <th>Delete</th>
                                                <th>Edit</th>
                                             </tr>
                                    </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                        <td ><%#DataBinder.Eval(Container,"DataItem.assetName")%></td>
                                        <td ><%#DataBinder.Eval(Container,"DataItem.AssetType")%></td>
                                        <td style="text-align:right;"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.currValuation")))%></td>
                                         <td ><%#DataBinder.Eval(Container,"DataItem.lockInDuration")%></td>
                                         <td ><%#DataBinder.Eval(Container,"DataItem.lockInInterest")%></td>
                                        

                                        <td><asp:LinkButton ID="lnkOtherAssetDelete" Text="Delete" runat="server" OnClick="lnkOtherAssetDelete_Click" OnClientClick="return confirm('Do you want to delete this row?');" /></td>
                                        <td><asp:LinkButton ID="lnkOtherAssetEdit" Text="Edit" runat="server" OnClick="lnkOtherAssetEdit_Click" /></td>
                                        <asp:Label ID="lblOthAssetId" runat="server" Text='<%# Eval("id")%>' Visible = "false" />
                                       
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                             <table></table>
                            </FooterTemplate>    
                    
                        </asp:Repeater>



                                 <asp:Repeater ID="rptrExpenseMFSIP" runat="server" Visible="false">
                                    <HeaderTemplate>

                                         <h4>MF SIP Added</h4>
                                        <table id="ulRptOverflowx">
                                           <tr>   
                                                  <th style="text-align:right;">Expense (MF SIP)</th>
                                                    <th>Amount</th>
                                                    
                                            </tr>

                                    </HeaderTemplate>
                            <ItemTemplate>
                               <tr>
                                        <td ><%#DataBinder.Eval(Container,"DataItem.Outflow_type_name")%></td>
                                        <td   style="text-align:right;"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.expense")))%></td>
                                        <asp:Label ID="lblMFId" runat="server" Text='<%# Eval("id")%>' Visible = "false" />
                                       
                                  </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                             </table>
                            </FooterTemplate>    
                    
                        </asp:Repeater>

                                 <asp:Repeater ID="rptrMFList" runat="server" Visible="false">
                                    <HeaderTemplate>
                                         <h4>MF Valuation & Details</h4>
                                        <table id="ulRptOverflowx">
                                           <tr>
                                                    <th>SchemeName</th>
                                                    <th>Category</th>
                                                    <th style="text-align:right;">Amount</th>
                                                    <th style="text-align:right;">Current Value</th>
                                             </tr>
                                    </HeaderTemplate>
                            <ItemTemplate>
                               <tr>
                                        <td ><%#DataBinder.Eval(Container,"DataItem.assetName")%></td>
                                        <td ><%#DataBinder.Eval(Container,"DataItem.otherGoalName")%></td>
                                        <td   style="text-align:right;"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.amount")))%></td>
                                        <td   style="text-align:right;""><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.currValuation")))%></td>

                                        <td><asp:LinkButton ID="lnkMFDelete" Text="Delete" runat="server" OnClick="lnkMFDelete_Click" OnClientClick="return confirm('Do you want to delete this row?');" /></td>
                                        <td><asp:LinkButton ID="lnkMFEdit" Text="Edit" runat="server" OnClick="lnkMFEdit_Click" /></td>
                                        <asp:Label ID="lblMFId" runat="server" Text='<%# Eval("id")%>' Visible = "false" />
                                       
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                              </table>
                            </FooterTemplate>    
                    
                        </asp:Repeater>
                                     </div>
                             </div>

                            
                                 </ContentTemplate>
                              </asp:UpdatePanel>
                         </div>
                        
                        
                        
                        <%--******* BOTTOM BUTTONS GOES HERE *******--%>
                        <div class="col-lg-12">
                             
                            &nbsp;
                        </div>
                        
                        <div class="col-lg-12">
                            <asp:Button Text="Next >>" OnClick="btnNext_Click" runat="server" ID="btnNext" class="btn btn-primary nextBtn pull-right" type="button" />
                            <asp:Button Text="<< Back" runat="server" ID="prevToCashOut" OnClick="prevToCashOut_Click" class="btn btn-primary nextBtn pull-right" type="button" />
                        </div>

                    </div>
            </div>
     </form>
</div>
</body>
</html>