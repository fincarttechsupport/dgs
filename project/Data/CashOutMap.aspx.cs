﻿using data.model;
using FinFinancialPlan;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;
using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{
    public partial class CashOutMap : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
        DataTable dtPolicyProvd;
        DataTable dtLoans;
        DataTable dtInsurance;
        DataTable dtOtherAssets;
        DataTable dtMFList;
        DataTable dtExpenseMFList;
        DataTable dtPolicyOwnerList;
        Dictionary<string, string> clintdic;
        public string strBasicId;
        public string strUserId;
        public string strCreatedByEmpId;
        public string strPlanId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];

                strPlanId = clintdic["planid"];
                strUserId = clintdic["userid"];
                strCreatedByEmpId = clintdic["createdby"];
                strBasicId = clintdic["basicid"];

                // ** SET LAST WORKING PAGE **
                data.SetLastPageWorkingById(clintdic["basicid"], clintdic["planid"], "CashOutMap.aspx");
            }
            else { Response.Redirect("default.aspx"); }

            if (!IsPostBack)
            {
                this.addControlAttributes();
               
						
                //this.bindInvestments(strBasicId);
                //this.bindMappedInvestments(strBasicId);
                //this.bindLoanAndOtherAssets(strBasicId);
                //this.bindMappedLoanOtherAssets(strBasicId);

                //this.bindInsurance(strBasicId);
                //this.bindMappedInsurance(strBasicId);
            }
            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ClientScript.RegisterStartupScript(this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);
            
            //Session["Reset"] = true;
            //Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            //SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            //int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            //ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            Response.Redirect("goalsToFund.aspx");
        }

        protected void prevToCashOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("CashOut.aspx");
        }



        protected void lnkMap_Click(object sender, EventArgs e)
        {
            CashflowOut invstmt = new CashflowOut();

            if (!string.IsNullOrWhiteSpace(strPlanId))
            {

                //Find the reference of the Repeater Item.
                RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
                string strAssetId = Convert.ToString((item.FindControl("lblAssetId") as Label).Text);
                string strAssetTypeId = Convert.ToString((item.FindControl("lblAssetTypeId") as Label).Text);
                string strAmount = Convert.ToString((item.FindControl("lblAmount") as Label).Text);

                invstmt.amount = strAmount;
                invstmt.basicId = strBasicId;
                invstmt.assetId = strAssetId;
                invstmt.assetType = strAssetTypeId;
                invstmt.planId = strPlanId;
                invstmt.createdBy = strCreatedByEmpId;
                invstmt.updatedBy = strCreatedByEmpId;

                bool resp = data.MapInvestment(invstmt);
                if (resp)
                {
                    //this.bindInvestments(strBasicId);
                    //this.bindMappedInvestments(strBasicId);
                }

            }
            else { Response.Redirect("default.aspx"); }

        }

        protected void saveloanentry_Click(object sender, EventArgs e)
        {
            LoanEntry loan = new LoanEntry();
            if (txtLoanName.Text.Trim() != string.Empty)
            {
                if (Principal_amt.Text.Trim() != string.Empty)
                {
                    if (Intesest_loan.Text.Trim() != string.Empty)
                    {
                        if (S_Date_loan.Text.Trim() != string.Empty)
                        {
                            if (E_Date_loan.Text.Trim() != string.Empty)
                            {
                                if (EMI_txt.Text.Trim() != string.Empty)
                                {
                                    if (ddlLoanTrxnSource.SelectedValue != "0")
                                    {
                                        loan.basicId = strBasicId;
                                        loan.planId = strPlanId;
                                        loan.AssetName = txtLoanName.Text.Trim();
                                        loan.amount = Principal_amt.Text.Trim();
                                        loan.interest = Intesest_loan.Text.Trim();
                                        loan.startDate = S_Date_loan.Text.Trim();
                                        loan.endDate = E_Date_loan.Text.Trim();
                                        loan.emi = EMI_txt.Text.Trim();
                                        loan.currentValue = txtPaidValue.Text.Trim();
                                        loan.updatedBy = strCreatedByEmpId;
                                        loan.trxnSource = ddlLoanTrxnSource.SelectedValue;
                                        bool result = data.AddLoanEntry(loan);

                                        if (result == true)
                                        {
                                            //lblMsg.Text = "";
                                            showMessage("Loan data saved successfully..!!", "success");
                                            lblMsg.ForeColor = System.Drawing.Color.Green;
                                            lblMsg.Visible = true;
                                            resetControls();
                                            bindLoanEntry(strBasicId);
                                        }
                                    }
                                    else { showMessage("Please select transaction source", "error"); }
                                }
                                else { showMessage("Please enter emi amount", "error"); }
                            }
                            else { showMessage("Please enter end date of loan", "error"); }
                        }
                        else { showMessage("Please enter start date of loan", "error");   }
                    }
                    else { showMessage("Please enter interest for loan", "error");  }
                }
                else { showMessage("Please enter principal amount for loan", "error");  }
            }
            else { showMessage("Please enter loan name", "error"); }
        }


        /// *********** ALL BINDING FUNCTIONS *********************

        void bindLoanEntry(string basicid)
        {
            dtLoans = data.getAllLoansById(basicid);
            rptrAllLoans.DataSource = dtLoans;
            rptrAllLoans.DataBind();
        }
        void bindInsuranceEntry(string basicid)
        {
            dtInsurance = data.getAllInsuranceByBasicId(basicid);
            rptrAllInsurance.DataSource = dtInsurance;
            rptrAllInsurance.DataBind();
        }


        void bindOtherAssetEntry(string basicid)
        {
            dtOtherAssets = data.getAllOtherAssetByBasicId(basicid);
            rptrAllOtherAssets.DataSource = dtOtherAssets;
            rptrAllOtherAssets.DataBind();
        }
        void bindPolicyOwnerDropdown()
        {
            dtPolicyOwnerList = data.getAllFamilyMbrDetailsByBasicId(strBasicId);
            ddlPolicyOwner.DataSource = dtPolicyOwnerList;
            ddlPolicyOwner.DataTextField = "Name";
            ddlPolicyOwner.DataValueField = "basicid";
            ddlPolicyOwner.DataBind();
            ddlPolicyOwner.Items.Insert(0, new ListItem("--Select--", "0"));
        }
        void bindPolicyProviderList()
        {
            dtPolicyProvd = data.getAllPolicyProvidersList();
            ddlPolicyProvider.DataSource = dtPolicyProvd;
            ddlPolicyProvider.DataTextField = "partner_name";
            ddlPolicyProvider.DataValueField = "partnerId";
            ddlPolicyProvider.DataBind();
            ddlPolicyProvider.Items.Insert(0, new ListItem("--Select--", "0"));

        }
        void bindMFEntry(string basicid)
        {
            dtMFList = data.getAllMFByBasicId(basicid);
            rptrMFList.DataSource = dtMFList;
            rptrMFList.DataBind();
        }
        void bindExpenseMFSipList()
        {
            dtExpenseMFList = data.getAllExpenseMFSipById(strBasicId,strPlanId);
            rptrExpenseMFSIP.DataSource = dtExpenseMFList;
            rptrExpenseMFSIP.DataBind();
        }

        void bindHumanLifeValueByID(string basicid,string planid)
        {
            lblHlvLabelText.InnerText = "Human Life Value (" + data.getClientNameByBasicId(strBasicId) + ")";
            lblHLVAmount.Text = formatter.IndianCurrency(data.getHumanLifeValueById(basicid,planid));
            
        }
        void bindCafCurrentHLVByID(string basicid)
        {  
            txtFinalHlv.Text = data.getCafCurrentHLVById(basicid);
        }
        void addControlAttributes()
        {
            txtCurntvalue.Attributes.Add("onkeyup", "integersOnly(this);");
            txtCurrValuation.Attributes.Add("onkeyup", "integersOnly(this);");
            txtInsurCurrVal.Attributes.Add("onkeyup", "integersOnly(this);");
            txtLockInDuration.Attributes.Add("onkeyup", "integersOnly(this);");
            
            txtLockInInterest.Attributes.Add("onkeyup", "integersOnly(this);");
            txtPaidValue.Attributes.Add("onkeyup", "integersOnly(this);");
            txtPolicyCover.Attributes.Add("onkeyup", "integersOnly(this);");
            txtPolicyTerm.Attributes.Add("onkeyup", "integersOnly(this);");
            txtPremium_amt.Attributes.Add("onkeyup", "integersOnly(this);");
            txtTrxnAmount.Attributes.Add("onkeyup", "integersOnly(this);");

            EMI_txt.Attributes.Add("onkeyup", "integersOnly(this);");
            Intesest_loan.Attributes.Add("onkeyup", "integersOnly(this);");
            Principal_amt.Attributes.Add("onkeyup", "integersOnly(this);");
            
        }

        protected void lnkLoanpDelete_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string Id = Convert.ToString((item.FindControl("lblId") as Label).Text);
            bool res = data.removeLoanEntryById(Id,strBasicId);
            showMessage("Loan record removed successfully..!!", "success");
            this.bindLoanEntry(strBasicId);
            resetControls();
        }

        protected void Entry_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Entry_type.SelectedValue == "Loan_Entry")
            {

                Loan_typetd.Visible = true;
                policy_typetd.Visible = false;
                other_assetstd.Visible = false;
                Transaction_typetd.Visible = false;

                rptrAllLoans.Visible = true;
                rptrAllInsurance.Visible = false;
                rptrAllOtherAssets.Visible = false;
                rptrMFList.Visible = false;
                rptrExpenseMFSIP.Visible = false;
                bindLoanEntry(strBasicId);

            }
            else if (Entry_type.SelectedValue == "Policy")
            {
                Loan_typetd.Visible = false;
                policy_typetd.Visible = true;
                other_assetstd.Visible = false;
                Transaction_typetd.Visible = false;

                rptrAllInsurance.Visible = true;
                rptrAllLoans.Visible = false;
                rptrAllOtherAssets.Visible = false;
                rptrMFList.Visible = false;
                rptrExpenseMFSIP.Visible = false;
                this.bindPolicyProviderList();
                this.bindInsuranceEntry(strBasicId);
                this.bindPolicyOwnerDropdown();
                this.bindHumanLifeValueByID(strBasicId, strPlanId);
                this.bindCafCurrentHLVByID(strBasicId);
            }
            else if (Entry_type.SelectedValue == "Other_Asset")
            {
                Loan_typetd.Visible = false;
                policy_typetd.Visible = false;
                other_assetstd.Visible = true;
                Transaction_typetd.Visible = false;

                rptrAllInsurance.Visible = false;
                rptrAllLoans.Visible = false;
                rptrAllOtherAssets.Visible = true;
                rptrMFList.Visible = false;
                rptrExpenseMFSIP.Visible = false;
                this.bindOtherAssetEntry(strBasicId);
            }
            else if (Entry_type.SelectedValue == "Mutual_Fund")
            {
                Loan_typetd.Visible = false;
                policy_typetd.Visible = false;
                other_assetstd.Visible = false;
                Transaction_typetd.Visible = true;

                rptrAllInsurance.Visible = false;
                rptrAllLoans.Visible = false;
                rptrAllOtherAssets.Visible = false;
                rptrMFList.Visible = true;
                rptrExpenseMFSIP.Visible = true;
                this.bindExpenseMFSipList();
                this.bindMFEntry(strBasicId);

            }
        }

        void resetControls()
        {
            txtLoanName.Text = "";
            Principal_amt.Text = "";
            Intesest_loan.Text = "";
            S_Date_loan.Text = "";
            E_Date_loan.Text = "";
            EMI_txt.Text = "";
            txtPaidValue.Text = "";
            ddlInsurType.SelectedValue = "0";
            txtPolicyName.Text = "";
            ddlPolicyProvider.SelectedValue = "0";
            txtPolicy_no.Text = "";
            txtPolicyCover.Text = "";
            txtPolicyIssueDate.Text = "";
            txtPremium_amt.Text = "";
            txtPolicyTerm.Text = "";
            txtPolicyCover.Text = "";
            ddlAssettype.SelectedValue = "0";
            txtAssetname.Text = "";
            txtCurntvalue.Text = "";
            txtInsurCurrVal.Text = "";
            txtLockInDuration.Text = "";
            txtLockInInterest.Text = "";
            ddlLiquidityType.SelectedValue = "0";
            ddlIsActivePolicy.SelectedValue = "-1";
            
            txtInsurLockDuration.Text = "";
            txtInsurLockInterest.Text = "";
            //MF reset
            ddlTransType.SelectedValue = "0";
            txtSchemeName.Text = "";
            hdnSchemeid.Value = "";
            txtTrxnAmount.Text = "";
            txtFolioNo.Text = "";
            txtCurrValuation.Text = "";
            txtSipStartDate.Text = "";
            ddlSipMDate.SelectedValue = "0";

            saveloanentry.Visible = true;
            updateLoanEntry.Visible = false;
            btnSave_poli.Visible = true;
            btnUpdate_Policy.Visible = false;
            btnOtherAssetSave.Visible = true;
            btnOtherAssetUpdate.Visible = false;
            btnMfSave.Visible = true;
            btnMfUpdate.Visible = false;
            insCurrValPnl.Visible = false;
            insLockDurationPnl.Visible = false;
            insLockInterestPnl.Visible = false;
            txtInsurCurrVal.Visible = false;
            ddlPolicyOwner.SelectedValue = "0";


            ddlInsurTrxnSource.SelectedValue = "0";
            ddlAssetTrxnSource.SelectedValue = "0";
            ddlLoanTrxnSource.SelectedValue = "0";
            ddlMfTrxnSource.SelectedValue = "0";
            btnLoanCancel.Visible = false;
            btnInsurCancel.Visible = false;
            btnOtherAssetCancel.Visible = false;
            btnMfCancel.Visible = false;

            lblMsg.Text = "";
           

        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string strLoanId = Convert.ToString((item.FindControl("lblId") as Label).Text);
            DataTable dtLoanEntry = data.getLoanEntryById(strLoanId, strBasicId);

            if (dtLoanEntry != null)
            {
                if (dtLoanEntry.Rows.Count > 0)
                {
                    txtLoanName.Text = Convert.ToString(dtLoanEntry.Rows[0]["Loan_name"]);
                    Principal_amt.Text = Convert.ToString(dtLoanEntry.Rows[0]["principal_amt"]);
                    Intesest_loan.Text = Convert.ToString(dtLoanEntry.Rows[0]["Loan_interest"]);
                    S_Date_loan.Text = formatter.enUsDateFormat(Convert.ToString(dtLoanEntry.Rows[0]["start_Date"]));
                    E_Date_loan.Text = formatter.enUsDateFormat(Convert.ToString(dtLoanEntry.Rows[0]["end_date"]));
                    EMI_txt.Text = Convert.ToString(dtLoanEntry.Rows[0]["emiAmount"]);
                    txtPaidValue.Text = Convert.ToString(dtLoanEntry.Rows[0]["left_amount"]);
                    ddlLoanTrxnSource.SelectedValue = Convert.ToString(dtLoanEntry.Rows[0]["trxnSource"]);
                    Session["loanEntryID"] = strLoanId;
                    
                    saveloanentry.Visible = false;
                    updateLoanEntry.Visible = true;
                    btnLoanCancel.Visible = true;
                }

            }
        }

        protected void updateLoanEntry_Click(object sender, EventArgs e)
        {
            LoanEntry loan = new LoanEntry();

            if (Session["loanEntryID"] != null)
            {
                if (txtLoanName.Text.Trim() != string.Empty)
                {
                    if (Principal_amt.Text.Trim() != string.Empty)
                    {
                        if (Intesest_loan.Text.Trim() != string.Empty)
                        {
                            if (S_Date_loan.Text.Trim() != string.Empty)
                            {
                                if (E_Date_loan.Text.Trim() != string.Empty)
                                {
                                    if (EMI_txt.Text.Trim() != string.Empty)
                                    {
                                        if (ddlLoanTrxnSource.SelectedValue != "0")
                                        {
                                            loan.basicId = strBasicId;
                                            loan.planId = strPlanId;
                                            loan.Id = Convert.ToString(Session["loanEntryID"]);
                                            loan.AssetName = txtLoanName.Text.Trim();
                                            loan.amount = Principal_amt.Text.Trim();
                                            loan.interest = Intesest_loan.Text.Trim();
                                            loan.startDate = S_Date_loan.Text.Trim();
                                            loan.endDate = E_Date_loan.Text.Trim();
                                            loan.emi = EMI_txt.Text.Trim();
                                            loan.currentValue = txtPaidValue.Text.Trim();
                                            loan.updatedBy = strCreatedByEmpId;
                                            loan.trxnSource = ddlLoanTrxnSource.SelectedValue;
                                            bool result = data.updateLoanEntryById(loan);

                                            if (result == true)
                                            {

                                                resetControls();
                                                bindLoanEntry(strBasicId);
                                                showMessage("Loan data updated successfully..!!", "success");
                                               

                                            }
                                        }
                                        else { showMessage("Please select transaction source..!!", "error"); }
                                    }
                                    else { showMessage("Please enter emi amount", "error");  }
                                }
                                else { showMessage("Please enter end date of loan", "error"); }
                            }
                            else { showMessage("Please enter start date of loan", "error");  }
                        }
                        else { showMessage("Please enter interest for loan", "error"); }
                    }
                    else { showMessage("Please enter principal amount for loan", "error"); }
                }
                else { showMessage("Please enter loan name", "error");  }
            }
            else { Response.Redirect("Default.aspx"); }

            
           
        }

        protected void lnkInsuranceDelete_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string InsuranceId = Convert.ToString((item.FindControl("lblInsurId") as Label).Text);
            bool res = data.removeInsuranceEntryById(InsuranceId, strBasicId);
            showMessage("Insurance record removed successfully..!!", "success");
        
            this.bindInsuranceEntry(strBasicId);
            resetControls();
        }

        protected void lnkInsuranceEdit_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string InsurId = Convert.ToString((item.FindControl("lblInsurId") as Label).Text);
            DataTable dtInsurEntry = data.getInsuranceEntryById(InsurId, strBasicId);
            if (dtInsurEntry != null)
            {
                if (dtInsurEntry.Rows.Count > 0)
                {
                    txtPolicyName.Text = Convert.ToString(dtInsurEntry.Rows[0]["assetName"]);
                    txtPolicy_no.Text = Convert.ToString(dtInsurEntry.Rows[0]["policyNo"]);
                    ddlInsurType.SelectedValue = Convert.ToString(dtInsurEntry.Rows[0]["AssetTypeId"]);
                    ddlPolicyProvider.SelectedValue = Convert.ToString(dtInsurEntry.Rows[0]["policyPartnerID"]);
                    txtPolicyIssueDate.Text = formatter.enUsDateFormat(Convert.ToString(dtInsurEntry.Rows[0]["PolicyIssue_Date"]));
                    txtPremium_amt.Text = Convert.ToString(dtInsurEntry.Rows[0]["PremiumAmount"]);
                    txtPolicyTerm.Text = Convert.ToString(dtInsurEntry.Rows[0]["Policy_Term"]);
                    txtPolicyCover.Text = Convert.ToString(dtInsurEntry.Rows[0]["SumAssured"]);
                    txtInsurCurrVal.Text = Convert.ToString(dtInsurEntry.Rows[0]["currValuation"]); 
                    txtInsurLockDuration.Text = Convert.ToString(dtInsurEntry.Rows[0]["lockInDuration"]);
                    txtInsurLockInterest.Text = Convert.ToString(dtInsurEntry.Rows[0]["lockInInterest"]);

                   

                    ddlIsActivePolicy.SelectedValue = Convert.ToString(dtInsurEntry.Rows[0]["isActiveInsur"]);
                    ddlPolicyOwner.SelectedValue = Convert.ToString(dtInsurEntry.Rows[0]["policyOwner"]) != string.Empty ? Convert.ToString(dtInsurEntry.Rows[0]["policyOwner"]) : "0";
                    ddlIsActivePolicy.SelectedValue = Convert.ToString(dtInsurEntry.Rows[0]["isActiveInsur"]) != string.Empty ? Convert.ToString(Convert.ToInt32(dtInsurEntry.Rows[0]["isActiveInsur"])) : "-1" ;
                    ddlInsurTrxnSource.SelectedValue = Convert.ToString(dtInsurEntry.Rows[0]["trxnSource"]);

                    Session["InsurEntryID"] = InsurId;

                    if (Convert.ToString(dtInsurEntry.Rows[0]["AssetTypeId"]) == "14")
                    {
                        insCurrValPnl.Visible = true;
                        txtInsurCurrVal.Visible = true;
                        insLockDurationPnl.Visible = true;
                        insLockInterestPnl.Visible = true;
                    }
                    btnSave_poli.Visible = false;
                    btnUpdate_Policy.Visible = true;
                    btnInsurCancel.Visible = true;
                }

            }
        }

        protected void btnSave_poli_Click(object sender, EventArgs e)
        {
            InsuranceEntry policy = new InsuranceEntry();

            if (ddlInsurType.SelectedValue != "0")
            {
                if (txtPolicyName.Text.Trim() != string.Empty)
                {
                    if (ddlPolicyProvider.SelectedValue != "0")
                    {
                        if (txtPolicyIssueDate.Text.Trim() != string.Empty)
                        {
                            if (txtPremium_amt.Text.Trim() != string.Empty)
                            {
                                if (txtPolicyTerm.Text.Trim() != string.Empty)
                                {
                                    if (txtPolicyCover.Text.Trim() != string.Empty)
                                    {
                                        if (ddlIsActivePolicy.SelectedValue != "-1")
                                        {
                                            if (ddlPolicyOwner.SelectedValue != "0")
                                            {
                                                if (ddlInsurTrxnSource.SelectedValue != "0")
                                                {
                                                    if (txtFinalHlv.Text.Trim() != "")
                                                    {
                                                        if (ddlInsurType.SelectedValue == "14" && txtInsurCurrVal.Text.Trim() == string.Empty)
                                                        {
                                                            showMessage("Please enter current value of ULIP", "error");
                                                            return;
                                                        }

                                                        policy.basicId = strBasicId;
                                                        policy.planId = strPlanId;
                                                        policy.AssetName = txtPolicyName.Text.Trim();
                                                        policy.AssetTypeId = ddlInsurType.SelectedValue;
                                                        policy.policyIssueDt = txtPolicyIssueDate.Text.Trim();
                                                        policy.policyNo = txtPolicy_no.Text.Trim();
                                                        policy.policyPartnerId = ddlPolicyProvider.SelectedValue;
                                                        policy.policyPremium = txtPremium_amt.Text.Trim();
                                                        policy.policySumAssured = txtPolicyCover.Text.Trim();
                                                        policy.policyTerm = txtPolicyTerm.Text.Trim();
                                                        policy.currentValue = txtInsurCurrVal.Text.Trim();
                                                        policy.policyOwnerBasicId = ddlPolicyOwner.SelectedValue;
                                                        policy.trxnSource = ddlInsurTrxnSource.SelectedValue;
                                                        policy.lockInDuration = txtInsurLockDuration.Text.Trim();
                                                        policy.lockInInterest = txtInsurLockInterest.Text.Trim();
                                                        policy.updatedBy = strCreatedByEmpId;
                                                        policy.finalHlv = txtFinalHlv.Text.Trim();
                                                        policy.isActivePolicy = int.Parse(ddlIsActivePolicy.SelectedValue);
                                                        bool result = data.AddInsuranceEntry(policy);

                                                        if (result == true)
                                                        {
                                                            showMessage("Insurance data saved successfully..!!", "success");
                                                            resetControls();
                                                            bindInsuranceEntry(strBasicId);
                                                        }
                                                    }
                                                    else { showMessage("Please enter final human life value", "error"); }
                                                }
                                                else { showMessage("Please select transaction source", "error");}
                                            }
                                            else { showMessage("Please select policy owner", "error"); }
                                        }
                                        else { showMessage("Please select policy continue or discontinue", "error"); }
                                    }
                                    else { showMessage("Please enter sum assured amount", "error");  }
                                }
                                else { showMessage("Please enter policy term", "error");  }
                            }
                            else { showMessage("Please enter premium amount", "error");  }
                        }
                        else { showMessage("Please enter policy issue date", "error");  }
                    }
                    else { showMessage("Please select policy provider", "error");  }
                }
                else { showMessage("Please enter policy name.", "error"); }
            }
            else { showMessage("Please select Insurance type", "error"); }
        }

        protected void btnUpdate_Policy_Click(object sender, EventArgs e)
        {
            InsuranceEntry policy = new InsuranceEntry();
            if (Session["InsurEntryID"] != null)
            {
                if (ddlInsurType.SelectedValue != "0")
                {
                    if (txtPolicyName.Text.Trim() != string.Empty)
                    {
                        if (ddlPolicyProvider.SelectedValue != "0")
                        {
                            if (txtPolicyIssueDate.Text.Trim() != string.Empty)
                            {
                                if (txtPremium_amt.Text.Trim() != string.Empty)
                                {
                                    if (txtPolicyTerm.Text.Trim() != string.Empty)
                                    {
                                        if (txtPolicyCover.Text.Trim() != string.Empty)
                                        {

                                            if (ddlPolicyOwner.SelectedValue != "0")
                                            {
                                                if (ddlInsurTrxnSource.SelectedValue != "0")
                                                {
                                                    if (ddlIsActivePolicy.SelectedValue != "-1")
                                                    {
                                                        if (txtFinalHlv.Text.Trim() != "")
                                                        {
                                                            if (ddlInsurType.SelectedValue == "14" && txtInsurCurrVal.Text.Trim() == string.Empty)
                                                            {
                                                                showMessage("Please enter current value of ULIP", "error");
                                                                return;
                                                            }

                                                            policy.basicId = strBasicId;
                                                            policy.planId = strPlanId;
                                                            policy.Id = Convert.ToString(Session["InsurEntryID"]);
                                                            policy.AssetName = txtPolicyName.Text.Trim();
                                                            policy.AssetTypeId = ddlInsurType.SelectedValue;
                                                            policy.policyIssueDt = txtPolicyIssueDate.Text.Trim();
                                                            policy.policyNo = txtPolicy_no.Text.Trim();
                                                            policy.policyPartnerId = ddlPolicyProvider.SelectedValue;
                                                            policy.policyPremium = txtPremium_amt.Text.Trim();
                                                            policy.policySumAssured = txtPolicyCover.Text.Trim();
                                                            policy.policyTerm = txtPolicyTerm.Text.Trim();
                                                            policy.currentValue = txtInsurCurrVal.Text.Trim();
                                                            policy.policyOwnerBasicId = ddlPolicyOwner.SelectedValue;
                                                            policy.updatedBy = strCreatedByEmpId;
                                                            policy.isActivePolicy = int.Parse(ddlIsActivePolicy.SelectedValue);
                                                            policy.trxnSource = ddlInsurTrxnSource.SelectedValue;
                                                            policy.finalHlv = txtFinalHlv.Text.Trim();
                                                            policy.lockInDuration = txtInsurLockDuration.Text.Trim();
                                                            policy.lockInInterest = txtInsurLockInterest.Text.Trim();
                                                            bool result = data.updateInsuranceEntryById(policy);

                                                            if (result == true)
                                                            {
                                                                showMessage("Insurance data saved successfully..!!", "success");
                                                                resetControls();
                                                                bindInsuranceEntry(strBasicId);
                                                            }
                                                        }
                                                        else { showMessage("Please enter final human life value", "error"); }
                                                    }
                                                    else { showMessage("Please select policy continue or discontinue.", "error");  }
                                                }
                                                else { showMessage("Please select transaction source.", "error"); }

                                            }
                                            else { showMessage("Please select policy owner", "error"); }

                                        }
                                        else { showMessage("Please enter sum assured amount", "error");  }
                                    }
                                    else { showMessage("Please enter policy term", "error"); }
                                }
                                else { showMessage("Please enter premium amount.", "error"); }
                            }
                            else { showMessage("Please enter policy issue date", "error"); }
                        }
                        else { showMessage("Please select policy provider", "error");  }
                    }
                    else { showMessage("Please enter policy name", "error");  }
                }
                else { showMessage("Please select Insurance type", "error"); }
            }
        }

        protected void btnOtherAssetSave_Click(object sender, EventArgs e)
        {
            LoanEntry loan = new LoanEntry();
            if (ddlAssettype.SelectedValue != "0")
            {
                if (ddlLiquidityType.SelectedValue != "0")
                {

                    if (txtAssetname.Text.Trim() != "")
                    {
                        if (txtCurntvalue.Text.Trim() != "")
                        {
                            if (ddlAssetTrxnSource.SelectedValue != "0")
                            {
                                loan.basicId = strBasicId;
                                loan.planId = strPlanId;
                                loan.AssetName = txtAssetname.Text.Trim();
                                loan.amount = txtCurntvalue.Text.Trim();
                                loan.AssetTypeId = ddlAssettype.SelectedValue;
                                loan.updatedBy = strCreatedByEmpId;
                                loan.LiquidityType = ddlLiquidityType.SelectedValue;
                                loan.lockInDuration = txtLockInDuration.Text.Trim();
                                loan.lockInInterest = txtLockInInterest.Text.Trim();
                                loan.trxnSource = ddlAssetTrxnSource.SelectedValue;
                                bool result = data.AddOtherAssetEntry(loan);

                                if (result == true)
                                {
                                    showMessage("Other Asset data saved successfully..!!", "success");
                                    resetControls();
                                    bindOtherAssetEntry(strBasicId);
                                }
                            }
                            else { showMessage("Please select transaction source", "error");}
                        }
                        else { showMessage("Please enter current valuation", "error");  }
                    }
                    else { showMessage("Please enter asset name", "error");  }

                }
                else { showMessage("Please select liquidity type of Asset", "error"); }
            
            }
            else { showMessage("Please select Other Asset type", "error"); }
        }
        
        protected void btnOtherAssetUpdate_Click(object sender, EventArgs e)
        {
            LoanEntry loan = new LoanEntry();
            if (Session["OthAsstEntryID"] != null)
            {
                if (ddlAssettype.SelectedValue != "0")
                {
                    if (ddlLiquidityType.SelectedValue != "0")
                    {

                       if (txtAssetname.Text.Trim() != "")
                        {
                            if (txtCurntvalue.Text.Trim() != "")
                            {
                                if (ddlAssetTrxnSource.SelectedValue != "0")
                                {
                                    loan.basicId = strBasicId;
                                    loan.Id = Convert.ToString(Session["OthAsstEntryID"]);
                                    loan.AssetName = txtAssetname.Text.Trim();
                                    loan.amount = txtCurntvalue.Text.Trim();
                                    loan.AssetTypeId = ddlAssettype.SelectedValue;
                                    loan.LiquidityType = ddlLiquidityType.SelectedValue;
                                    loan.updatedBy = strCreatedByEmpId;
                                    loan.lockInDuration = txtLockInDuration.Text.Trim();
                                    loan.lockInInterest = txtLockInInterest.Text.Trim();
                                    loan.trxnSource = ddlAssetTrxnSource.SelectedValue;
                                    bool result = data.updateOtherAssetEntryById(loan);

                                    if (result == true)
                                    {
                                        showMessage("Other Asset data updated successfully..!!", "success");
                                        resetControls();
                                        bindOtherAssetEntry(strBasicId);
                                    }
                                }
                                else { showMessage("Please select transaction source", "error"); }
                            }
                        else { showMessage("Please enter current valuation", "error");}
                    }
                    else { showMessage("Please enter asset name", "error");  }

                    }
                    else { showMessage("Please select liquidity type of Asset", "error"); }
                }
                else { showMessage("Please select Other Asset type", "error"); }
            }
        }

        protected void lnkOtherAssetDelete_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string strOthAssetId = Convert.ToString((item.FindControl("lblOthAssetId") as Label).Text);
            bool res = data.removeLoanEntryById(strOthAssetId, strBasicId);
            showMessage("Other asset record removed successfully..!!", "success");
          
            this.bindOtherAssetEntry(strBasicId);
            resetControls();
        }

        protected void lnkOtherAssetEdit_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string OtherAssetId = Convert.ToString((item.FindControl("lblOthAssetId") as Label).Text);
            DataTable dtOthAssets = data.getOtherAssetEntryById(OtherAssetId, strBasicId);
            if (dtOthAssets != null)
            {
                if (dtOthAssets.Rows.Count > 0)
                {
                    ddlAssettype.SelectedValue = Convert.ToString(dtOthAssets.Rows[0]["AssetTypeId"]);
                    txtAssetname.Text = Convert.ToString(dtOthAssets.Rows[0]["AssetName"]);
                    txtCurntvalue.Text = Convert.ToString(dtOthAssets.Rows[0]["currValuation"]);
                    ddlLiquidityType.SelectedValue = Convert.ToString(dtOthAssets.Rows[0]["Liquidity"]);
                    txtLockInDuration.Text = Convert.ToString(dtOthAssets.Rows[0]["lockInDuration"]);
                    txtLockInInterest.Text = Convert.ToString(dtOthAssets.Rows[0]["lockInInterest"]);
                    ddlAssetTrxnSource.SelectedValue = Convert.ToString(dtOthAssets.Rows[0]["trxnSource"]);

                    Session["OthAsstEntryID"] = OtherAssetId;
                    btnOtherAssetSave.Visible = false;
                    btnOtherAssetUpdate.Visible = true;
                    btnOtherAssetCancel.Visible = true;
                }

            }
        }

        protected void btnMfSave_Click(object sender, EventArgs e)
        {
            MFEntry mf = new MFEntry();

            if (ddlTransType.SelectedValue != "0")
            {
                if (hdnSchemeid.Value != string.Empty)
                {
                    
                        if (txtTrxnAmount.Text.Trim() != string.Empty)
                        {
                            if (txtFolioNo.Text.Trim() != string.Empty)
                            {
                            if (ddlMfTrxnSource.SelectedValue != "0")
                            {

                                if (txtCurrValuation.Text.Trim() != string.Empty)
                                {

                                    if (ddlTransType.SelectedValue == "2" && !data.checkExpenseMFSipAmount(strBasicId, strPlanId, txtTrxnAmount.Text.Trim()))
                                    {
                                        showMessage("your sip transaction amount does not match with expense sip list", "error");
                                        return;
                                    }

                                    mf.basicId = strBasicId;
                                    mf.planId = strPlanId;
                                    mf.AssetName = txtSchemeName.Text.Trim();
                                    mf.schemeId = hdnSchemeid.Value;
                                    mf.amount = txtTrxnAmount.Text.Trim();
                                    mf.currentValue = txtCurrValuation.Text.Trim();
                                    mf.sipMdate = ddlSipMDate.SelectedValue != "0" ? ddlSipMDate.SelectedValue : null;
                                    mf.sipStartDate = txtSipStartDate.Text.Trim();
                                    mf.folioNo = txtFolioNo.Text.Trim();
                                    mf.updatedBy = strCreatedByEmpId;
                                    mf.trxnSource = ddlMfTrxnSource.SelectedValue;
                                    bool result = data.AddMFEntry(mf);

                                    if (result == true)
                                    {
                                        showMessage("scheme data saved successfully..!!", "success");
                                        
                                        resetControls();
                                        bindMFEntry(strBasicId);
                                    }


                                }
                                else { showMessage("Please enter current valuation.", "error"); }
                            }
                            else { showMessage("Please select transaction source.", "error"); }

                            }
                            else { showMessage("Please enter folio number", "error");  }
                        }
                        else { showMessage("Please enter  transaction amount", "error"); }
                   
                }
                else { showMessage("Please enter scheme name.", "error");  }
            }
            else { showMessage("Please select investment type.", "error"); }
        }

        protected void btnMfUpdate_Click(object sender, EventArgs e)
        {
            MFEntry mf = new MFEntry();
            if (Session["MFEntryID"] != null)
            {
                if (ddlTransType.SelectedValue != "0")
                {
                    if (hdnSchemeid.Value != string.Empty)
                    {
                            if (txtTrxnAmount.Text.Trim() != string.Empty)
                            {
                                if (txtFolioNo.Text.Trim() != string.Empty)
                                {
                                    if (ddlMfTrxnSource.SelectedValue != "0")
                                    {
                                        if (txtCurrValuation.Text.Trim() != string.Empty)
                                        {

                                            mf.basicId = strBasicId;
                                            mf.Id = Convert.ToString(Session["MFEntryID"]);
                                            mf.AssetName = txtSchemeName.Text.Trim();
                                            mf.transType = ddlTransType.SelectedValue;
                                            mf.schemeId = hdnSchemeid.Value;
                                            mf.amount = txtTrxnAmount.Text.Trim();
                                            mf.currentValue = txtCurrValuation.Text.Trim();
                                            mf.sipMdate = ddlSipMDate.SelectedValue != "0" ? ddlSipMDate.SelectedValue : null;
                                            mf.sipStartDate = txtSipStartDate.Text.Trim();
                                            mf.folioNo = txtFolioNo.Text.Trim();
                                            mf.updatedBy = strCreatedByEmpId;
                                            mf.trxnSource = ddlMfTrxnSource.SelectedValue;

                                            bool result = data.updateMFEntryById(mf);

                                            if (result == true)
                                            {
                                                showMessage("scheme data saved successfully..!!", "success");
                                               
                                                resetControls();
                                                bindMFEntry(strBasicId);
                                            }

                                        }
                                        else { showMessage("Please enter current valuation", "error");  }
                                    }
                                else { showMessage("Please select transaction source", "error"); }
                            }
                                else { showMessage("Please enter folio number.", "error"); }
                            }
                            else { showMessage("Please enter  transaction amount", "error");  }
                      
                    }
                    else { showMessage("Please enter scheme name", "error"); }
                }
                else { showMessage("Please select investment type.", "error"); }
            }
        }

        protected void lnkMFDelete_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string strId = Convert.ToString((item.FindControl("lblMFId") as Label).Text);
            bool res = data.removeMFEntryById(strId, strBasicId);
            showMessage("Mutual fund Investment removed successfully..!!", "success");
            this.bindMFEntry(strBasicId);
            resetControls();
        }

        protected void lnkMFEdit_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string MFId = Convert.ToString((item.FindControl("lblMFId") as Label).Text);
            DataTable dtMFList = data.getMFEntryById(MFId, strBasicId);
            if (dtMFList != null)
            {
                if (dtMFList.Rows.Count > 0)
                {
                    ddlTransType.SelectedValue = Convert.ToString(dtMFList.Rows[0]["transType"]);
                    txtSchemeName.Text = Convert.ToString(dtMFList.Rows[0]["AssetName"]);
                    hdnSchemeid.Value = Convert.ToString(dtMFList.Rows[0]["schemeId"]);
                    txtTrxnAmount.Text = Convert.ToString(dtMFList.Rows[0]["amount"]);
                    txtFolioNo.Text = Convert.ToString(dtMFList.Rows[0]["folioNo"]);
                    txtSipStartDate.Text = formatter.enUsDateFormat(Convert.ToString(dtMFList.Rows[0]["sipStartDate"]));
                    ddlSipMDate.SelectedValue = Convert.ToString(dtMFList.Rows[0]["sipMDate"]);
                    txtCurrValuation.Text = Convert.ToString(dtMFList.Rows[0]["currValuation"]);
                    ddlMfTrxnSource.SelectedValue = Convert.ToString(dtMFList.Rows[0]["trxnSource"]);

                    Session["MFEntryID"] = MFId;
                    btnMfSave.Visible = false;
                    btnMfUpdate.Visible = true;
                    btnMfCancel.Visible = true;
                }

            }
        }

        [System.Web.Services.WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string getSchemes(string text)
        {
            List<Schemes> schemeList = new List<Schemes>();
            DataAccess datalayer = new DataAccess();
            DataTable dtSchemes = datalayer.getSchemeListByTextSearch(text);

            if (dtSchemes != null)
            {
                if (dtSchemes.Rows.Count > 0)
                {
                    schemeList = dtSchemes.AsEnumerable().Select(dr => new Schemes
                    {
                        id = Convert.ToString(dr["scheme_Id"]),
                        fundId = Convert.ToString(dr["fund_id"]),
                        schemeName = Convert.ToString(dr["Org_SchemeName"])

                    }).ToList();
                }
            }

            return JsonConvert.SerializeObject(schemeList);
        }

        protected void ddlInsurType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlInsurType.SelectedValue == "14")
            {
                insCurrValPnl.Visible = true;
                txtInsurCurrVal.Visible = true;
                insLockDurationPnl.Visible = true;
                insLockInterestPnl.Visible = true;


            }
            else
            {
                insCurrValPnl.Visible = false;
                txtInsurCurrVal.Visible = false;
                insLockDurationPnl.Visible = false;
                insLockInterestPnl.Visible = false;
            }
        }

        protected void btnLoanCancel_Click(object sender, EventArgs e)
        {
            resetControls();

        }

        protected void btnInsurCancel_Click(object sender, EventArgs e)
        {
            resetControls();
        }

        protected void btnOtherAssetCancel_Click(object sender, EventArgs e)
        {
            resetControls();
        }

        protected void btnMfCancel_Click(object sender, EventArgs e)
        {
            resetControls();
        }
        //protected void lnkMapInvstDelete_Click(object sender, EventArgs e)
        //{
        //    //Find the reference of the Repeater Item.
        //    RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
        //    string mappedInvstId = Convert.ToString((item.FindControl("lblMappedInvstId") as Label).Text);
        //    bool res = data.removeCashflowOutById(mappedInvstId, strBasicId, strPlanId);
        //    lblMsg.Text = "Investment mapped removed successfully..!!";
        //    lblMsg.ForeColor = System.Drawing.Color.Green;
        //    lblMsg.Visible = true;
        //    this.bindInvestments(strBasicId);
        //    this.bindMappedInvestments(strBasicId);
        //}

        //protected void lnkMapLoanOthAst_Click(object sender, EventArgs e)
        //{
        //    CashflowOut invstmt = new CashflowOut();


        //    //Find the reference of the Repeater Item.
        //    RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
        //    string strAssetId = Convert.ToString((item.FindControl("lblAssetId") as Label).Text);
        //    string strAssetTypeId = Convert.ToString((item.FindControl("lblAssetTypeId") as Label).Text);
        //    string strAmount = Convert.ToString((item.FindControl("lblAmount") as Label).Text);

        //    invstmt.amount = strAmount;
        //    invstmt.basicId = strBasicId;
        //    invstmt.assetId = strAssetId;
        //    invstmt.assetType = strAssetTypeId;
        //    invstmt.planId = strPlanId;
        //    invstmt.createdBy = strCreatedByEmpId;
        //    invstmt.updatedBy = strCreatedByEmpId;

        //    bool resp = data.MapToCashOut(invstmt);
        //    if (resp)
        //    {
        //        this.bindLoanAndOtherAssets(strBasicId);
        //        this.bindMappedLoanOtherAssets(strBasicId);
        //    }

        //}

        //protected void lnkMapLoanAssetDelete_Click(object sender, EventArgs e)
        //{
        //    //Find the reference of the Repeater Item.
        //    RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
        //    string cashflowOutId = Convert.ToString((item.FindControl("lblMappedLoanAsstId") as Label).Text);
        //    bool res = data.removeCashflowOutById(cashflowOutId, strBasicId, strPlanId);
        //    lblMsg.Text = "CashFlowOut deleted successfully..!!";
        //    lblMsg.ForeColor = System.Drawing.Color.Green;
        //    lblMsg.Visible = true;
        //    this.bindLoanAndOtherAssets(strBasicId);
        //    this.bindMappedLoanOtherAssets(strBasicId);
        //}

        //protected void lnkInsuranceMap_Click(object sender, EventArgs e)
        //{
        //    CashflowOut invstmt = new CashflowOut();


        //    //Find the reference of the Repeater Item.
        //    RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
        //    string strAssetId = Convert.ToString((item.FindControl("lblAssetId") as Label).Text);
        //    string strAssetTypeId = Convert.ToString((item.FindControl("lblAssetTypeId") as Label).Text);
        //    string strAmount = Convert.ToString((item.FindControl("lblAmount") as Label).Text);

        //    invstmt.amount = strAmount;
        //    invstmt.basicId = strBasicId;
        //    invstmt.assetId = strAssetId;
        //    invstmt.assetType = strAssetTypeId;
        //    invstmt.planId = strPlanId;
        //    invstmt.createdBy = strCreatedByEmpId;
        //    invstmt.updatedBy = strCreatedByEmpId;

        //    bool resp = data.MapToCashOut(invstmt);
        //    if (resp)
        //    {
        //        this.bindInsurance(strBasicId);
        //        this.bindMappedInsurance(strBasicId);
        //    }
        //}

        //protected void lnkDeleteMappedInsurance_Click(object sender, EventArgs e)
        //{
        //    //Find the reference of the Repeater Item.
        //    RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
        //    string cashflowOutId = Convert.ToString((item.FindControl("lblId") as Label).Text);
        //    bool res = data.removeCashflowOutById(cashflowOutId, strBasicId, strPlanId);
        //    lblMsg.Text = "Insurance mapping removed successfully..!!";
        //    lblMsg.ForeColor = System.Drawing.Color.Green;
        //    lblMsg.Visible = true;

        ////}


        ////void bindMappedInvestments(string basicid)
        ////{
        ////    dtInvestments = data.getAllMappedInvstmtByBasicId(basicid, strPlanId);
        ////    rptrMappedInvstm.DataSource = dtInvestments;
        ////    rptrMappedInvstm.DataBind();
        ////}
        ////void bindLoanAndOtherAssets(string basicid)
        ////{
        ////    dtInvestments = data.getLoanAndOtherAssetByBasicId(basicid, strPlanId);
        ////    rptrLoanOtherAsst.DataSource = dtInvestments;
        ////    rptrLoanOtherAsst.DataBind();
        ////}
        ////void bindMappedLoanOtherAssets(string basicid)
        ////{
        ////    dtInvestments = data.getAllMappedLoanAssetByBasicId(basicid, strPlanId);
        ////    rptrMappedLoanAssets.DataSource = dtInvestments;
        ////    rptrMappedLoanAssets.DataBind();
        ////}
        ////void bindInsurance(string basicid)
        ////{
        ////    dtInvestments = data.getAllInsuranceByBasicId(basicid, strPlanId);
        ////    rptrInsuranceRecuurFlow.DataSource = dtInvestments;
        ////    rptrInsuranceRecuurFlow.DataBind();
        ////}
        ////void bindMappedInsurance(string basicid)
        ////{
        ////    dtInvestments = data.getAllInsuranceMappedById(basicid, strPlanId);
        ////    rptrMappedInsurance.DataSource = dtInvestments;
        ////    rptrMappedInsurance.DataBind();
        //}



        //protected void btnAddSipExternal_Click1(object sender, EventArgs e)
        //{
        //    Response.Redirect("https://workpoint.fincart.com/MICS/MutualFund_Asset");
        //}

        //protected void btnAddEmiExternal_Click1(object sender, EventArgs e)
        //{
        //    Response.Redirect("https://workpoint.fincart.com/MICS/Loan_Oth_Asset");
        //}

        //protected void btnAddInsurExternal_Click1(object sender, EventArgs e)
        //{
        //    Response.Redirect("https://workpoint.fincart.com/MICS/Insurance_Asset");
        //}
        void showMessage(string msg, string type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "swal({text: '" + msg + "',icon: '" + type + "'});", true);
        }



    }
}