﻿using FinFinancialPlan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using data.model;
using System.Data;

using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{
    public partial class Coverpageofplan : System.Web.UI.Page
    {
        Dictionary<string, string> clintdic;
        DataAccess data = new DataAccess();
        DataTable dtCashInFlow;

        public string strBasicId = string.Empty;
        
        public string strUserId = string.Empty;
        string strToken = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            strToken = Request.QueryString["tk"];


            if (Session["clientdata"] != null)
            {
                clintdic = (Dictionary<string, string>)Session["clientdata"];
                strUserId = clintdic["userid"];
                strBasicId = clintdic["basicid"];
            }
            else if (!string.IsNullOrWhiteSpace(strToken))
            {
                //basicid | planid
                string strDecrypt = CryptorEngine.Decrypt(strToken, false);
                string[] arrData = strDecrypt.Split('|');
                if (arrData.Length > 1)
                {
                    DataTable dtPlanMasterDtls;

                    dtPlanMasterDtls = data.getCurrentPlanMasterDtlsByBasicId(arrData[0]);

                    if (dtPlanMasterDtls.Rows.Count > 0)
                    {
                        strUserId = Convert.ToString(dtPlanMasterDtls.Rows[0]["userid"]);
                        clintdic = new Dictionary<string, string>();
                        clintdic.Add("basicid", Convert.ToString(dtPlanMasterDtls.Rows[0]["basicid"]));
                        clintdic.Add("userid", Convert.ToString(dtPlanMasterDtls.Rows[0]["userid"]));
                        clintdic.Add("Planname", Convert.ToString(dtPlanMasterDtls.Rows[0]["plan_name"]));
                        clintdic.Add("planid", Convert.ToString(dtPlanMasterDtls.Rows[0]["id"]));
                        clintdic.Add("createdby", Convert.ToString(dtPlanMasterDtls.Rows[0]["empid"]));
                        clintdic.Add("lastWorkingPage", Convert.ToString(dtPlanMasterDtls.Rows[0]["lastPageWorking"]));

                        Session["clientdata"] = clintdic;
                        strUserId = clintdic["userid"];
                        strBasicId = clintdic["basicid"];
                    }
                }
                else { Response.Redirect("notfound.html");  }
            }
            else { Response.Redirect("notfound.html"); }

            dtCashInFlow = data.getclientnamebybasicid(strBasicId);
            string name =dtCashInFlow.Rows[0][0].ToString();
            ClientName.Text = name;

            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);
        }

        protected void PersonalDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("PersonalDetailspage.aspx");
        }
    }
}