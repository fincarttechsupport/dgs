﻿using data.model;
using FinFinancialPlan;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;
using System.Web.UI;
namespace FinFinancialPlan
{
    public partial class Default : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
        DataTable dtpersonalFlow;
        DataTable dtPlanMasterDtls;
        public string strBasicId = "";
        public string strUserId = "";
        public string planname = "";
        public string plainidd;
        public string createdby = "";
        public string gender;
    
        DataTable dtCashOutFlow;
        Dictionary<string, string> clientdict;

        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.QueryString["bid"] != string.Empty && Request.QueryString["bid"] != null && Request.QueryString["pid"] != string.Empty && Request.QueryString["pid"] != null))
            {
                strBasicId = Request.QueryString["bid"];
                plainidd = Request.QueryString["pid"];

                dtPlanMasterDtls = data.getPlanMasterDetailsById(strBasicId, plainidd);
                if (dtPlanMasterDtls.Rows.Count > 0)
                {

                    strUserId = Convert.ToString(dtPlanMasterDtls.Rows[0]["userid"]);
                    planname = Convert.ToString(dtPlanMasterDtls.Rows[0]["plan_name"]);
                    createdby = Convert.ToString(dtPlanMasterDtls.Rows[0]["empid"]);

                    clientdict = new Dictionary<string, string>();
                    clientdict.Add("basicid", strBasicId);
                    clientdict.Add("userid", strUserId);
                    clientdict.Add("Planname", planname);
                    clientdict.Add("planid", plainidd);
                    clientdict.Add("createdby", createdby);
                    clientdict.Add("lastWorkingPage", Convert.ToString(dtPlanMasterDtls.Rows[0]["lastPageWorking"]));

                    Session["clientdata"] = clientdict;


                    if (clientdict["lastWorkingPage"] != string.Empty && clientdict["lastWorkingPage"] != "default.aspx")
                    {
                        Response.Redirect(clientdict["lastWorkingPage"]);
                    }
                    else
                    {

                        dtCashOutFlow = data.getclientpersonaldedails(strUserId);
                        member.DataSource = dtCashOutFlow;
                        member.DataBind();
                    }

                }
            }
            else if (((Request.QueryString["bid"] == string.Empty && Request.QueryString["pid"] == string.Empty) || Session["clientdata"] != null))
            {
                clientdict = (Dictionary<string, string>)Session["clientdata"];
                strBasicId = clientdict["basicid"];
                plainidd = clientdict["planid"];

                dtPlanMasterDtls = data.getPlanMasterDetailsById(strBasicId, plainidd);
                if (dtPlanMasterDtls.Rows.Count > 0)
                {

                    strUserId = Convert.ToString(dtPlanMasterDtls.Rows[0]["userid"]);
                    planname = Convert.ToString(dtPlanMasterDtls.Rows[0]["plan_name"]);
                    createdby = Convert.ToString(dtPlanMasterDtls.Rows[0]["empid"]);

                    clientdict = new Dictionary<string, string>();
                    clientdict.Add("basicid", strBasicId);
                    clientdict.Add("userid", strUserId);
                    clientdict.Add("Planname", planname);
                    clientdict.Add("planid", plainidd);
                    clientdict.Add("createdby", createdby);
                    clientdict.Add("lastWorkingPage", Convert.ToString(dtPlanMasterDtls.Rows[0]["lastPageWorking"]));

                    Session["clientdata"] = clientdict;
                    dtCashOutFlow = data.getclientpersonaldedails(strUserId);
                    member.DataSource = dtCashOutFlow;
                    member.DataBind();
                    

                }
            }
            else 
            {
                Response.Redirect("notfound.html");
            }



            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);

           
        }


   

        protected void Nexttocashin_Click(object sender, EventArgs e)
        {
            Response.Redirect("CashIn.aspx");
        }

       




    }
}