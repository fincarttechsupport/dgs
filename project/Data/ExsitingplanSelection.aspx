﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExsitingplanSelection.aspx.cs" Inherits="FinFinancialPlan.ExsitingplanSelection" %>

  <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>

        <title>Fincart</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/" />

        <!-- Bootstrap core CSS -->
        <link href="css/planpage2.css" rel="stylesheet" />
    <link href="css/timeout-dialog.css" rel="stylesheet" />
        <script src="js/timeout-dialog.js"></script>
        <script src="js/session.js"></script>
        <!-- Custom styles for this template -->
        <link href="css/pagedashboard.css" rel="stylesheet" />
        <style>
            .background {
                background-image: url('imagess/Backforplan.png');
                background-repeat: no-repeat;
                /* Full height */
                height: 100%;
                /* Center and scale the image nicely */
                background-repeat: no-repeat;
                background-size: cover;
            }

            .heading {
                text-align: center;
                color: black;
            }

            .img {
                margin-left: 350px;
                border: 0px;
                height: 620px;
            }

            #rptlstofplans {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border: 1px solid #ddd;
                width: 100%;
            }

                #rptlstofplans tr {
                    border: 1px solid #ddd;
                    padding: 0px;
                    padding-bottom: 12px;
                    text-align: left;
                    color: black;
                    text-align: center;
                }

                    #rptlstofplans tr th {
                        border: 1px solid #ddd;
                        padding: 0px;
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        background-color: #4CAF50;
                        color: white;
                        text-align: center;
                    }

                    #rptlstofplans tr td {
                        border: 1px solid #ddd;
                        padding: 0px;
                        padding-bottom: 12px;
                        text-align: left;
                        color: black;
                        text-align: center;
                    }

            inputbuton {
                width: 100%;
                background-color: #4CAF50;
                color: white;
                padding: 14px 20px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }
            testbox {
            
            width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
            
            }

        </style>
    </head>
    <body>
         <form id="form1" runat="server" role="form">

        <h1 class="heading">CLIENT'S EXISTING PLANS</h1>
        <br />

        <asp:Repeater ID="rptlstofplansm" runat="server">
            <HeaderTemplate>
                <table id="rptlstofplans">
                    <tr>
                       
                        <th>PLAN NAME</th>
                        <th>RM NAME</th>
                        <th>CREATED BY</th>                        
                        <th>CREATED</th>
                        <th>LAST UPDATED BY</th>
                        <th>LAST UPDATED</th>
                        <th>UPDATE</th>
                        <th>Email To client</th>
                        <th>DELETE</th>
                        
                        
                         </tr>
            </HeaderTemplate>
            <ItemTemplate>

                <tr>
                  
           

                    <td><%#DataBinder.Eval(Container,"DataItem.plan_name")%></td>
             
                    <td><%#DataBinder.Eval(Container,"DataItem.RM_Name")%></td>
                    <td><%#DataBinder.Eval(Container,"DataItem.Created_by")%></td>
                    <td><%#DataBinder.Eval(Container,"DataItem.Created_date")%></td>
                    <td><%#DataBinder.Eval(Container,"DataItem.Updated_by")%></td>
                    <td><%#DataBinder.Eval(Container,"DataItem.Last_Updateddate")%></td>
                     <td><asp:LinkButton ID="lnkUpdate" Text="Edit" runat="server" OnClick="lnkUpdate_Click" /></td>
                     <td><asp:LinkButton ID="lnkSndPlanEmail" Text="Email to Client" runat="server" OnClick="lnkSndPlanEmail_Click" /></td>
                     <td><asp:LinkButton ID="lnkdelete" Text="Delete" runat="server" OnClick="lnkDelete_Click" OnClientClick="return confirm('Do you want to delete this row?');" /></td>
                     <asp:Label ID="lblId" runat="server" Text='<%# Eval("planid") %>' Visible = "false" />

                </tr>





            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>

        </asp:Repeater>
             <br />
             <br />

              <h3 class="heading">CREATE CLIENT'S NEW PLANS<label><b>Plan Name</b><asp:TextBox ID="txtplanname" runat="server" CssClass="textbox"></asp:TextBox>
        <asp:Button id="btnCreateNewPlan" runat="server" Text="Create" CssClass="inputbuton" OnClick="btnCreateNewPlan_Click" />

                  </label> 
             </h3>
&nbsp;<br /> <br />




</form>
    </body>
    </html>