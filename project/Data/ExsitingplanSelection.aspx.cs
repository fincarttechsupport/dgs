﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinFinancialPlan;
using data.model;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace FinFinancialPlan
{
    public partial class ExsitingplanSelection : System.Web.UI.Page
    {
        Dictionary<string, string> clintdic;
        DataAccess data = new DataAccess();
        DataTable Planlist;
        public string strBasicId = string.Empty;
        public string strempId = string.Empty;
        string strPlanId = string.Empty;
        string strRmEmpId = string.Empty;
        string API_URL_Production = "https://api.fincart.com/";

        protected void Page_Load(object sender, EventArgs e)
        {
            strBasicId = Request.QueryString["bid"];
            strempId = Request.QueryString["Eid"];
            strRmEmpId = Request.QueryString["REID"];


            //strBasicId = "622923";
            //strempId = "FIN18052018023";
            //strRmEmpId = "FIN01032013001";


            if (!string.IsNullOrWhiteSpace(strBasicId) && !string.IsNullOrWhiteSpace(strempId) && !string.IsNullOrWhiteSpace(strRmEmpId))
            {

                Planlist = data.getExistingPlanGeneratebybasicid(strBasicId);
                if (Planlist.Rows.Count >= 1)
                {


                    rptlstofplansm.DataSource = Planlist;
                    rptlstofplansm.DataBind();
                }
            
                
           }
            else
            {   
                Response.Redirect("notfound.html");

            }
            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);
            //Session["Reset"] = true;
            //Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            //SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            //int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            //ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);

        }





        void bindplanExist(string userid)
        {
            Planlist = data.getExistingPlanGeneratebybasicid(strBasicId);
            if (Planlist.Rows.Count >= 1)
            {


                rptlstofplansm.DataSource = Planlist;
                rptlstofplansm.DataBind();
            }
            else
            {

                Response.Write("<script>alert('Basic Id and Not Found.');</script>");
                Response.Redirect("ExsitingplanSelection.aspx");


            }
        }


        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.

            strBasicId = Request.QueryString["bid"];
            strempId = Request.QueryString["Eid"];
            strRmEmpId = Request.QueryString["REID"];
            //strBasicId = "622923";
            //strempId = "FIN18052018023";
            //strRmEmpId = "FIN01032013001";


            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string strPlanId = Convert.ToString((item.FindControl("lblId") as Label).Text);
      

            updateexixtingplan updateplan = new updateexixtingplan();

            updateplan.basicid = strBasicId;
            updateplan.planid = strPlanId;
            updateplan.rmid = strRmEmpId;
            updateplan.updaterid = strempId;
            bool res = data.updateplanlasthandeler(updateplan);
            if (res)
            {

                Response.Redirect("callonepage.aspx?" + "bid=" + strBasicId + "" + "&pid=" + strPlanId + "");

            }
            else     
            {
                Response.Write("<script>alert('Plan Not Found.');</script>");
             
            }

        }



      


        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.

            strBasicId = Request.QueryString["bid"];
            strempId = Request.QueryString["Eid"];
            strRmEmpId = Request.QueryString["REID"];
            //strBasicId = "622923";
            //strempId = "FIN18052018023";
            //strRmEmpId = "FIN01032013001";


            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string strPlanId = Convert.ToString((item.FindControl("lblId") as Label).Text);


            updateexixtingplan updateplan = new updateexixtingplan();

            updateplan.basicid = strBasicId;
            updateplan.planid = strPlanId;
            updateplan.updaterid = strempId;
            bool res = data.deleteplanby_basic_id_and_plan_id(updateplan);
            if (res)
            {
                showMessage("Plan deleted.", "success");


                this.bindplanExist(strBasicId);

            }
            else
            {
                showMessage("Plan Not Found.", "error");
                

            }






        }

        protected void btnCreateNewPlan_Click(object sender, EventArgs e)
        {


            strBasicId = Request.QueryString["bid"];
            strempId = Request.QueryString["Eid"];
            strRmEmpId = Request.QueryString["REID"];
            //strBasicId = "622923";
            //strempId = "FIN18052018023";
            //strRmEmpId = "FIN01032013001";
          updateexixtingplan updateplan = new updateexixtingplan();
            if(txtplanname.Text == null || txtplanname.Text == "")
            {


                showMessage("Please Enter Plan Name", "error");


            }
            else { 
            updateplan.basicid = strBasicId;
            updateplan.planid = strPlanId;
            updateplan.updaterid = strempId;
            updateplan.planname = txtplanname.Text;
            updateplan.rmid = strRmEmpId;
          

            bool res = data.Create_new_planby_basic_id(updateplan);
            if (res)
            {
                strPlanId = data.getplanIdByBasicId(strBasicId).ToString();
                Response.Redirect("callonepage.aspx?" + "bid=" + strBasicId + "" + "&pid=" + strPlanId + "");
            }

            }

        }

        void showMessage(string msg, string type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "swal({text: '" + msg + "',icon: '" + type + "'});", true);
        }

        protected void lnkSndPlanEmail_Click(object sender, EventArgs e)
        {
            strBasicId = Request.QueryString["bid"];
            DataTable dtPlanDetails = new DataTable();

            if (!string.IsNullOrWhiteSpace(strBasicId))
            {
                //Find the reference of the Repeater Item.
                RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
                string strPlanId = Convert.ToString((item.FindControl("lblId") as Label).Text);

                //dtPlanDetails = data.getPlanMasterDetailsById(strBasicId,strPlanId);
                HttpClient httpClient = new HttpClient();
                httpClient.Timeout = TimeSpan.FromMinutes(30);
                //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToString(Session["authToken"]));
                HttpResponseMessage response;
                response = httpClient.GetAsync(API_URL_Production + string.Format("api/v2/fullfinplan/sendplanemail/{0}/{1}", strBasicId, strPlanId)).Result;
                var responseData = response.Content.ReadAsStringAsync().Result;
                ServerResponse srvResponse = JsonConvert.DeserializeObject<ServerResponse>(responseData.Replace("\\", "").ToString());
                if (srvResponse.status == "Success")
                {
                    showMessage("Financial Plan link sent to client email successfully..!!", "success");
                }
                else { showMessage("something went wrong while sending email", "error"); }

            }
            else { showMessage("your session expired. Please open from workpoint again..!!", "error"); }
            
        }
    }
}