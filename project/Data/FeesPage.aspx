﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FeesPage.aspx.cs" Inherits="FinFinancialPlan.FeesPage" MasterPageFile="~/Planmasterpage.Master" %>


<asp:content ID="cover" ContentPlaceHolderID="masterpagecontent" runat="server"> 

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>
    <title>Fincart</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/"/>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="css/planpage2.css" rel="stylesheet" /> 

    <!-- Custom styles for this template -->
    <link href="css/pagedashboard.css" rel="stylesheet" />
    <script src="js/session.js"></script>
<style>
.background
{
    background-image:url('imagess/Backforplan.png');
    background-repeat:no-repeat;
     /* Full height */
  height: 100%;

  /* Center and scale the image nicely */
  background-repeat: no-repeat;
  background-size: cover
}

.heading
{
    text-align:center;
    color:black;


}



.img{


    margin-left:210px;

}

    .para {
    
    font-family:'Product Sans';

    font-style:normal;
    color:black;

    font-size:larger;
    
    
    }
    </style>
</head>
<body>

    <h1 class="heading">Fees Structure</h1>
   
    <p class="para">
        <br />
       <b><span class="auto-style1">Financial Planning Fees :</span></b><span class="auto-style1"> <br /><br /> 
We charged you a first year fees of&nbsp; <b><asp:Label ID="Withgst" runat="server"></asp:Label></b>&nbsp; 
           (i.e&nbsp;&nbsp;<b> <asp:Label ID="Withoutgst" runat="server"></asp:Label></b> &nbsp;&nbsp;
            plus GST of 18% as per government regulation for drawing the financial plan.<br />

<b>Transaction Charges For Online Accounts :</b><br />
SEBI guidelines vide CIrcular No."SEBI/IMD/CIR No. 4/168230/09(<b>
    <a href="http://www.sebi.gov.in/circulars/2009/imd_cir_3009.pdf">http://www.sebi.gov.in/circulars/2009/imd_cir_3009.pdf</a></b>)" 
           states that the investors may make payment<br /> to the mutual fund distributors directly instead of the mutual
fund houses deducting the same from the scheme invested upon.
We, at FINCART create free online transaction account.<br /> We do not charge any fee for opening the transaction account,<br /> no charge for managing the investments other than the mentioned above. However, We, being the distributors of Mutual Funds, get remunerated by the way of brokerage</span>
         </p>
       


   

</body>
</html>
    </asp:content>