﻿using data.model;
using FinFinancialPlan;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{
    public partial class FeesPage : System.Web.UI.Page
    {
        Dictionary<string, string> clintdic;
        DataAccess data = new DataAccess();
        DataTable dtCashOutFlow;
       // DataTable dtInvestments;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];


                strUserId = clintdic["userid"];

                strBasicId = clintdic["basicid"];
            }
            else { Response.Redirect("nofound.html"); }


            // SESSION POPUP
            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);


            dtCashOutFlow = data.getclientfeesbybasicid(strBasicId);
            if (dtCashOutFlow.Rows.Count != 0)
            {
                string WithGSTAMOUNT = dtCashOutFlow.Rows[0][0].ToString();
                string WithoutGSTAMOUNT = dtCashOutFlow.Rows[0][1].ToString();
                Withoutgst.Text = WithoutGSTAMOUNT;
                Withgst.Text = WithGSTAMOUNT;

            }
            else {

                showMessage("Fees Not Found", "error");
            
            }
        
        
        
        }
        void showMessage(string msg, string type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "swal({text: '" + msg + "',icon: '" + type + "'});", true);
        }

        protected void FeesPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("Content.aspx");
        }
    }
}