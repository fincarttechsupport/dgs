﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GOALBYBASICID.aspx.cs" Inherits="FinFinancialPlan.GOALBYBASICID" MasterPageFile="~/Planmasterpage.Master" %>



<asp:Content ID="cover" ContentPlaceHolderID="masterpagecontent" runat="server">

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>

        <title>Fincart</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/" />

        <!-- Bootstrap core CSS -->
        <link href="css/planpage2.css" rel="stylesheet" />
          <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- Custom styles for this template -->
        <link href="css/pagedashboard.css" rel="stylesheet" />
          <script src="js/session.js"></script>
        <style>
            .background {
                background-image: url('imagess/Backforplan.png');
                background-repeat: no-repeat;
                /* Full height */
                height: 100%;
                /* Center and scale the image nicely */
                background-repeat: no-repeat;
                background-size: cover;
            }

            .heading {
                text-align: center;
                color: black;
            }

            .img {
                margin-left: 350px;
                border: 0px;
                height: 620px;
            }

            #rptrLinkInvst {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border: 1px solid #ddd;
                width: 100%;
            }

                #rptrLinkInvst tr {
                    border: 1px solid #ddd;
                    padding: 0px;
                    padding-bottom: 12px;
                    text-align: left;
                    color: black;
                    text-align: center;
                }

                    #rptrLinkInvst tr th {
                        border: 1px solid #ddd;
                        padding: 0px;
                        padding-top: 12px;
                        padding-bottom: 12px;
                        text-align: left;
                        background-color: #4CAF50;
                        color: white;
                        text-align: center;
                    }

                    #rptrLinkInvst tr td {
                        border: 1px solid #ddd;
                        padding: 0px;
                        padding-bottom: 12px;
                        text-align: left;
                        color: black;
                        text-align: center;
                    }
        </style>
    </head>
    <body>


        <h1 class="heading">YOUR DREAMS OUR PRIORITY</h1>
        <br />

        <asp:Repeater ID="rptrLinkInvstm" runat="server">
            <HeaderTemplate>
                <table id="rptrLinkInvst">
                    <tr>
                        <th>Priority</th>
                        <th>IMAGE</th>
                        <th>Goal Name</th>
                        <th>Time Horizon</th>
                        <th>CURRENT COST</th>
                        <th>REQUIREMENT AT MATURITY</th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>

                <tr>

                    <td><%#DataBinder.Eval(Container,"DataItem.goalPriority")%></td>
                    <td>
                        <img runat="server" style="height: 40px; width: 40px;" src='<%# getgoalimage(Eval("GoalCode").ToString())%>' /></td>
                    <td><%#DataBinder.Eval(Container,"DataItem.GName")%></td>
             
                    <td><%#DataBinder.Eval(Container,"DataItem.TimeHorizon")%></td>
                    <td><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.currentCost")))%></td>
                    <td><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.futureCost")))%></td>
                </tr>

            </ItemTemplate>

            <FooterTemplate>
                </table>
            </FooterTemplate>

        </asp:Repeater>






    </body>
    </html>
</asp:Content>
