﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinFinancialPlan;
using data.model;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
namespace FinFinancialPlan
{
    public partial class GOALBYBASICID : System.Web.UI.Page
    {

        Dictionary<string, string> clintdic;
        DataAccess data = new DataAccess();
        DataTable dtCashOutFlow;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
        public  string imagepath;
        string goal;
        string strPlanId = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["clientdata"] != null)
            {
                clintdic = (Dictionary<string, string>)Session["clientdata"];
                strUserId = clintdic["userid"];
                strBasicId = clintdic["basicid"];
                strPlanId = clintdic["planid"];
            }
            else { Response.Redirect("notfound.html"); }


            if (!IsPostBack)
            {
                
						

                dtCashOutFlow = data.getclientgoalForPlanGeneratebybasicid(strBasicId, strPlanId);
                rptrLinkInvstm.DataSource = dtCashOutFlow;

                getgoalimage(goal);


                rptrLinkInvstm.DataBind();
            }
            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);
        }

        public string getgoalimage(string goalcode) 
        {
            
            switch (goalcode)
            {
                case "FG1":
                    imagepath = "~/imagess/GoalICON/bike.png";
                    break;
                case "FG2":
                    imagepath = "~/imagess/GoalICON/sabbatical.png";
                    break;
                case "FG3":
                    imagepath = "~/imagess/GoalICON/car.png";
                    break;
                case "FG4":
                    imagepath = "~/imagess/GoalICON/house.png";
                    break;
                case "FG5":
                    imagepath = "~/imagess/GoalICON/Study.png";
                    break;
                case "FG6":
                    imagepath = "~/imagess/GoalICON/Travel.png";
                    break;
                case "FG7":
                    imagepath = "~/imagess/GoalICON/child-marriage-min.png";
                    break;
                case "FG8":
                    imagepath = "~/imagess/GoalICON/emergrency.png";
                    break;
                case "FG9":
                    imagepath = "~/imagess/GoalICON/retirement.png";
                    break;
                case "FG10":
                    imagepath = "~/imagess/GoalICON/sabbatical.png";
                    break;
                case "FG12":
                    imagepath = "~/imagess/GoalICON/child-education.png";
                    break;
                case "FG13":
                    imagepath = "~/imagess/GoalICON/child-marriage.png";
                    break;
                case "FG14":
                    imagepath = "~/imagess/GoalICON/other-goal-min.png";

                    break;
                case "FG17":
                    imagepath = "~/imagess/GoalICON/emergrency.png";
                    break;
   
                default:
                    imagepath = "~/imagess/GoalICON/other-goal-min.png";

                    break;


            }

            return imagepath;
        
        }

        protected void Realitycheck_Click(object sender, EventArgs e)
        {
            Response.Redirect("Realitycheckk.aspx");
        }
    }
}