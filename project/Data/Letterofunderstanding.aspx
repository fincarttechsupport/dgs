﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Letterofunderstanding.aspx.cs" Inherits="FinFinancialPlan.Letterofunderstanding"  MasterPageFile="~/Planmasterpage.Master" %>


<asp:content ID="cover" ContentPlaceHolderID="masterpagecontent" runat="server"> 

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>
    <title>Fincart</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/"/>

    <!-- Bootstrap core CSS -->
    <link href="css/planpage2.css" rel="stylesheet" /> 

    <!-- Custom styles for this template -->
    <link href="css/pagedashboard.css" rel="stylesheet" />
      <script src="js/session.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
.background
{
    background-image:url('imagess/Backforplan.png');
    background-repeat:no-repeat;
     /* Full height */
  height: 100%;

  /* Center and scale the image nicely */
  background-repeat: no-repeat;
  background-size: cover
}

.heading
{
    text-align:center;
    color:black;


}



.img{


    margin-left:210px;

}

    .para {
    
    font-family:'Product Sans';

    font-style:normal;
    color:black;

    font-size:larger;
    
    
    }
    .auto-style1 {
        font-family: 'Product Sans';
        font-style: normal;
        color: black;
        font-size: large;
        text-align: justify;
    }
    </style>
</head>
<body>

    <h1 class="heading">Letter of understanding</h1>
  
    <p class="auto-style1">
       
        

        Dear&nbsp;<b><asp:Label ID="Salutation" runat="server"></asp:Label><asp:Label ID="Name" runat="server"></asp:Label></b> 
         </p>
    <p class="auto-style1">
       
        

        <br />
Greetings!<br /><br />
We are pleased to drawdown your financial plan. We have taken utmost care in making this plan towards a realistic & practical approach from execution
perspective.<br />
I, as a clients understand you and agree to the terms of this agreement:<br />
&#8594; That FINCART’s primary role is that of an advisor, having said that FINCART has the complete bouquet of wealth management products and help me
transact and execute my plan.<br />
&#8594; That FINCART as an advisor is bound by professional secrecy and may not disclose any of my confidential information without my written consent unless
required to do so by law.<br />
&#8594; That I agree to provide all necessary information related to personal finance that is required for drawing my financial & investment plan.<br />
&#8594; That I am under no obligation to act on the recommendations presented in this financial plan and that I am fully responsible for all decisions related to the
advice given by FINCART, as an advisor.<br />
&#8594; That FINCART will only execute the advice those are clearly explained and understood by me<br />
&#8594; That as a practice – FINCART as an advisor provides written recommendations/ suggestions. Having said that the verbal conversations over phone are
 generally recorded and the audio link is shared on request.<br />
I, as a client, ensure that the same doesn’t get missed and should remind before the start of the
recommendations.<br />
All information received from FINCART and all recommendations made to my client will be regarded by both parties as confidential.
The detail of the fees charged is stated in the next page. 
         </p>
       


</body>
</html>
    </asp:content>