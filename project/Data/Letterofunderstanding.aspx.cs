﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinFinancialPlan;
using data.model;
using System.Data;
using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{

    public partial class Letterofunderstanding : System.Web.UI.Page
    {

        Dictionary<string, string> clintdic;
        DataAccess data = new DataAccess();
        DataTable dtCashOutFlow;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
             if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];


                strUserId = clintdic["userid"];

                strBasicId = clintdic["basicid"];
            }
            else { Response.Redirect("notfound.html"); }
			

            dtCashOutFlow = data.getclientNameandSalutation(strBasicId);
            string name = dtCashOutFlow.Rows[0][0].ToString();
            string Salut = dtCashOutFlow.Rows[0][1].ToString();
            Name.Text = name;
            Salutation.Text = Salut;

            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);
        }

        protected void FeesPage_Click(object sender, EventArgs e)
        {
            Response.Redirect("FeesPage.aspx");
        }
    }
}