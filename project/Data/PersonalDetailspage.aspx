﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PersonalDetailspage.aspx.cs" Inherits="FinFinancialPlan.PersonalDetailspage"  MasterPageFile="~/Planmasterpage.Master" %>
 





<asp:content ID="cover" ContentPlaceHolderID="masterpagecontent" runat="server"> 
     <html>
<head>
        <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>
    <title>Fincart</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/"/>

    <!-- Bootstrap core CSS -->
    <link href="css/planpage2.css" rel="stylesheet" /> 
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Custom styles for this template -->
    <link href="css/pagedashboard.css" rel="stylesheet" />
       <script src="js/session.js"></script>
<style>
.background
{
    background-image:url('imagess/Backforplan.png');
    background-repeat:no-repeat;
     /* Full height */
  height: 100%;

  /* Center and scale the image nicely */
  background-repeat: no-repeat;
  background-size: cover
}

.heading
{
    text-align:center;
    color:black;


}



.img{


    margin-left:210px;

}
    #rptrLinkInvst {
  
        
           font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border: 1px solid #ddd;
  width: 100%;
       
    }
      #rptrLinkInvst tr 
       {
            
            background-color:white;
            color:blue;
            grid-column-gap:inherit;
            padding:2px;
            width:150%;
            text-align:center;
            border:1px solid #ddd;
        
   


       }
       #rptrLinkInvst tr th 
       {
             border: 1px solid #ddd;
  padding: 8px;
            padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
  text-align:center;
   


       }
       #rptrLinkInvst tr td
       { border: 1px solid #ddd;
  padding: 8px;
  color:black;
   


       }
/*table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}*/

</style>
</head>
<body >
   


   

   <div>
       
           <h1 class="heading" >WHAT WE KNOW ABOUT YOU</h1>
       <br />
      



         <asp:Repeater ID="personaldetails" runat="server" >
                    <HeaderTemplate>
                        <table id="rptrLinkInvst">
                            <tr>
                                <th>Name</th>
                                <th>Relation</th>
                                <th>Date Of Birth</th>
                                <th>Designation</th>
                                <th>Company Name</th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
            
                          <tr>
                              
                              <td><%#DataBinder.Eval(Container,"DataItem.Name")%></td>
                              <td><%#DataBinder.Eval(Container,"DataItem.Relation")%></td>
                              <td><%#DataBinder.Eval(Container,"DataItem.Date Of Birth","{0:dd/MM/yyyy}")%></td>
                              <td><%#DataBinder.Eval(Container,"DataItem.Designation")%></td>
                              <td><%#DataBinder.Eval(Container,"DataItem.Company Name")%></td>
                          


                          </tr>

                    </ItemTemplate>


             
                    <FooterTemplate>   </table>
                    </FooterTemplate>    
                    
                </asp:Repeater>
          
   </div>
    

     <br />
 



    </body>
</html>
     </asp:content>