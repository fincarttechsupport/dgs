﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinFinancialPlan;
using data.model;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
namespace FinFinancialPlan
{
    public partial class PersonalDetailspage : System.Web.UI.Page
    {
        Dictionary<string, string> clintdic;
        DataAccess data = new DataAccess();
        DataTable dtCashOutFlow;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
        public string imgpath;
        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];


                strUserId = clintdic["userid"];

                strBasicId = clintdic["basicid"];
            }
            else { Response.Redirect("notfound.html"); }

            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);				

            dtCashOutFlow = data.getclientpersonaldedails(strUserId);
            personaldetails.DataSource = dtCashOutFlow;
            personaldetails.DataBind();

        }

        protected void Letterofunderstanding_Click(object sender, EventArgs e)
        {
            Response.Redirect("Letterofunderstanding.aspx");
        }
    }
}