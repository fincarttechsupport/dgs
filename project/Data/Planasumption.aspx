﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Planasumption.aspx.cs" Inherits="FinFinancialPlan.Planasumption" MasterPageFile="~/Planmasterpage.Master" %>





<asp:content ID="cover" ContentPlaceHolderID="masterpagecontent" runat="server"> 

    <html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>
    <title>Fincart</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/"/>

    <!-- Bootstrap core CSS -->
    <link href="css/planpage2.css" rel="stylesheet" /> 
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Custom styles for this template -->
    <link href="css/pagedashboard.css" rel="stylesheet" />
      <script src="js/session.js"></script>
    <style>
        .background {
            background-image: url('imagess/Backforplan.png');
            background-repeat: no-repeat;
            /* Full height */
            height: 100%;
            /* Center and scale the image nicely */
            background-repeat: no-repeat;
            background-size: cover;
        }

        .heading {
            text-align: center;
            color: black;
        }



        .img {
            margin-left: 350px;
            border: 0px;
            height: 620px;
        }
          #planasamp
        {

                width:10%;
          margin-left:10px;
       


        }
        #planasamp tr td {
            background-color: white;
            color: black;
            grid-column-gap: inherit;
            padding: 2px;
            width: 10%;
            text-align: center;
            border: groove;
            border-radius: 0px;
            border-color: black;
            text-decoration-color: aqua;
         }
    </style>
</head>
<body>



        <div>

            <h1 class="heading">PLAN ASSUMPTIONS</h1>

            <br />
            <br />

            <table id="planasamp" style="height: 346px; width: 998px">
                <tr>
                    <td>Retirement Age</td>
                    <td>
                        <asp:Label runat="server" ID="Retirementage"></asp:Label></td>
                </tr>
                <tr>
                    <td>Expectancy</td>
                    <td>
                        <asp:Label runat="server" ID="lifeexpentency"></asp:Label></td>
                </tr>
                <tr>
                    <td>General Inflation Rate</td>
                    <td>6%</td>
                </tr>
                <tr>
                    <td>House Inflation</td>
                    <td>6%</td>
                </tr>
                <tr>
                    <td>Education Inflation Rate</td>
                    <td>10%</td>
                </tr>
                <tr>
                  <td>Liquid Fund Return</td>
                    <td>7.50%</td>
                </tr>
                <tr>
                    <td>Debt Fund</td>
                    <td>8%</td>
                </tr>
                <tr>
                    <td>Equity Fund</td>
                    <td>12%</td>
                </tr>
                <tr>
                    <td>Balanced Fund</td>
                    <td>10%</td>
                </tr>
            </table>
            <h6 style="color:black; margin-left:10px;"> *None of the returns are guaranteed.</h6> 
 
        </div>

   


</body>
</html>
    </asp:content>


