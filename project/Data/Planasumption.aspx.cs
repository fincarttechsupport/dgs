﻿using FinFinancialPlan;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;
namespace FinFinancialPlan
{
    public partial class Planasumption : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
        DataSet dtCashOutFlow;
        DataTable retirement;
        Dictionary<string, string> clintdic;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
        public string plainid = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["clientdata"] != null)
            {
                clintdic = (Dictionary<string, string>)Session["clientdata"];
                strUserId = clintdic["userid"];
                strBasicId = clintdic["basicid"];
            }
            else {
                Response.Redirect("notfound.html");
            }


            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);					


            dtCashOutFlow = data.getAllRetirement(strBasicId, plainid, strUserId);


            if (dtCashOutFlow.Tables[2].Rows.Count != 0)
            {
                retirement = dtCashOutFlow.Tables[2];


                Retirementage.Text = retirement.Rows[0]["Retirment_age"].ToString();
                lifeexpentency.Text = retirement.Rows[0]["life_Expen"].ToString();
            }
            else
            {

                showMessage("Retirement Age Not Found", "error");

            }


        }
        void showMessage(string msg, string type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "swal({text: '" + msg + "',icon: '" + type + "'});", true);
        }
        protected void Dream_Click(object sender, EventArgs e)
        {
            Response.Redirect("GOALBYBASICID.aspx");
            
        }
    }
}