﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Retirment.aspx.cs" Inherits="FinFinancialPlan.Retirment" MasterPageFile="~/Planmasterpage.Master" %>

<asp:Content ID="cover" ContentPlaceHolderID="masterpagecontent" runat="server">

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>
        <title>Fincart</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/" />

        <!-- Bootstrap core CSS -->
        <link href="css/planpage2.css" rel="stylesheet" />
          <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- Custom styles for this template -->
        <link href="css/pagedashboard.css" rel="stylesheet" />
        <script src="js/session.js"></script>
        <style>
<style>
    .background {
        background-image: url('imagess/Backforplan.png');
        background-repeat: no-repeat;
        /* Full height */
        height: 100%;
        /* Center and scale the image nicely */
        background-repeat: no-repeat;
        background-size: cover;
    }

    .heading {
        text-align: center;
        color: black;
        border-bottom: medium;
        border-color: black;
    }

    .heading2 {
        text-align: center;
        color: black;
        border: 10px;
    }



    .img {
        margin-left: 350px;
        border: 0px;
        height: 620px;
    }

    #Retierment {
         font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border: 1px solid #ddd;
                width: 100%;
    }

      #rptretirement Retierment tr {
            background-color: white;
            color: blue;
            grid-column-gap: inherit;
            padding: 2px;
            width: 150%;
            text-align: center;
            border: groove;
            border-radius: 0px;
            border-color: black;
        }
    table {
    
             font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border: 1px solid #ddd;
                width: 100%;
    
    
    }
           tr th {
                background-color:#4CAF50;
                color: black;
            
                padding: 1px;
                border: groove;
              color:white;
            width:60%;
                font-size: medium;
                font-family: sans-serif;
                          
            }



       tr td {
                background-color: white;
                color: black;
                grid-column-gap: inherit;
                padding: 2px;
                  padding: 1px;
                border: groove;
                text-align: right;
              
                          
            }
    /*table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}*/
</style>
</head>
<body>


 


        <div>
            <h1 class="heading2"><u><b>Retirement Plan</b></u></h1>
           <asp:Repeater ID="rptretirement" runat="server">
               <ItemTemplate>
            <table id="Retierment" runat="server" class="Retierment">
                <tr>
                    <th>Retierment Age</th>

                    <td><%#DataBinder.Eval(Container,"DataItem.Retirment_age")%></td>
                </tr>


                <tr>
                    <th>Life Expectancy</th>


                    <td><%#DataBinder.Eval(Container,"DataItem.life_Expen")%></td>

                </tr>
                <tr>
                    <th>Number of years to reach goal</th>

                    <td><%#DataBinder.Eval(Container,"DataItem.Year_To_reach")%></td>


                </tr>


                <tr>
                    <th>Monthly Expenses in today's terms</th>

                    <td><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.monthal_Expence")))%></td>

                </tr>


                <tr>
                    <th>Monthly Expenses @ retirement</th>

                    <td><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.Monthly_exp_at_ret")))%></td>
                </tr>


                <tr>

                    <th>Corpus Required (At start of goal)</th>

                    <td><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.Corpus")))%></td>

                </tr>

            </table>
               </ItemTemplate>
               </asp:Repeater>

                   <asp:Repeater ID="rptPotentiolFunding" runat="server">
              <HeaderTemplate>
            <table  id="Retierment">                
                <tr><td colspan="3"><h3 style="text-align:center"><b>POTENTIAL FUNDING</b></h3></td></tr>
                <tr>
                   
                    <th style="text-align:center;width:30%;"">Asset Name</th>
                    <th style="text-align:center;width:30%;">Current Value</th>
                    <th style="text-align:center">Future Value</th>
                    </tr>
              
                  </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                         <td style="text-align:center"><%#DataBinder.Eval(Container,"DataItem.assetname")%></td>
                         <td style="text-align:center"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.Amount")))%></td>
                         <td style="text-align:center"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.futurecost")))%></td>
                       </tr>
                   </ItemTemplate> 
                           <FooterTemplate>
                    </table>
                </FooterTemplate>
                       </asp:Repeater>

            <%--Calculation--%>

            <asp:Repeater ID="rptcalculatio" runat="server">
               <ItemTemplate>
            <table id="Retierment" runat="server" class="Retierment">
                <tr>
                    <th>Total Funding</th>

                    <td> <%=formatter.IndianCurrency(prttotalasset.ToString())%></td>
                </tr>


                <tr>
                    <th>Total Deficit</th>


                    <td><%=formatter.IndianCurrency(totaldeficit.ToString())%></td>

                </tr>
                <tr>
                    <th>Additional Monthly Investment Req.</th>

                    <td><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.additional_sip")))%></td>


                </tr>


                  </table>
               </ItemTemplate>
               </asp:Repeater>







</div>
            


</body>
</html>
    </asp:Content>
