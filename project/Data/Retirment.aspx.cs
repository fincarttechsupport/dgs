﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinFinancialPlan;
using data.model;
using System.Data;
using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{
    public partial class Retirment : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
        DataSet dtCashOutFlow;
        DataTable Assettotaltbl;
        DataTable tbldeficet;
        DataTable mediuminvs;
        DataTable dISEXPENSE;
        DataTable sAVEXPENSE;
        Dictionary<string, string> clintdic;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
        public string plainid = string.Empty ;
        public string prttotalasset;
        public int totaldeficit;
        public object obldeficet;
        public object sumObjectmed;
        public string totcorp = string.Empty;
        public string totaldisexpence;
        public int totalmedium;
        public int totalhighinv;
        public int totalowinv;
        public int totalinvestment;
        public object objtotalasset;


        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];


                strUserId = clintdic["userid"];

                strBasicId = clintdic["basicid"];

                plainid = clintdic["planid"];
            }
            else { Response.Redirect("notfound.html"); }


            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);




            dtCashOutFlow = data.getAllRetirement(strBasicId, plainid, strUserId);
            if (dtCashOutFlow.Tables[2].Rows.Count != 0)
            {



                rptretirement.DataSource = dtCashOutFlow.Tables[2];

                rptretirement.DataBind();
            }


            if (dtCashOutFlow.Tables[3].Rows.Count != 0)
            {



                rptPotentiolFunding.DataSource = dtCashOutFlow.Tables[3];

                rptPotentiolFunding.DataBind();
              
                 Assettotaltbl = dtCashOutFlow.Tables[3];
                 objtotalasset = Assettotaltbl.Compute("Sum(Amount)", string.Empty);

                 prttotalasset = objtotalasset.ToString();
                tbldeficet = dtCashOutFlow.Tables[2];

                obldeficet = tbldeficet.Compute("Sum(Corpus)", string.Empty);
                totcorp = obldeficet.ToString();

                if (prttotalasset == null || prttotalasset == "")
                {

                    prttotalasset = "0";
                }

                if (totcorp == null || totcorp == "")
                {

                    totcorp = "0";
                }



                totaldeficit = (Int32.Parse(totcorp) - Int32.Parse(prttotalasset));













                rptcalculatio.DataSource = dtCashOutFlow.Tables[4];

                rptcalculatio.DataBind();
            }
      



        }

        protected void Disclmier_Click(object sender, EventArgs e)
        {
            Response.Redirect("Revisedcashflow.aspx");
        }
    }
}