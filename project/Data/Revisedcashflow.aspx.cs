﻿using FinFinancialPlan;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{
    public partial class Revisedcashflow : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
        DataSet dtCashOutFlow;
        DataTable income;
        DataTable tOTALincome;
        DataTable FIXEDEXPENCE;
        DataTable dISEXPENSE;
        DataTable sAVEXPENSE;
        DataTable goalmapped;
        Dictionary<string, string> clintdic;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
        public string plainid = string.Empty;
        public string totalin;
        public string totalfixedexpence;
        public object sumObject;
        public object sumObjectdis;
        public object sumObjectsaving;
        public object sumofsurplus;
        public string totaldisexpence;
        public string totalsavexpence;
        public string totalsurplus;
        public int netcashflow;
        public int totalout;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];


                strUserId = clintdic["userid"];

                strBasicId = clintdic["basicid"];
                plainid = clintdic["planid"];
            }
            else { Response.Redirect("notfound.html"); }


            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);

            dtCashOutFlow = data.getrevisedCashflowBybasicId(strUserId, strBasicId, plainid);
            tOTALincome = dtCashOutFlow.Tables[4];
            totalin = tOTALincome.Rows[0][0].ToString();
            FIXEDEXPENCE = dtCashOutFlow.Tables[1];
            sumObject = FIXEDEXPENCE.Compute("Sum(Fixed_Expense)", string.Empty);
            totalfixedexpence = sumObject.ToString();
            dISEXPENSE = dtCashOutFlow.Tables[2];
            sumObjectdis = dISEXPENSE.Compute("Sum(discretionay_Expense)", string.Empty);
            totaldisexpence = sumObjectdis.ToString();
            sAVEXPENSE = dtCashOutFlow.Tables[3];
            goalmapped = dtCashOutFlow.Tables[5];
            sumObjectsaving = goalmapped.Compute("Sum(surplusAlloted)", string.Empty);
            totalsavexpence = sumObjectsaving.ToString();

            if (totalfixedexpence == null || totalfixedexpence == "")
            {
                totalfixedexpence = "0";


            }
            if (totalsavexpence == null || totalsavexpence == "")
            {

                totalsavexpence = "0";


            }

            if (totalin == null || totalin == "")
            {

                totalin = "0";

            }

            if (totaldisexpence == null || totaldisexpence == "")
            {

                totaldisexpence = "0";
            }

            totalout = Int32.Parse(totalfixedexpence) + Int32.Parse(totalsavexpence) + +Int32.Parse(totaldisexpence);
            netcashflow = int.Parse(totalin) - totalout;
            income = dtCashOutFlow.Tables[0];
            if (income.Rows.Count >= 1)
            {
                rptrLinkInvstm.DataSource = income;
                rptrLinkInvstm.DataBind();
                Repeater8.DataSource = dISEXPENSE;
                Repeater8.DataBind();
                Repeater9.DataSource = dISEXPENSE;
                Repeater9.DataBind();
                Repeater10.DataSource = income;
                Repeater10.DataBind();
            }
            else
            {
                rptrLinkInvstm.Visible = false;



            }
            if (tOTALincome.Rows.Count >= 1)
            {
                Repeater2.DataSource = dtCashOutFlow.Tables[4];
                Repeater2.DataBind();
            }
            else
            {
                Repeater2.Visible = false;

            }
            if (FIXEDEXPENCE.Rows.Count >= 1)
            {
                Repeater1.DataSource = dtCashOutFlow.Tables[1];
                Repeater1.DataBind();
                Repeater3.DataSource = dtCashOutFlow.Tables[1];
                Repeater3.DataBind();
            }
            else
            {
                Repeater1.Visible = false;
                Repeater3.Visible = false;
            }
            if (dISEXPENSE.Rows.Count >= 1)
            {

                Repeater4.DataSource = dtCashOutFlow.Tables[2];
                Repeater4.DataBind();
                Repeater7.DataSource = dtCashOutFlow.Tables[2];
                Repeater4.DataBind();
            }
            else
            {
                Repeater4.Visible = false;
                Repeater4.Visible = false;
            }
            if (sAVEXPENSE.Rows.Count >= 1)
            {

                Repeater6.DataSource = dtCashOutFlow.Tables[5];
                Repeater6.DataBind();
            }
            else
            {

            }

            if (goalmapped.Rows.Count >= 1)
            {

                assets.DataSource = goalmapped;
                assets.DataBind();


            }


        
    }
        protected void Avaibleresouces_Click(object sender, EventArgs e)
        {
            Response.Redirect("Wayforward.aspx");
        }
    }
}