﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinFinancialPlan;
using data.model;
using System.Data;
using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{
    public partial class Riskprotection : System.Web.UI.Page
    {

        Dictionary<string, string> clintdic;
        DataAccess data = new DataAccess();
        DataTable Alredyhealth;
        DataTable Alreadylife;
        DataTable SuggestHealth;
        DataTable Suggestlife;

        DataSet Dataset;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
        public string imagepath;
        string goal;




        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];


                strUserId = clintdic["userid"];

                strBasicId = clintdic["basicid"];
            }
            else { Response.Redirect("notfound.html"); }
            if (!IsPostBack)


            {


                Dataset = data.getPolicyBybasicId(strBasicId);
                int cont = Dataset.Tables.Count;
                if (cont != 0)
                { 

                             
                Alredyhealth = Dataset.Tables[1];
                    if(Alredyhealth.Rows.Count != 0)
                    { 
                Aldheth.DataSource = Alredyhealth ;
                Aldheth.DataBind();
                    }
                Alreadylife = Dataset.Tables[3];
                if (Alreadylife.Rows.Count != 0)
                {
                    Aldlife.DataSource = Alreadylife;
                    Aldlife.DataBind();
                }
                    SuggestHealth = Dataset.Tables[0];
                    if (SuggestHealth.Rows.Count != 0)
                    {
                        Sugheth.DataSource = SuggestHealth;
                        Sugheth.DataBind();
                    }
                     Suggestlife = Dataset.Tables[2];
                     if (Suggestlife.Rows.Count != 0)
                     {
                         suglife.DataSource = Suggestlife;
                         suglife.DataBind();
                     }




                
                
                }
                else
                {

                    Response.Redirect("Retirment.aspx");
                
                
                }


                
            }

            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);
        }

        protected void Dream_Click(object sender, EventArgs e)
        {
            Response.Redirect("Retirment.aspx");
        }
    }
}