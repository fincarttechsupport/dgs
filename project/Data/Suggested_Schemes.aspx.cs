﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinFinancialPlan;
using data.model;
using System.Data;
using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{
    public partial class Suggested_Schemes : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
        DataSet dtCashOutFlow;
        DataTable Scemetbl;
        Dictionary<string, string> clintdic;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
             public string strPlanId;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];


                strUserId = clintdic["userid"];

                strBasicId = clintdic["basicid"];

                strPlanId = clintdic["planid"];
            }
            else { Response.Redirect("notfound.html"); }

            // SESSION POPUP
            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);
							




            Scemetbl = data.getSelected_sceme_by_basicid_for_plan(strBasicId, strPlanId);
            if (Scemetbl.Rows.Count != 0)
            {

                rptrLinkInvstm.DataSource = Scemetbl;

                rptrLinkInvstm.DataBind();
            }


        }

        protected void Backtodefault_Click(object sender, EventArgs e)
        {
            Response.Redirect("callonepage.aspx?" + "bid=" + strBasicId + "" + "&pid=" + strPlanId + "");



            
        }
    }
}