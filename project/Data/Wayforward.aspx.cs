﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinFinancialPlan;
using data.model;
using System.Data;
using System.Configuration;
using System.Web.Configuration;
namespace FinFinancialPlan
{
    public partial class Wayforward : System.Web.UI.Page
    {
        Dictionary<string, string> clintdic;
        DataAccess data = new DataAccess();
        DataTable Emergency;
        DataTable Goals;
        DataTable SuggestHealth;
        DataTable Suggestlife;
        DataTable Retirement;
        DataTable Estate;

        DataSet Dataset;
        public string strBasicId = string.Empty;
        public string strUserId = string.Empty;
        public string imagepath;
        string goal;
        public string strPlanId;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];

                strPlanId = clintdic["planid"];


                strUserId = clintdic["userid"];

                strBasicId = clintdic["basicid"];
            }
            else { Response.Redirect("notfound.html"); }
            if (!IsPostBack)
            {
                
							
                Dataset = data.getwayforward(strBasicId, strPlanId);
                int cont = Dataset.Tables.Count;
                if (cont != 0)
                {


                    Emergency = Dataset.Tables[0];
                    if (Emergency.Rows.Count != 0)
                    {
                        emergencyrpt.DataSource = Emergency;
                        emergencyrpt.DataBind();
                    }
                    Retirement = Dataset.Tables[4];
                    if (Retirement.Rows.Count != 0)
                    {
                        Ritirement.DataSource = Retirement;
                        Ritirement.DataBind();
                    }
                    SuggestHealth = Dataset.Tables[1];
                    if (SuggestHealth.Rows.Count != 0)
                    {
                        Sugheth.DataSource = SuggestHealth;
                        Sugheth.DataBind();
                    }
                    Suggestlife = Dataset.Tables[2];
                    if (Suggestlife.Rows.Count != 0)
                    {
                        suglife.DataSource = Suggestlife;
                        suglife.DataBind();
                    }

                    Goals = Dataset.Tables[3];
                    if (Suggestlife.Rows.Count != 0)
                    {
                        Goalsrpt.DataSource = Goals;
                        Goalsrpt.DataBind();
                    }




                }
                else
                {

                    Response.Redirect("Retirment.aspx");


                }



            }


            // SESSION POPUP
            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);

        }

        protected void Dream_Click(object sender, EventArgs e)
        {
            Response.Redirect("Suggested_Schemes.aspx");
        }
    }
}