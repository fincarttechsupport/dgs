﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true"  CodeBehind="callonepage.aspx.cs" Inherits="FinFinancialPlan.callonepage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>DGS CALL ONE</title>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>
        <script src="js/session.js"></script>

  <script >
      $(function () {
          debugger;
          $("#datepicker").datepicker({ changeMonth: true, changeYear: true, yearRange: '1950:1995', dateFormat: "yy-mm-dd" });
          $("#txtYChilddob").datepicker({ changeMonth: true, changeYear: true, yearRange: '1980:2020', dateFormat: "yy-mm-dd" });
          $("#txtEChilddob").datepicker({ changeMonth: true, changeYear: true, yearRange: '1980:2020', dateFormat: "yy-mm-dd" });
          $("#txtFdob").datepicker({ changeMonth: true, changeYear: true, yearRange: '1920:2020', dateFormat: "yy-mm-dd" });
          $("#txtMDob").datepicker({ changeMonth: true, changeYear: true, yearRange: '1920:2020', dateFormat: "yy-mm-dd" });
          $("#txtGOALDATE").datepicker({ changeMonth: true, changeYear: true, yearRange: '2020:2100', dateFormat: "yy-mm-dd" });
          $("#txtEdob").datepicker({ changeMonth: true, changeYear: true, yearRange: '1980:2020', dateFormat: "yy-mm-dd" });
          $("#txtspdob").datepicker({ changeMonth: true, changeYear: true, yearRange: '1980:2020', dateFormat: "yy-mm-dd" });

      });


      window.onload = function () {
          var scrollY = parseInt('<%=Request.Form["scrollY"] %>');
          if (!isNaN(scrollY)) {
              window.scrollTo(0, scrollY);
          }
      };
      window.onscroll = function () {
          var scrollY = document.body.scrollTop;
          if (scrollY == 0) {
              if (window.pageYOffset) {
                  scrollY = window.pageYOffset;
              }
              else {
                  scrollY = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
              }
          }
          if (scrollY > 0) {
              var input = document.getElementById("scrollY");
              if (input == null) {
                  input = document.createElement("input");
                  input.setAttribute("type", "hidden");
                  input.setAttribute("id", "scrollY");
                  input.setAttribute("name", "scrollY");
                  document.forms[0].appendChild(input);
              }
              input.value = scrollY;
          }
      };

      </script>
<style type="text/css">
    input:focus,
    select:focus,
    textarea:focus,
    button:focus {
        outline: none;
    }


    #downBtn {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        font-size: 18px;
        border: none;
        outline: none;
        background-color: green;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 4px;
    }

        #downBtn:hover {
            background-color: #555;
        }
    #myBtn {
        display: none;
        position: fixed;
        bottom: 20px;
        right: 30px;
        z-index: 99;
        font-size: 18px;
        border: none;
        outline: none;
        background-color: red;
        color: white;
        cursor: pointer;
        padding: 15px;
        border-radius: 4px;
    }

        #myBtn:hover {
            background-color: #555;
        }




    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        height: 300px;
        border-radius: 50px;
        border-color: Window;
        margin-left: 250px;
        margin-right: 250px;
        margin-top: 40px;
        background-color: #fbc03f;
        background-size: 50px;
        border-width: 2px;
        background-image: linear-gradient(#fbc03f, #ffb020);
    }

        /* On mouse-over, add a deeper shadow */
        .card:hover {
        }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }

    .heading {
        text-align: center;
        font-family: 'Comic Sans MS';
        border-radius: 20px;
    }

    .radio {
        text-align: center;
        color: Background;
        font-family: 'Comic Sans MS';
        border-radius: 340px;
        align-content: center;
        align-items: center;
        align-self: center;
    }

    .button {
        text-align: center;
        color: Background;
        font-family: 'Comic Sans MS';
        background-color: Window;
        border-radius: 40px;
        margin-left: 750px;
    }

    .body {
        /*// background-image: url(https://www.fincart.com/Images/FinancialPlanning-NewLanding/bannerN.png);
          //  background-attachment: fixed;
            background-repeat: no-repeat;*/
        background-color: #0056a7;
        background-image: linear-gradient(#0056a7, white);
    }

    .step {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        height: 90px;
        border-radius: 50px;
        border-color: orange;
        margin-top: 40px;
        background-color: white;
        background-size: 50px;
        margin-left: 280px;
        margin-right: 280px;
    }
    .box {
  width: 40%;
  margin: 0 auto;
  background-color:white;
  padding: 35px;
  border: 2px solid #fff;
  border-radius: 20px/50px;
  background-clip: padding-box;
  text-align: center;
}

    #tblgoal{

        width:60%;
       margin-left:260px;

    }

     #tblgoal tr {
            background-color: white;
            color: blue;
            grid-column-gap: inherit;
            padding: 2px;
            width: 150%;
            text-align: center;
            border: groove;
            border-radius: 0px;
            border-color: black;
        }

            #tblgoal tr th {
             background-color: #4dcdfe26;
    color: black;
    grid-column-gap: inherit;
    padding: 1px;
    border: groove;
    border-radius: 0px;
    border-color: black;
    text-align: center;
    font-size: medium;
    font-family: sans-serif;
            }

            #tblgoal tr td {
                background-color: white;
                color: black;
                grid-column-gap: inherit;
                padding: 2px;
                width: 10%;
                text-align: center;
                border: groove;
                border-radius: 0px;
                border-color: black;
            }


            #tblmemeber
            {

        width:60%;
        margin-left:190px;

    }
                 #tblmemeber tr {
            background-color: white;
            color: blue;
    
            padding: 2px;
            width: 150%;
            text-align: center;
            border: groove;
            border-radius: 0px;
            border-color: black;
        }

            #tblmemeber tr th {
             background-color: #4dcdfe26;
    color: black;

    padding: 1px;
    border: groove;
    border-radius: 0px;
    border-color: black;
    text-align: center;
    font-size: medium;
    font-family: sans-serif;
            }

            #tblmemeber tr td {
                background-color: white;
                color: black;
              
                padding: 2px;
                width: 10%;
                text-align: center;
                border: groove;
                border-radius: 0px;
                border-color: black;
            }

</style>
</head>
    
<body class="body">
      
    <form id="form1" runat="server">
    <div id="prnt">
      <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div align="Right">
            <div style="position:fixed" align="right">
                <img style=" width: 230px; position: fixed;" src="imagess/white_logo.png" />

            </div>
            <br />
            <br />
            <br />
            <br />
           
            <div class="step" id="start">


                <h4 class="heading">
                    Welcome TO Fincart Financial Plan Portal...
                    <br />
                    <br />Lets Begin...
                </h4>
                <br />
            </div>
            <br />
            
            <div class="card" id="divdob">
                <br />
                <br />
                <br />
                <h2 class="heading">What Is Your Date Of Birth?</h2>
                <h2 align="center"></h2>
                <br />
                <br />
                <p align="center" class="radio">
                    <b>Date Of Birth:-</b> <asp:TextBox runat="server"  id="datepicker" class="heading"  ></asp:TextBox>
                    
                </p>
            </div>
        </div>
        <br />
    
        
        <div class="card" id="divGender">
            <br />
            <br />
            <br />
            <h2 class="heading">Gender</h2>
            <br />
            <br />

            <p align="center" class="radio">
                <asp:RadioButton runat="server" id="rdbMale"  GroupName="GEN"  AutoPostBack="true" OnCheckedChanged="Parent_changed" /><b>Male</b>
                <asp:RadioButton runat="server" id="rdbfemale"  GroupName="GEN"  AutoPostBack="true" OnCheckedChanged="Parent_changed" /> <b>Female</b>
            </p>

        </div>
        <br />
            <div class="card" id="divMaritalstatus">
            <br />
            <br />
            <br />
            <h2 class="heading">Are You Married?</h2>
            <br />
            <br />

            <p align="center" class="radio">
                <asp:RadioButton runat="server" id="rbdYesmarried"  GroupName="MARITAL"  AutoPostBack="true" OnCheckedChanged="Parent_changed"/><b>Yes</b>
                <asp:RadioButton runat="server" id="rbdNotmarried"   GroupName="MARITAL"  AutoPostBack="true" OnCheckedChanged="Parent_changed" /> <b>No</b>
            </p>

        </div>
        <div class="step" id="diveSpouse" runat="server" >
           
            <h4 class="heading">I Dont Want To Be To Personal But..<br />May I Know Your Family Details</h4>
            <br />
        </div>
        <div class="card" id="divSpoucename" runat ="server" >
            <br />
            <br />
            <h2 class="heading">May I Know Your Spouse Name.. </h2>
            <p align="center" class="radio">
                <b>Name:-</b><asp:TextBox runat="server" class="radio" id="txtSPNAME"></asp:TextBox>
                <b>D.O.B:-</b><asp:TextBox runat="server" class="radio" id="txtspdob"></asp:TextBox><br />
               
                 <h2 class="heading">Is Your Spouse Working ? </h2>
            </p>
            <p align="center" class="radio">
                <asp:RadioButton runat="server" id="rbdYesworking"  GroupName="SWORK"  AutoPostBack="true" OnCheckedChanged="Parent_changed" /> <b>Yes</b>
                <asp:RadioButton runat="server" id="rbdNonotworking"  GroupName="SWORK"  AutoPostBack="true" OnCheckedChanged="Parent_changed" /> <b>No</b>
            </p>

        </div>


        <br />
        <div id="divSpoucedetails" class="card" runat="server" >
            <br />
            <br /><br />

            <h2 class="heading">What Does Your Spouse Do ?</h2>
            <br /><br />
            <p align="center" class="radio">
                <b>Occupation:-</b><asp:TextBox runat="server" class="radio" id="txtSoupsocc" ></asp:TextBox>
                <b>Designation:-</b><asp:TextBox runat="server" class="radio" id="txtSpousedeg" ></asp:TextBox><br /><br />
                <b>SPOUSE INCOME:-</b><asp:TextBox runat="server" class="radio" MaxLength="10" id="txtSPOUSEINCOME" ></asp:TextBox>
                <asp:label runat="server" text="(OPTIONAL)"></asp:label>
                <br />

        </div>
        <div id="divhavechild" class="card" runat="server" >
            <br />


            <h2 class="heading">Are You Blessed With Children</h2>
            <br /><br />
            <p align="center" class="radio">
                <asp:RadioButton runat="server" id="rbdYeschild"  GroupName="CHILDSTATUS"  AutoPostBack="true" OnCheckedChanged="Parent_changed" /> <b>Yes</b>
                <asp:RadioButton runat="server" id="rbdNochild"  GroupName="CHILDSTATUS"  AutoPostBack="true" OnCheckedChanged="Parent_changed" /> <b>No</b>
            </p>
            <br />


        </div>

        <div id="divchildetails" class="card" runat="server" >
            <br />

            <h2 class="heading">May I Know Your Child Details..</h2>

            <p align="center" class="radio">
                <b>Younger Child Name:-</b><asp:TextBox runat="server" class="radio" id="txtychildname" ></asp:TextBox>
                <b>DOB:-</b><asp:TextBox runat="server" class="radio" id="txtYChilddob" ></asp:TextBox><br />
                  <br />&nbsp;&nbsp;<b>Gender</b>
            
                 <asp:DropDownList id="Yddchildgender" class="radio" runat="server" >                        
                        <asp:ListItem value="M">Male</asp:ListItem>
                        <asp:ListItem value="F">Female </asp:ListItem>
                        </asp:DropDownList><br />

                <br />
                <br />
                &nbsp;&nbsp;<b>Elder Child Name:-</b><asp:TextBox runat="server" class="radio" id="txtEchildname" ></asp:TextBox>
                <b>DOB:-</b><asp:TextBox runat="server" class="radio" id="txtEdob" ></asp:TextBox><br /><br />
                &nbsp;&nbsp;<b>Gender</b>
                 <asp:DropDownList id="Eddchildgender" class="radio" runat="server" >                        
                        <asp:ListItem value="M">Male </asp:ListItem>
                        <asp:ListItem value="F">Female </asp:ListItem>
                        </asp:DropDownList>

                <br />


        </div>

        <div class="step" id="divfinancial" runat="server" >

            <h4 class="heading">To Design Your Financial Plan Better<br /> Let Us Know You Financially</h4>
            <br />
        </div>
        <div id="divoccupation" class="card" runat="server" >
            <br />
            <h2 class="heading">What Do You For Your Living ?</h2>
            <br /><br />
            <p align="center" class="radio">
                <b>Occupation:-</b><asp:TextBox runat="server" class="radio" id="txtself_Occupation"  ></asp:TextBox>
                <b>Designation:-</b><asp:TextBox runat="server" class="radio" id="txtDesignation" ></asp:TextBox>

                <br />


                <h2 class="heading">Are Your Parents Dependent On You ?</h2>
            <p align="center" class="radio">
                <asp:RadioButton runat="server" id="rdbYesparrent"  GroupName="patr" AutoPostBack="true" OnCheckedChanged="Parent_changed" /> <b>Yes</b>
                <asp:RadioButton runat="server" id="rdbNoparent" GroupName="patr" AutoPostBack="true" OnCheckedChanged="Parent_changed" /> <b>No</b>
            </p>

            <br />
            <br />


        </div>

        <div id="divparentdetails" class="card" runat="server" >

            <br />
            <h2 class="heading">May I Know Your Parent Details..</h2>
            <br />
            <p align="center" class="radio">
                <b>Father Name:-</b><asp:TextBox runat="server" class="radio" id="txtFname" ></asp:TextBox>
                <b>DOB:-</b><asp:TextBox runat="server" class="radio" id="txtFdob" ></asp:TextBox><br /><br />
                <b>Any Disease:-</b><asp:TextBox runat="server" class="radio" id="txtFhealth" ></asp:TextBox>
                <asp:label runat="server" text="(OPTIONAL)"></asp:label>
                    <br />
                    <br />
                    &nbsp;&nbsp;<b>Mother Name:-</b><asp:TextBox runat="server" class="radio" id="txtMname" ></asp:TextBox>
                    <b>DOB:-</b><asp:TextBox runat="server" class="radio" id="txtMDob" ></asp:TextBox><br /><br />
                    <b>Any Disease:-</b><asp:TextBox runat="server" class="radio" id="txtMhealth" ></asp:TextBox>
                    <asp:label runat="server" text="(OPTIONAL)"></asp:label> <br />
        </div>

        <div id="divgoalprnt" runat="server" class="card">
       
                <br />
                <h2 class="heading">Lets Talk About Your Dreams</h2>


                <p align="center" class="radio">
                    <b class="radio">Goal Name:-</b>

                    <asp:DropDownList id="drpgoalcode" class="radio" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList_Changed">                        
                        <asp:ListItem value="-SELECT-">--Select-- </asp:ListItem>
                        <asp:ListItem value="FG1">Bike </asp:ListItem>
                        <asp:ListItem value="FG2">Business </asp:ListItem>
                        <asp:ListItem value="FG3">Car</asp:ListItem>
                        <asp:ListItem value="FG4">House </asp:ListItem>
                        <asp:ListItem value="FG5">Study</asp:ListItem>
                        <asp:ListItem value="FG6">Travel</asp:ListItem>
                        <asp:ListItem value="FG7">Wedding</asp:ListItem>
                        <asp:ListItem value="FG8">Wealth</asp:ListItem>
                        <asp:ListItem value="FG9">Retirement</asp:ListItem>
                        <asp:ListItem value="FG10">Sabbatical</asp:ListItem>
                        <asp:ListItem value="FG11">Family Planning</asp:ListItem>
                        <asp:ListItem value="FG12">Child Study</asp:ListItem>
                        <asp:ListItem value="FG13">Child Wedding</asp:ListItem>
                        <asp:ListItem value="FG15">Charity</asp:ListItem>
                        <asp:ListItem value="FG16">Cosmetic Surgery</asp:ListItem>
                        <asp:ListItem value="FG17">Emergency</asp:ListItem>
                        <asp:ListItem value="FG14">Other</asp:ListItem>
                    </asp:DropDownList>


                    <b id="goalname_1" runat="server" >Goal Name:-</b>
                    <asp:TextBox runat="server" class="radio" id="txtGOAL_Name_1"  ></asp:TextBox> <br />
                    <br />
                     <b id ="bretiretirementage" runat="server">Retirement Age:-</b>

                    <asp:TextBox runat="server" class="radio" id="txtretirementage"  ></asp:TextBox> <br />
                    

                    <b>Present Value:-</b><asp:TextBox runat="server" class="radio" MaxLength="10" id="txtGOALPV_1" ></asp:TextBox>


                    <b>When You Want To Achieve?</b><asp:TextBox runat="server" class="radio" id="txtGOALDATE" ></asp:TextBox>
                </p>
                <asp:Button runat="server" class="button" id="btnaddgoal" Text="Add Goal" OnClick="btnaddgoal_Click" />
                <br />
            </div><br />
        <br />
        <div id="divgoallist" runat="server">
            <asp:Repeater ID="rptgoallist" runat="server">

                <HeaderTemplate>
                    <table id="tblgoal">
                        <tr>
                            <th>Goal Name</th>
                            <th>Present Value</th>
                            <th>Time Horizon</th>
                            <th>Delete</th>
                        </tr>
                    

                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                    <td><%#DataBinder.Eval(Container,"DataItem.GName") %></td>
                    <td><%#DataBinder.Eval(Container,"DataItem.currentCost") %></td>
                    <td><%#DataBinder.Eval(Container,"DataItem.TimeHorizon") %></td>
                    <td><asp:LinkButton ID="lnkdelete" Text="Delete" runat="server" OnClick="lnkDelete_Click" OnClientClick="return confirm('Do you want to delete this row?');" /></td>
                     <asp:Label ID="lblId" runat="server" Text='<%# Eval("id") %>' Visible = "false" />
    
                    </tr>
                     
                </ItemTemplate>
                <FooterTemplate>
                     </table>
                </FooterTemplate>
               
            </asp:Repeater>
       

        </div>
        <div id="divfinancedetails" class="card" runat="server" >
            <br />
            <br />

            <h2 class="heading">May I Know Your Financial Details..</h2>
            <br />
            <p align="center" class="radio">
                <b>Monthly Salary:-</b><asp:TextBox runat="server" class="radio" id="txtMsalary" MaxLength="10" ></asp:TextBox>(OPTIONAL)<br /><br />
                <b>Monthly Expense:-</b><asp:TextBox runat="server" class="radio" id="txtMexpence" MaxLength="10" ></asp:TextBox> (OPTIONAL)<br /><br />
                <b>Total Availiable Amount:-</b><asp:TextBox runat="server" class="radio" id="txtAvailAmount" MaxLength="10" ></asp:TextBox>
                <asp:label runat="server" text="(OPTIONAL)"></asp:label>
            

            <asp:Button runat="server" class="button" id="btnsubmit" Text="Submit" OnClick="btnsubmit_Click"/>
                </p>
        </div>
    </div>
    <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
    <button onclick="DownFunction()" id="downBtn" title="Go to top">Down</button>
      <%--  //AJAX ModalPopupExtender configuration--%>
        <asp:Label ID="test" runat="server" Text=""></asp:Label>
<asp:ModalPopupExtender ID="ModalPopupExt" runat="server"
        Enabled="True"  TargetControlID="test"
        BackgroundCssClass="modal" PopupControlID="panmemberlst"
        CancelControlID="btnclose">
    </asp:ModalPopupExtender>
        <asp:Panel ID="panmemberlst" runat="server">
       
                    <div style=" margin-right: 250px; margin-left: 250px;
      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        height: 300px;
        border-radius: 50px;
        border-color: Window;
        background-color:white;
        background-size: 50px;
        border-width: 2px;
        ">
                <p align="center" class="radio">
                    <br />
                <h5 class="heading">Please Update Your Member Relation First</h5>
                <br />
                <br />
            <asp:Repeater ID="rptmemberlist" runat="server" >
                 <HeaderTemplate>
                    <table id="tblmemeber" >
                        <tr>
                            <th>Client Name</th>
                          
                            <th>Select Relation</th>
                            <th>Update</th>
                        </tr>
                    

                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                    <td><%#DataBinder.Eval(Container,"DataItem.ClientName") %></td>
                  
                   <td>        <asp:DropDownList ID="ddlRelationList" runat="server" DataSourceID="dsLocation" DataTextField="Relation" DataValueField="Relation_code"></asp:DropDownList>
                        <asp:SqlDataSource ID="dsLocation" runat="server" ConnectionString="<%$ ConnectionStrings:mycon %>" SelectCommand="Select Relation , Relation_code  from fin_tbl_Realtionmaster"></asp:SqlDataSource>
                             </td> 
                         <td><asp:Button ID="btnUpdatereleation" runat="server" Text="Update" CssClass="radio" OnClick="btnUpdatereleation_Click" /></td>
                       
                         <asp:Label ID="lblId" runat="server" Text='<%# Eval("basic_ID") %>' Visible = "false" />
                        </tr>
                     
                </ItemTemplate>
                <FooterTemplate>
                     </table>
                </FooterTemplate>
                

            </asp:Repeater>

  
       
                     <asp:Button ID="btnclose" runat="server" Text="Close" CssClass="button" Visible="true" />

                    </p>
                </div>
             

        </asp:Panel>
            </form>

</body>
</html>






