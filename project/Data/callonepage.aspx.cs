﻿using data.model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace FinFinancialPlan
{
    public partial class callonepage : System.Web.UI.Page
    {



        DataSet dataforcallon;
        DataTable selftdetails;
        DataTable Spousedel;
        DataTable Father;
        DataTable Mother;
        DataTable Children;
        DataTable goals;
        DataAccess data = new DataAccess();
        DataTable userpass;
        string selfdob = string.Empty;
        string selfgen = string.Empty;
        string marital = string.Empty;
        string spouswork = string.Empty;
        int childstatus;
        string spousename = string.Empty;
        string spdegig = string.Empty;
        string spousdob = string.Empty;
        string fatherdob = string.Empty;
        string motherdob = string.Empty;
        string ychilddob = string.Empty;
        string Echilddob = string.Empty;
        string strbasicid = string.Empty;
        string Mthlyincome = string.Empty;
        string Mthlyexpense = string.Empty;
        string aval_res = string.Empty;
        string strplain = string.Empty;
        bool othrFlag = false;
        bool resp = false;
        public string username = string.Empty;
        public string password = string.Empty;
        string API_URL_Production = "https://api.fincart.com/";
        const string BaseUri = "https://api.fincart.com/api/v2/fintoken";

        string childgender = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            strbasicid = Request.QueryString["bid"];
            strplain = Request.QueryString["pid"];




            if (!IsPostBack)
            {

                panmemberlst.Visible = false;
                rptmemberlist.Visible = false;
                resp = false;
                txtGOAL_Name_1.Text = string.Empty;
                txtGOALDATE.Text = string.Empty;
                txtGOALPV_1.Text = string.Empty;
                drpgoalcode.SelectedValue = "-SELECT-";
                txtretirementage.Visible = false;
                bretiretirementage.Visible = false;
                rdbMale.Checked = false;
                rdbfemale.Checked = false;
                diveSpouse.Visible = false;
                divchildetails.Visible = true;
                divSpoucename.Visible = false;
                divhavechild.Visible = false;
                divfinancial.Visible = false;
                divoccupation.Visible = false;
                divparentdetails.Visible = false;
                divgoalprnt.Visible = false;
                divfinancedetails.Visible = false;
                divSpoucedetails.Visible = false;
                divgoallist.Visible = false;
                txtGOAL_Name_1.Visible = false;
                goalname_1.Visible = false;
                rptgoallist.Visible = false;
                divchildetails.Visible = false;
                divSpoucedetails.Visible = false;
                strbasicid = Request.QueryString["bid"];
                strplain = Request.QueryString["pid"];

                if (!string.IsNullOrWhiteSpace(strbasicid))
                {
                    dataforcallon = data.callonedata(strbasicid);



                    if (dataforcallon.Tables[6].Rows.Count >= 1)
                    {
                        btnsubmit.Visible = false;
                        panmemberlst.Visible = true;
                        rptmemberlist.Visible = true;
                        rptmemberlist.DataSource = dataforcallon.Tables[6];
                        rptmemberlist.DataBind();
                        ModalPopupExt.Show();

                    }




                    selftdetails = dataforcallon.Tables[0];
                    Spousedel = dataforcallon.Tables[1];
                    Children = dataforcallon.Tables[2];
                    selfdob = selftdetails.Rows[0]["Date_of_Birth"].ToString();
                    string[] dateofbirth = selfdob.Split(' ');
                    datepicker.Text = selfdob = formatter.enUsDateFormat(dateofbirth[0].ToString());
                    selfgen = selftdetails.Rows[0]["Gender"].ToString();
                    marital = selftdetails.Rows[0]["marital_status"].ToString();



                    if (Spousedel.Rows.Count >= 1)
                    {
                        spouswork = Spousedel.Rows[0]["Designation"].ToString();
                    }
                    childstatus = Children.Rows.Count;
                    Father = dataforcallon.Tables[3];
                    Mother = dataforcallon.Tables[4];
                    goals = dataforcallon.Tables[5];
                    if (selftdetails.Rows.Count >= 1)
                    {
                        divoccupation.Visible = true;
                        txtself_Occupation.Text = selftdetails.Rows[0]["company_name"].ToString();
                        txtDesignation.Text = selftdetails.Rows[0]["Designation"].ToString();
                    }
                    if (selfgen == "M")
                    {


                        rdbMale.Checked = true;


                    }
                    else if (selfgen == "F")
                    {

                        rdbfemale.Checked = true;

                    }
                    else
                    {

                        rdbMale.Checked = false;
                        rdbfemale.Checked = false;


                    }

                    if (marital == "Y")
                    {

                        rbdYesmarried.Checked = true;
                        divSpoucename.Visible = true;
                        diveSpouse.Visible = true;
                        divhavechild.Visible = true;
                        if (Spousedel.Rows.Count >= 1)
                        {
                            txtSPNAME.Text = spouswork = Spousedel.Rows[0]["ClientName"].ToString();
                            spousdob = spouswork = Spousedel.Rows[0]["Date_of_Birth"].ToString();
                            string[] spodob = spousdob.Split(' ');

                            txtspdob.Text = spodob[0].ToString();
                        }
                    }
                    else if (marital == "N")
                    {

                        rbdNotmarried.Checked = true;
                        divSpoucename.Visible = true;
                        diveSpouse.Visible = true;


                    }
                    else
                    {


                        rbdYesmarried.Checked = false;
                        rbdNotmarried.Checked = false;
                    }


                    if (!string.IsNullOrWhiteSpace(spouswork))
                    {
                        rbdYesworking.Checked = true;
                        divSpoucedetails.Visible = true;
                        txtSoupsocc.Text = spouswork = Spousedel.Rows[0]["company_name"].ToString();
                        txtSpousedeg.Text = spouswork = Spousedel.Rows[0]["Designation"].ToString();
                        txtSPOUSEINCOME.Text = spouswork = Spousedel.Rows[0]["month_income"].ToString();
                    }

                    else
                    {
                        rbdYesworking.Checked = false;
                        divSpoucedetails.Visible = false;
                        rbdNonotworking.Checked = true;
                        rbdNotmarried.Checked = true;

                    }

                    if (childstatus >= 1)
                    {

                        rbdYeschild.Checked = true;
                        divhavechild.Visible = true;
                        divchildetails.Visible = true;
                        txtychildname.Text = Children.Rows[0]["ClientName"].ToString();
                        ychilddob = Children.Rows[0]["Date_of_Birth"].ToString();
                        string[] ydob = ychilddob.Split(' ');
                        txtYChilddob.Text = ydob[0].ToString();
                        childgender = Children.Rows[0]["Gender"].ToString();


                        if (childgender == "M")
                        {
                            Yddchildgender.SelectedValue = "M";

                        }
                        if (childgender == "F")
                        {
                            Yddchildgender.SelectedValue = "F";

                        }
                        if (childstatus >= 2)
                        {

                            txtEchildname.Text = Children.Rows[1]["ClientName"].ToString();
                            Echilddob = Children.Rows[1]["Date_of_Birth"].ToString();
                            string[] eclddob = Echilddob.Split(' ');
                            txtEdob.Text = eclddob[0].ToString();
                            childgender = Children.Rows[0]["Gender"].ToString();
                            if (childgender == "M")
                            {
                                Eddchildgender.SelectedValue = "M";

                            }
                            if (childgender == "F")
                            {
                                Eddchildgender.SelectedValue = "F";

                            }

                        }
                    }
                    else
                    {


                        rbdYeschild.Checked = false;
                        txtychildname.Text = string.Empty;
                        txtYChilddob.Text = string.Empty;
                        txtEchildname.Text = string.Empty;
                        txtEdob.Text = string.Empty;
                        rbdNochild.Checked = true;



                    }


                    if (Father.Rows.Count >= 1)
                    {

                        divparentdetails.Visible = true;


                        txtFname.Text = Father.Rows[0]["ClientName"].ToString();
                        fatherdob = Father.Rows[0]["Date_of_Birth"].ToString();
                        string[] fathdob = fatherdob.Split(' ');
                        txtFdob.Text = fathdob[0].ToString();
                        txtFhealth.Text = Father.Rows[0]["Disease_name"].ToString();
                    }
                    else
                    {


                        divparentdetails.Visible = false;

                    }


                    if (Mother.Rows.Count >= 1)
                    {

                        divparentdetails.Visible = true;


                        txtMname.Text = Mother.Rows[0]["ClientName"].ToString();
                        motherdob = Mother.Rows[0]["Date_of_Birth"].ToString();
                        string[] mothdob = motherdob.Split(' ');
                        txtMDob.Text = mothdob[0].ToString();
                        txtMhealth.Text = Mother.Rows[0]["Disease_name"].ToString();
                    }


                    if (goals.Rows.Count >= 1)
                    {
                        divgoallist.Visible = true;
                        divgoalprnt.Visible = true;
                        rptgoallist.DataSource = goals;
                        rptgoallist.DataBind();
                        divgoallist.Visible = true;
                        rptgoallist.Visible = true;

                    }
                    if (selftdetails.Rows.Count >= 1)
                    {
                        Mthlyincome = selftdetails.Rows[0]["month_income"].ToString();
                        if (!string.IsNullOrWhiteSpace(Mthlyincome))
                        {
                            divfinancedetails.Visible = true;
                            Mthlyincome = selftdetails.Rows[0]["month_income"].ToString();
                            txtMsalary.Text = Mthlyincome;
                            Mthlyexpense = selftdetails.Rows[0]["Month_Expense"].ToString();
                            txtMexpence.Text = Mthlyexpense;
                            aval_res = selftdetails.Rows[0]["Avail_res"].ToString();
                            txtAvailAmount.Text = aval_res;
                        }
                    }




                }

                ///Postbackstats///
                ///

                if (rbdYesmarried.Checked == true)
                {

                    rbdNotmarried.Checked = false;
                    rbdYesmarried.Checked = true;
                    divSpoucename.Visible = true;
                    diveSpouse.Visible = true;
                    divfinancedetails.Visible = true;

                }
                if (rbdNotmarried.Checked == true)
                {

                    rbdYesmarried.Checked = false;
                    divSpoucename.Visible = false;
                    diveSpouse.Visible = false;
                    divfinancedetails.Visible = true;
                }




            }
        }


        protected void Parent_changed(object sender, EventArgs e)
        {
            if (rdbYesparrent.Checked == true)
            {

                divparentdetails.Visible = true;
                divgoallist.Visible = true;
                divgoalprnt.Visible = true;
                divfinancedetails.Visible = true;

            }
            else
            {


                divparentdetails.Visible = false;
                divgoallist.Visible = true;
                divgoalprnt.Visible = true;
                divfinancedetails.Visible = true;

            }

            if (rbdYesmarried.Checked == true)
            {

                divSpoucename.Visible = true;
                divhavechild.Visible = true;
            }
            else
            {
                diveSpouse.Visible = false;
                txtSPNAME.Text = null;
                txtSoupsocc.Text = null;
                txtspdob.Text = null;
                txtSpousedeg.Text = null;
                divSpoucename.Visible = false;
                divhavechild.Visible = false;
                divSpoucedetails.Visible = false;
                divfinancial.Visible = true;
                divfinancedetails.Visible = true;
                divoccupation.Visible = true;
                rbdNonotworking.Checked = true;
                rbdNochild.Checked = true;

            }

            if (rbdYesworking.Checked == true && rbdYesmarried.Checked == true)
            {

                divSpoucedetails.Visible = true;

            }
            else
            {


                divSpoucedetails.Visible = false;
                divoccupation.Visible = true;


            }

            if (rbdYeschild.Checked == true && rbdYesmarried.Checked == true)
            {

                divchildetails.Visible = true;

            }
            else
            {
                txtychildname.Text = null;
                txtYChilddob.Text = null;
                divchildetails.Visible = false;
                txtEchildname.Text = null;
                txtEdob.Text = null;

            }





        }
        protected void DropDownList_Changed(object sender, EventArgs e)
        {

            if (drpgoalcode.SelectedItem.Value == "FG14")
            {
                goalname_1.Visible = true;
                txtGOAL_Name_1.Visible = true;
            }
            else
            {

                goalname_1.Visible = false;
                txtGOAL_Name_1.Visible = false;
            }
            if (drpgoalcode.SelectedValue == "FG9")
            {
                txtretirementage.Visible = true;
                bretiretirementage.Visible = true;

            }
            else
            {
                bretiretirementage.Visible = false;
                txtretirementage.Visible = false;


            }
        }

        protected void btnaddgoal_Click(object sender, EventArgs e)
        {

            strbasicid = Request.QueryString["bid"];
            callonegoals goalin = new callonegoals();
            goalin.basicid = Request.QueryString["bid"];
            goalin.goalcode = drpgoalcode.SelectedValue;
            if (drpgoalcode.SelectedValue != "-SELECT-")
            {
                if (drpgoalcode.SelectedValue == "FG14")
                {
                    txtGOAL_Name_1.Visible = true;
                    if (!string.IsNullOrWhiteSpace(txtGOAL_Name_1.Text))
                    {

                        goalin.gname = txtGOAL_Name_1.Text;

                    }
                    else
                    {
                        showMessage("'Please Enter Goal Name.", "error");


                    }

                }
                else
                {
                    if (drpgoalcode.SelectedValue == "FG9")
                    {
                        if (!string.IsNullOrWhiteSpace(txtretirementage.Text))
                        {


                            goalin.retirementage = txtretirementage.Text;

                        }
                        else {
                            
                            showMessage("Please Enter Retirement Age", "error");

                        
                        }


                    }
                    goalin.goalcode = drpgoalcode.SelectedValue;
                }

                if (!string.IsNullOrWhiteSpace(txtGOALPV_1.Text))
                {
                    goalin.goalpv = txtGOALPV_1.Text;



                }

                else
                {
                    showMessage("Please Enter Goal Present Value.", "error");



                }
                if (!string.IsNullOrWhiteSpace(txtGOALDATE.Text))
                {
                    goalin.gachdate = txtGOALDATE.Text;




                    fincalgoal fincal = new fincalgoal();

                  fincal.type = "CAMT";
                  fincal.currentAmount = txtGOALPV_1.Text;
                  fincal.startDate = DateTime.Now.ToString();
                  fincal.endDate = txtGOALDATE.Text;
                  fincal.goalCode = drpgoalcode.SelectedValue;
                  fincal.otherGoalName =  txtGOAL_Name_1.Text;
                  fincal.childName= "";
                  fincal.travelPeople = "";
                  fincal.locationCode ="";
                  fincal.budgetType ="";
                  fincal.businessStartupCost = "";
                  fincal.monthlyexpence = "";

                    HttpClient httpClient = new HttpClient();
                    httpClient.Timeout = TimeSpan.FromMinutes(30);
            
            
                      httpClient.BaseAddress = new Uri(API_URL_Production);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response;
                  
                    response = httpClient.PostAsJsonAsync(API_URL_Production + "api/v2/fincalc", fincal).Result;
                     
                     
                   
                    var responseData = response.Content.ReadAsStringAsync().Result;


                    ServerResponse srvResponse = JsonConvert.DeserializeObject<ServerResponse>(responseData.Replace("\\", "").ToString());
                    if (srvResponse.status == "Success")
                    {

                        userpass = data.getuserpassword(strbasicid);
                        if (userpass.Rows.Count >= 1)
                        {
                            username = userpass.Rows[0]["userid"].ToString();
                            password = userpass.Rows[0]["userpass"].ToString();


                                    string token = tokengeneration(username, password);
                                    string fincalresponse = responseData.Replace("{", "").ToString();
                                    fincalresponse = fincalresponse.Replace(",", "").ToString();
                                    fincalresponse = fincalresponse.Replace("}", "").ToString();
                                    string[] fincalres = fincalresponse.Split(':', '"');
                                    gaolcreatemodel goalcreate = new gaolcreatemodel();
                                    goalcreate.usergoalId = "";
                                    goalcreate.basicid = strbasicid;
                                    goalcreate.Relation = "";
                                    goalcreate.childName = txtYChilddob.Text;
                                    goalcreate.age = "";
                                    goalcreate.gender = "";
                                    goalcreate.annualIncome = "";
                                    goalcreate.goalCode = drpgoalcode.SelectedValue;
                                    goalcreate.otherGoalName = txtGOAL_Name_1.Text;
                                    goalcreate.typeCode = "002";
                                    goalcreate.monthlyAmount = "";
                                    goalcreate.presentValue = txtGOALPV_1.Text;
                                    goalcreate.risk = "M";
                                    goalcreate.goal_StartDate = DateTime.Now.ToString();
                                    goalcreate.goal_EndDate = txtGOALDATE.Text;
                                    goalcreate.inflationRate = fincalres[58].ToString();
                                    goalcreate.ror = fincalres[48].ToString();
                                    goalcreate.PMT = fincalres[43].ToString();
                                    goalcreate.downPaymentRate = "";
                                    goalcreate.trnx_Type = "s";
                                    goalcreate.getAmount = fincalres[23].ToString();
                                    goalcreate.retirementAge = txtretirementage.Text;
                                    goalcreate.investAmount = fincalres[38].ToString();
                                    goalcreate.people = "";

                                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToString(token));
                        
                                    response = httpClient.PostAsJsonAsync(API_URL_Production + "api/v2/goal/create", goalcreate).Result;
                                    var responseDatacreate = response.Content.ReadAsStringAsync().Result;


                                    ServerResponse srvResponsecreate = JsonConvert.DeserializeObject<ServerResponse>(responseDatacreate.Replace("\\", "").ToString());
                                    if (srvResponsecreate.status == "Success")
                                    {
                                        dataforcallon = data.callonedata(strbasicid);
                                        goals = dataforcallon.Tables[5];
                                        rptgoallist.DataSource = goals;
                                        rptgoallist.DataBind();
                                        divgoallist.Visible = true;
                                        rptgoallist.Visible = true;

                                        showMessage("Goal Added Sucessfully.", "success");
                                    }


                            
                        }
                    }
                    else { showMessage("something went wrong while sending email", "error"); }
              

                 

                }

                else
                {
                    showMessage("Please Enter when you want to achive.", "error");

                }
            }

            else
            {

                showMessage("Please Select Goal", "error");
            }
        }




        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            strbasicid = Request.QueryString["bid"];
            dataforcallon = data.callonedata(strbasicid);
            Spousedel = dataforcallon.Tables[1];
            selftdetails = dataforcallon.Tables[0];
            Father = dataforcallon.Tables[3];
            Mother = dataforcallon.Tables[4];
            Children = dataforcallon.Tables[2];
            if (!string.IsNullOrWhiteSpace(datepicker.Text) == false)
            {

                showMessage("Please Enter Clients DOB", "error");

            }


            else if (!string.IsNullOrWhiteSpace(txtSPNAME.Text) == false && rbdYesmarried.Checked == true)
            {

                showMessage("Please Enter Spouse Name", "error");
            }

            else if (rbdYeschild.Checked == true && !string.IsNullOrWhiteSpace(txtychildname.Text) == false)
            {


                showMessage("Please Enter Child Details", "error");

            }
            else
                if (rdbYesparrent.Checked == true && !string.IsNullOrWhiteSpace(txtFname.Text) == false)
                {


                    showMessage("Please Enter Parents Details", "error");

                }

                else
                {


                    callonebasicinsert cltbasicinsert = new callonebasicinsert();
                    ////Sumary/////
                    ///Binding model for client selftdata////
                    ///

                    cltbasicinsert.Clientbasicid = strbasicid;
                    cltbasicinsert.ClientDOB = datepicker.Text;
                    if (rdbMale.Checked == true)
                    {
                        cltbasicinsert.ClientGender = "M";

                    }
                    else if (rdbfemale.Checked == true)
                    {
                        cltbasicinsert.ClientGender = "F";

                    }
                    else
                    {

                        cltbasicinsert.ClientGender = null;

                    }
                    if (rbdYesmarried.Checked == true)
                    {

                        cltbasicinsert.ClientMarital = "Y";
                    }
                    else if (rbdNotmarried.Checked == true)
                    {
                        cltbasicinsert.ClientMarital = "N";

                    }
                    else
                    {
                        cltbasicinsert.ClientMarital = null;
                    }

                    cltbasicinsert.ClientOccup = txtself_Occupation.Text;
                    cltbasicinsert.ClientDegns = txtDesignation.Text;
                    cltbasicinsert.ClientMnthin = txtMsalary.Text;
                    cltbasicinsert.Clientmnthex = txtMexpence.Text;
                    cltbasicinsert.Clientavlres = txtAvailAmount.Text;

                    ////Sumary/////
                    ///Binding model for client Spousedata////
                    ///


                    if (Spousedel.Rows.Count >= 1)
                    {
                        cltbasicinsert.spousebasicid = Spousedel.Rows[0]["basic_ID"].ToString();
                    }

                    cltbasicinsert.spousedob = txtspdob.Text;
                    cltbasicinsert.spouseName = txtSPNAME.Text;
                    cltbasicinsert.spouseocc = txtSoupsocc.Text;
                    cltbasicinsert.spousdeg = txtSpousedeg.Text;
                    cltbasicinsert.spouseincome = txtSPOUSEINCOME.Text;

                    ////Sumary/////
                    ///Binding model for client Child one data////
                    ///


                    if (Children.Rows.Count >= 1)
                    {
                        cltbasicinsert.chldonebasicid = Children.Rows[0]["basic_ID"].ToString();
                    }


                    cltbasicinsert.chldoneDOb = txtYChilddob.Text;
                    cltbasicinsert.chldoneName = txtychildname.Text;
                    cltbasicinsert.chldoneGen = Yddchildgender.SelectedValue;


                    ////Sumary/////
                    ///Binding model for client child two data////
                    ///


                    if (Children.Rows.Count > 1)
                    {
                        cltbasicinsert.chldtwobasicid = Children.Rows[1]["basic_ID"].ToString();
                    }

                    cltbasicinsert.chldtwodob = txtEdob.Text;
                    cltbasicinsert.chldtwoname = txtEchildname.Text;
                    cltbasicinsert.chldtwoGen = Eddchildgender.SelectedValue;
                    ////Sumary/////
                    ///Binding model for client father data////
                    ///

                    if (Father.Rows.Count >= 1)
                    {
                        cltbasicinsert.fatherbasicid = Father.Rows[0]["basic_ID"].ToString();
                    }

                    cltbasicinsert.fatherdob = txtFdob.Text;
                    cltbasicinsert.fathername = txtFname.Text;


                    ////Sumary/////
                    ///Binding model for client mother data////
                    ///


                    if (Mother.Rows.Count >= 1)
                    {
                        cltbasicinsert.motherbasicid = Mother.Rows[0]["basic_ID"].ToString();
                    }

                    cltbasicinsert.motherdob = txtMDob.Text;
                    cltbasicinsert.mothername = txtMname.Text;

                    resp = data.insertclientinfofromcallone(cltbasicinsert);


                }




            if (resp)
            {
                strbasicid = Request.QueryString["bid"];
                strplain = Request.QueryString["pid"];
                showMessage("Lets Create Your Plan Now", "success");

                Response.Redirect("Default.aspx?" + "bid=" + strbasicid + "" + "&pid=" + strplain + "");

            }

            showMessage("Data Not Saved", "Error");



        }
        public string tokengeneration(string userid, string userpasss)
        {

            HttpClient client = new HttpClient();
            
                var request = new HttpRequestMessage(HttpMethod.Post, BaseUri);
                request.Content = new FormUrlEncodedContent(new Dictionary<string, string> {
                                {"username",username},
                                {"password",password},
                                { "client_id", "fincartApp" },
                                { "client_secret", "264e3285b0934defa5067bfb4bc9d682" },
                                { "grant_type", "password" }});



                var responsetoken = client.SendAsync(request);
                responsetoken.Wait();
                var contentObj = responsetoken.Result.Content.ReadAsStringAsync().Result;
                var payload = JObject.Parse(contentObj);
                var token = payload.Value<string>("access_token");
                string tokenn = token.ToString();
            
            return tokenn;
        
        
        }
        protected void lnkDelete_Click(object sender, EventArgs e)

         {  
            
            userpass = data.getuserpassword(strbasicid);
                            username = userpass.Rows[0]["userid"].ToString();
                            password = userpass.Rows[0]["userpass"].ToString();

            string token = tokengeneration(username, password);

            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string usergoalid = Convert.ToString((item.FindControl("lblId") as Label).Text);
            HttpClient httpClient = new HttpClient();
            httpClient.Timeout = TimeSpan.FromMinutes(30);
            //httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Convert.ToString(token));
            HttpResponseMessage response;
            response = httpClient.DeleteAsync(API_URL_Production + string.Format("api/v2/goal/delete/{0}", usergoalid)).Result;
            //response = httpClient.GetAsync(API_URL_Production + string.Format("api/v2/goal/delete/{0}", usergoalid)).Result;
            var responseData = response.Content.ReadAsStringAsync().Result;
            ServerResponse srvResponse = JsonConvert.DeserializeObject<ServerResponse>(responseData.Replace("\\", "").ToString());
            if (srvResponse.status == "Success")
            {
                showMessage("Goal Deleted Successfuly", "success");
                dataforcallon = data.callonedata(strbasicid);
                goals = dataforcallon.Tables[5];
                rptgoallist.DataSource = goals;
                rptgoallist.DataBind();
                divgoallist.Visible = true;
                rptgoallist.Visible = true;
                
            }
            else { showMessage("Something went wrong while Deleting Goal", "error"); }

        
        }
        void addControlAttributes()
        {
            txtAvailAmount.Attributes.Add("onkeyup", "integersOnly(this);");
            txtMexpence.Attributes.Add("onkeyup", "integersOnly(this);");
            txtMsalary.Attributes.Add("onkeyup", "integersOnly(this);");
            txtGOALPV_1.Attributes.Add("onkeyup", "integersOnly(this);");
            txtSPOUSEINCOME.Attributes.Add("onkeyup", "integersOnly(this);");


        }
        void showMessage(string msg, string type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "swal({text: '" + msg + "',icon: '" + type + "'});", true);
        }

        protected void btnUpdatereleation_Click(object sender, EventArgs e)
        {
            strbasicid = Request.QueryString["bid"];
            strplain = Request.QueryString["pid"];
            RepeaterItem item = (sender as Button).Parent as RepeaterItem;
            DropDownList ddlRelationList = item.FindControl("ddlRelationList") as DropDownList;


            string strmbrbasicid = Convert.ToString((item.FindControl("lblId") as Label).Text);
            string strrelation = ddlRelationList.SelectedValue.ToString();
            resp = data.updaterelation(strmbrbasicid, strrelation);
            if (resp == true)
            {
                panmemberlst.Visible = false;
                rptmemberlist.Visible = false;
                btnsubmit.Visible = true;
                showMessage("Lets Create Your Plan Now", "success");
                Response.Redirect("callonepage.aspx?" + "bid=" + strbasicid + "" + "&pid=" + strplain + "");


            }

            else
            {
                panmemberlst.Visible = true;
                rptmemberlist.Visible = true;
                btnsubmit.Visible = false;
                showMessage("Data Not Saved", "Error");
                Response.Redirect("callonepage.aspx?" + "bid=" + strbasicid + "" + "&pid=" + strplain + "");

            }

        }


    }

}
