﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="goalsToFund.aspx.cs" Inherits="FinFinancialPlan.goalsToFund" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

    <link href="css/timeout-dialog.css" rel="stylesheet" />
    <script src="js/timeout-dialog.js"></script>
    <link href="css/repeaterstype.css" rel="stylesheet" />
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="css/tabs.css" rel="stylesheet" />
     <script src="js/tabCustom.js"></script>
       <script src="js/validation.js"></script>
    <script src="js/session.js"></script>
</head>
<body>
<div class="container">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-1" type="button" class="btn btn-default btn-circle" disabled="disabled">1</a>
                    <p><small>Basic Details</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p><small>Income</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p><small>Expense</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle"  disabled="disabled">4</a>
                    <p><small>NetAssets Entry</small></p>
                </div>
                 <div class="stepwizard-step col-xs-2">
                    <a href="#step-5" type="button" class="btn btn-default btn-success btn-circle">5</a>
                    <p><small>Goals Summary</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                    <p><small>NetAssets Allocation</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-7" type="button" class="btn btn-default btn-circle" disabled="disabled">7</a>
                    <p><small>NetCashFlow Allocation</small></p>
                </div>
            </div>
        </div>



 <form id="form1" runat="server" role="form">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

            <div class="panel panel-primary setup-content" id="step-5">
                 <div class="panel-heading"><h3 class="panel-title">Goals Summary</h3></div>
              
                    <div class="panel-body">
                       
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <input type="button" onClick="parent.open('https://workpoint.fincart.com/Goal/UserGoal')" value='Manage Goals' />
                               
                            </div>
                            <div class="col-lg-6">
                            </div>
                        </div>
                        
                         <div class="col-lg-12">

                             <asp:UpdatePanel runat="server" ID="updatepanel1">
                                <ContentTemplate>
                             <%--******* REPEATER TO SHOW ENTRY *******--%>
                           
                                 <asp:Repeater ID="rptrAllGoals" runat="server" >
                                    <HeaderTemplate>
                                        <table id="ulRptCashIngoal">
                                          <tr>
                                                    <th>Priority</th>
                                                    <th>Goal</th>
                                                    <th>Time Horizon</th>
                                                    <th>Present Cost</th>
                                                    <th>Future Cost</th>

                                                    
                                             </tr>
                                    </HeaderTemplate>
                            <ItemTemplate>
                       <tr>
                                         <td >   
                                          <asp:Label ID="lblGoalPriority" runat="server" CssClass="small-circle" Text='<%#DataBinder.Eval(Container,"DataItem.goalPriority")%>'></asp:Label>

                                          <asp:TextBox ID="txtGoalPriority" onkeyup="integersOnly(this);" runat="server" Text='<%# Eval("goalPriority") %>' Visible="false" MaxLength="5" />
                                             
                                          <asp:LinkButton ID="lnkEditGoalPriority" Text="Set Priority" runat="server" OnClick="lnkEditGoalPriority_Click" />
                                          <asp:LinkButton ID="lnkUpdateGoalPriority" Text="Save" runat="server" OnClick="lnkUpdateGoalPriority_Click" Visible="false" />
                                         </td>
                                         <td ><%#DataBinder.Eval(Container,"DataItem.goalName")%></td>
                                         <td ><%#DataBinder.Eval(Container,"DataItem.duration")%></td>
                                        <td style="text-align:right" ><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.presentCost")))%></td>
                                        <td style="text-align:right"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.futureCost")))%>

                                             <asp:Label ID="lblId" runat="server" Text='<%# Eval("id")%>' Visible = "false" />
                                        </td>

                                      
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>    
                    
                        </asp:Repeater>
                            

                          
                                 </ContentTemplate>
                              </asp:UpdatePanel>
                         </div>
                        
                        
                        
                        <%--******* BOTTOM BUTTONS GOES HERE *******--%>
                        <div class="col-lg-12">
                            &nbsp;
                        </div>
                        
                        <div class="col-lg-12">
                            <asp:Button Text="Next >>" OnClick="btnNext_Click" runat="server" ID="btnNext" class="btn btn-primary nextBtn pull-right" type="button" />
                            <asp:Button Text="<< Back" runat="server" ID="prevToCashOutMap" OnClick="prevToCashOutMap_Click" class="btn btn-primary nextBtn pull-right" type="button" />
                        </div>

                    </div>
            </div>
     </form>
</body>
</html>
