﻿using data.model;
using FinFinancialPlan;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{
    public partial class goalsToFund : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
      
        DataTable dtGoalList;
        Dictionary<string, string> clintdic;
        public string strBasicId;
        public string strUserId;
        public string strCreatedByEmpId;
        public string strPlanId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];

                strPlanId = clintdic["planid"];
                strUserId = clintdic["userid"];
                strCreatedByEmpId = clintdic["createdby"];
                strBasicId = clintdic["basicid"];

                // ** SET LAST WORKING PAGE **
                data.SetLastPageWorkingById(clintdic["basicid"], clintdic["planid"], "CashOutMap.aspx");
            }
            else { Response.Redirect("default.aspx"); }

            if (!IsPostBack)
            {

                this.bindAllGoals(strBasicId);
               
						
            }
            // SESSION POPUP

            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);

            //Session["Reset"] = true;
            //Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            //SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            //int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            //ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);
        }

        protected void btnNext_Click(object sender, EventArgs e)
        {
            Response.Redirect("mapAssets.aspx");
        }

        protected void prevToCashOutMap_Click(object sender, EventArgs e)
        {
            Response.Redirect("CashOutMap.aspx");
        }

        void bindAllGoals(string strBasicId)
        {
            dtGoalList = data.getclientgoalbybasicid(strBasicId,strPlanId);
            rptrAllGoals.DataSource = dtGoalList;
            rptrAllGoals.DataBind();
        }

        

        protected void lnkEditGoalPriority_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            this.ToggleElements(item, true);
        }

        protected void lnkUpdateGoalPriority_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string goalId = (item.FindControl("lblId") as Label).Text;
            
            string strPriority = (item.FindControl("txtGoalPriority") as TextBox).Text.Trim();
            bool res = data.updateGoalPriority(goalId, strPriority);
            this.bindAllGoals(strBasicId);
            showMessage("goal priority updated successfully", "success");
        }

        private void ToggleElements(RepeaterItem item, bool isEdit)
        {
            //Toggle Buttons.
            item.FindControl("lnkEditGoalPriority").Visible = !isEdit;
            item.FindControl("lnkUpdateGoalPriority").Visible = isEdit;

            //Toggle Labels.
            item.FindControl("lblGoalPriority").Visible = !isEdit;
           

            //Toggle TextBoxes.
            item.FindControl("txtGoalPriority").Visible = isEdit;
           
        }

       
        void showMessage(string msg, string type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "swal({text: '" + msg + "',icon: '" + type + "'});", true);
        }
    }
}