﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;

namespace FincartApi
{

    
    public class Financial
    {
        public Double PV(Double rate, Double periods, Double payment, Double future, Double type)
        {

            if (type == null) type = 0.0;

            // Return present value
            if (rate == 0)
            {
                return -payment * periods - future;
            }
            else
            {
                return (((1 - Math.Pow(1 + rate, periods)) / rate) * payment * (1 + rate * type) - future) / Math.Pow(1 + rate, periods);
            }

        }
        public Double FV(Double rate, Double nper, Double pmt, Double pv, Double type)
        {
            if (type == null) type = 0.0;

            Double pow = Math.Pow(1 + rate, nper);
            Double fv = 0.0;

            if (rate != null)
            {
                fv = (pmt * (1 + rate * type) * (1 - pow) / rate) - pv * pow;
            }
            else
            {
                fv = -1 * (pv + pmt * nper);
            }

            return fv;
        }
        public Double PMT(Double rate, Double nper, Double pv, Double fv, Double type)
        {
            if (fv == null) fv = 0.0;
            if (type == null) type = 0.0;

            if (rate == 0) return -(pv + fv) / nper;

            Double pvif = Math.Pow(1 + rate, nper);
            Double pmt = rate / (pvif - 1) * -(pv * pvif + fv);

            if (type == 1)
            {
                pmt /= (1 + rate);
            };

            return pmt;
        }
        public Double ROR(Double time, String type)
        {

            Double ror = 6.0;

            if (type == "M")
            {//for monthly
                if (time <= 1)
                {
                    ror = 0.4868;
                }
                else if (time <= 3)
                {
                    ror = 0.5262;
                }
                else if (time <= 4)
                {
                    ror = 0.6821;
                }
                else if (time <= 7)
                {
                    ror = 0.7974;
                }
                else if (time <= 9)
                {
                    ror = 0.9489;
                }
                else
                {
                    ror = 1.1715;
                }
            }
            else
            {
                if (time <= 1)
                {
                    ror = 6.0;
                }
                else if (time <= 3)
                {
                    ror = 6.5;
                }
                else if (time <= 4)
                {
                    ror = 8.5;
                }
                else if (time <= 7)
                {
                    ror = 10.0;
                }
                else if (time <= 9)
                {
                    ror = 12.0;
                }
                else
                {
                    ror = 15.0;
                }
            }

            return ror;
        }

        public double inflation = 6.0 / 100;
        public double inflation10 = 10.0 / 100;
    }

}