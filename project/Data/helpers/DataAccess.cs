﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using System.IO;
using System.Drawing.Printing;

using System.Diagnostics;
using System.Reflection;
using System.Security.Claims;

using System.Globalization;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Newtonsoft.Json;
using data.model;
using System.Net;


namespace FinFinancialPlan
{

    public class DataAccess
    {

        SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["mycon"].ToString());


        /// <summary>
        /// helper function to fetch Dropdown List by Column Name from fincart database
        /// </summary>
        /// <param name="email"></param>
        /// <returns>List</returns>
        public List<KeyValue> getDropDownListByName(string columnkey,string columnvalue)
        {
            List<KeyValue> InflowTypList = new List<KeyValue>();
            List<KeyValue> sortedList = new List<KeyValue>();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ACTION", "Drop_down_master");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    Con.Open();
                    DA.Fill(ds);
                    Con.Close();

                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dr[columnkey])) && !string.IsNullOrWhiteSpace(Convert.ToString(dr[columnvalue])))
                                    {
                                        KeyValue ObjKeyVal = new KeyValue();
                                        ObjKeyVal.key = Convert.ToString(dr[columnkey]);
                                         ObjKeyVal.value = Convert.ToString(dr[columnvalue]);
                                         InflowTypList.Add(ObjKeyVal);   
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }

            sortedList = InflowTypList.OrderBy(s => s.value).ToList();
            return sortedList;
        }

        public List<KeyValue> getDropDownListByDisease(string columnkey, string columnvalue)
        {
            List<KeyValue> InflowTypList = new List<KeyValue>();

            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ACTION", "GET_Disease_Master");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    Con.Open();
                    DA.Fill(ds);
                    Con.Close();

                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dr[columnkey])) && !string.IsNullOrWhiteSpace(Convert.ToString(dr[columnvalue])))
                                    {
                                        KeyValue ObjKeyVal = new KeyValue();
                                        ObjKeyVal.key = Convert.ToString(dr[columnkey]);
                                        ObjKeyVal.value = Convert.ToString(dr[columnvalue]);
                                        InflowTypList.Add(ObjKeyVal);
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }

            return InflowTypList;
        }
        public List<KeyValue> getDropDownListByRelation(string columnkey, string columnvalue)
        {
            List<KeyValue> InflowTypList = new List<KeyValue>();

            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ACTION", "GET_Relation_Master");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    Con.Open();
                    DA.Fill(ds);
                    Con.Close();

                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dr[columnkey])) && !string.IsNullOrWhiteSpace(Convert.ToString(dr[columnvalue])))
                                    {
                                        KeyValue ObjKeyVal = new KeyValue();
                                        ObjKeyVal.key = Convert.ToString(dr[columnkey]);
                                        ObjKeyVal.value = Convert.ToString(dr[columnvalue]);
                                        InflowTypList.Add(ObjKeyVal);
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }

            return InflowTypList;
        }

        /// <summary>
        /// helper function to fetch usergoal List by userid from fincart database
        /// </summary>
        /// <param name="email"></param>
        /// <returns>List</returns>
        public List<KeyValue> getUserGoalsByUserid(string userid)
        {
            List<KeyValue> GoalList = new List<KeyValue>();

            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_GOAL_Name_by_userid");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    Con.Open();
                    DA.Fill(ds);
                    Con.Close();

                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["ID"])) && !string.IsNullOrWhiteSpace(Convert.ToString(dr["Goal_Name"])))
                                    {
                                        KeyValue ObjKeyVal = new KeyValue();
                                        ObjKeyVal.key = Convert.ToString(dr["Goal_Name"]);
                                        ObjKeyVal.value = Convert.ToString(dr["ID"]);
                                        GoalList.Add(ObjKeyVal);
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }

            return GoalList;
        }
        /// <summary>
        /// helper function to insert cashflowIn entry in fincart database
        /// </summary>
        /// <param name="email"></param>
        /// <returns>List</returns>
        public bool saveCashflowIn(cashflowIn cashIn)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@frequency", cashIn.frequency);
                    cmd.Parameters.AddWithValue("@basicid", cashIn.basicId);
                    cmd.Parameters.AddWithValue("@amount", cashIn.amount);
                    cmd.Parameters.AddWithValue("@income_type_name", cashIn.inflowTypeName);
                    cmd.Parameters.AddWithValue("@Income_type", cashIn.inflowType);
                    cmd.Parameters.AddWithValue("@planId", cashIn.planId);
                    cmd.Parameters.AddWithValue("@createdby", cashIn.createdBy);
                    cmd.Parameters.AddWithValue("@updatedby", cashIn.updatedBy);
                    cmd.Parameters.AddWithValue("@ACTION", "Insertcashinflow");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }


        /// <summary>
        /// helper function to select all cashflowIn entry by userid from fincart database
        /// </summary>
        /// <param name="email"></param>
        /// <returns>List</returns>
        public DataTable getAllCashFlowInByUserId(string userid,string planid)
        {
            
            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@planId", planid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_Cashin_Details");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }



        public DataTable getAllPersonalDetailsUserId(string userid , string basicid)
        {

            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                
                    cmd.Parameters.AddWithValue("@ACTION", "Get_All_Client_Details");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }



        public DataTable getuserpassword(string basicid)
        {

            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);

                    cmd.Parameters.AddWithValue("@ACTION", "Get_user_pass");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }

        /// <summary>
        /// helper function to remove cashflowIn entry in fincart database
        /// </summary>
        /// <param name="cashflowId"></param>
        /// <returns>List</returns>
        public bool removeCashflowInById(string cashflowId,string basicid,string planId)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", cashflowId);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "Deletecashinflow");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }

        /// <summary>
        /// helper function to select single cashflowIn entry by cashflowId in fincart database
        /// </summary>
        /// <param name="cashflowId"></param>
        /// <returns>DataTable</returns>
        public DataTable getCashflowInById(string cashflowId,string basicid,string planId)
        {
            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", cashflowId);
                    cmd.Parameters.AddWithValue("@planid", planId);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_Current_Cashin_Basis_id");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }

        public DataTable getpersonalById( string basicid)
        {
            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_Client_Details_byBasicID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }

        /// <summary>
        /// helper function to insert cashflowIn entry in fincart database
        /// </summary>
        /// <param name="email"></param>
        /// <returns>List</returns>
        public bool updateCashflowInById(cashflowIn cashIn)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@frequency", cashIn.frequency);
                    cmd.Parameters.AddWithValue("@basicid", cashIn.basicId);
                    cmd.Parameters.AddWithValue("@amount", cashIn.amount);
                    cmd.Parameters.AddWithValue("@income_type_name", cashIn.inflowTypeName);
                    cmd.Parameters.AddWithValue("@Income_type", cashIn.inflowType);
                    cmd.Parameters.AddWithValue("@updatedBy", cashIn.updatedBy);
                    cmd.Parameters.AddWithValue("@Id", cashIn.id);
                    cmd.Parameters.AddWithValue("@planId", cashIn.planId);
                    cmd.Parameters.AddWithValue("@ACTION", "updatecashinflow");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }


        /// <summary>
        /// helper function to select all cashflowOut entry by userid from fincart database
        /// </summary>
        /// <param name="email"></param>
        /// <returns>List</returns>
        public DataTable getAllCashFlowOutByUserId(string userid,string planId)
        {

            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_Cashout_Details");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }

        /// <summary>
        /// helper function to insert cashflowOut entry in fincart database
        /// </summary>
        /// <param name="CashflowOut"></param>
        /// <returns>List</returns>
        public bool saveCashflowOut(CashflowOut cashOut)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@exptype", cashOut.expenseType);
                    cmd.Parameters.AddWithValue("@Id", cashOut.id);
                    cmd.Parameters.AddWithValue("@basicid", cashOut.basicId);
                    cmd.Parameters.AddWithValue("@amount", cashOut.amount);
                    cmd.Parameters.AddWithValue("@outflow_type", cashOut.OutflowType);
                    cmd.Parameters.AddWithValue("@outflow_type_name", cashOut.OutflowOthTypeName);
                    cmd.Parameters.AddWithValue("@assettypeid", cashOut.assetId);
                    cmd.Parameters.AddWithValue("@assettype", cashOut.assetType);
                    cmd.Parameters.AddWithValue("@planId", cashOut.planId);
                    cmd.Parameters.AddWithValue("@createdBy", cashOut.createdBy);
                    cmd.Parameters.AddWithValue("@updatedBy", cashOut.updatedBy);
                    cmd.Parameters.AddWithValue("@ACTION", "insertoutflowflow");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }


        /// <summary>
        /// helper function to select single cashflowOut entry by cashflowId in fincart database
        /// </summary>
        /// <param name="cashflowId"></param>
        /// <returns>DataTable</returns>
        public DataTable getCashflowOutById(string cashflowId, string basicid,string planId)
        {
            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", cashflowId);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_Current_Cashout_Basis_id");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }

        /// <summary>
        /// helper function to remove cashflowIn entry in fincart database
        /// </summary>
        /// <param name="cashflowId"></param>
        /// <returns>List</returns>
        public bool removeCashflowOutById(string cashflowId, string basicid,string planId)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", cashflowId);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "Deletecashoutflow");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }

        /// <summary>
        /// helper function to get all Investments by basicId from fincart database
        /// </summary>
        /// <param name="basicid"></param>
        /// <returns>DataTable</returns>
        public DataTable getAllInvestmentsByBasicId(string basicId,string planId)
        {

            DataTable dtInvestments = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "get_curernt_investment_by_basicID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtInvestments);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtInvestments;

        }


        /// <summary>
        /// helper function to insert Invemtment Mapping in cashflowOut table in fincart database
        /// </summary>
        /// <param name="CashflowOut"></param>
        /// <returns>List</returns>
        public bool MapInvestment(CashflowOut cashOut)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@exptype", cashOut.expenseType);
                    cmd.Parameters.AddWithValue("@Id", cashOut.id);
                    cmd.Parameters.AddWithValue("@basicid", cashOut.basicId);
                    cmd.Parameters.AddWithValue("@amount", cashOut.amount);
                    cmd.Parameters.AddWithValue("@outflow_type", cashOut.OutflowType);
                    cmd.Parameters.AddWithValue("@outflow_type_name", cashOut.OutflowOthTypeName);
                    cmd.Parameters.AddWithValue("@assettypeid", cashOut.assetId); // ID OF RECORD
                    cmd.Parameters.AddWithValue("@Ass_type_id", cashOut.assetType); // ID OF TTYPE MASTER
                    cmd.Parameters.AddWithValue("@planId", cashOut.planId);
                    cmd.Parameters.AddWithValue("@createdBy", cashOut.createdBy);
                    cmd.Parameters.AddWithValue("@updatedBy", cashOut.updatedBy);
                    cmd.Parameters.AddWithValue("@ACTION", "map_investment_in_cashout");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);
        }
        /// <summary>
        /// helper function to insert LoanAsset Mapping in cashflowOut table in fincart database
        /// </summary>
        /// <param name="CashflowOut"></param>
        /// <returns>List</returns>
        public bool MapToCashOut(CashflowOut cashOut)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@basicid", cashOut.basicId);
                    cmd.Parameters.AddWithValue("@amount", cashOut.amount);
                    cmd.Parameters.AddWithValue("@assettypeid", cashOut.assetId); // ID OF RECORD
                    cmd.Parameters.AddWithValue("@Ass_type_id", cashOut.assetType); // ID OF TTYPE MASTER
                    cmd.Parameters.AddWithValue("@planId", cashOut.planId);
                    cmd.Parameters.AddWithValue("@createdBy", cashOut.createdBy);
                    cmd.Parameters.AddWithValue("@updatedBy", cashOut.updatedBy);
                    cmd.Parameters.AddWithValue("@ACTION", "map_loan_in_cashout");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);
        }



        /// <summary>
        /// helper function to get all mapped Investments by basicId from fincart database
        /// </summary>
        /// <param name="basicid"></param>
        /// <returns>DataTable</returns>
        public DataTable getAllMappedInvstmtByBasicId(string basicId, string planId)
        {

            DataTable dtInvestments = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "get_investment_from_cashout_by_basicID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtInvestments);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtInvestments;

        }

        /// <summary>
        /// helper function to get all LoansOther Assets by basicId from fincart database
        /// </summary>
        /// <param name="basicid"></param>
        /// <returns>DataTable</returns>
        public DataTable getLoanAndOtherAssetByBasicId(string basicId,string planId)
        {

            DataTable dtLoanOther = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_ALL_LOAN_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtLoanOther);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtLoanOther;

        }

        /// <summary>
        /// helper function to get all Mapped LoansAssets by basicId from fincart database
        /// </summary>
        /// <param name="basicid"></param>
        /// <returns>DataTable</returns>
        public DataTable getAllMappedLoanAssetByBasicId(string basicId, string planId)
        {

            DataTable dtLoanOther = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_ALL_MAPPED_LOANASSETS_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtLoanOther);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtLoanOther;

        }


        // get current cash flow//
        public DataSet getCashflowBybasicId(string userid, string basicid , string planid)
        {
            DataSet dtCashflowIn = new DataSet();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@planid", planid);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_Current_Cashflow");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }

        // get current revised cash flow//

        public DataSet getrevisedCashflowBybasicId(string userid, string basicid, string planid)
        {
            DataSet dtCashflowIn = new DataSet();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@planid", planid);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_Revised_Cashflow_For_Plan");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }


        public DataSet getPolicyBybasicId(string basicid)
        {
            DataSet dtCashflowIn = new DataSet();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_all_data_for_risk_proc_page");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }





        //get function for wayforward


        public DataSet getwayforward(string basicid , string planid )
        {
            DataSet dtCashflowIn = new DataSet();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@planid", planid);

                    cmd.Parameters.AddWithValue("@ACTION", "WayForward_cal_for_Plan_page");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }








        //<summary>
        //get the revised cashflow

        public DataSet getRevisedCashflowBybasicId(string userid, string basicid, int planid)
        {
            DataSet dtCashflowIn = new DataSet();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@planid", planid);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_Current_Realitycheck");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }





        /// <summary>
        /// helper function to get calculated Used Surplus sum by planid and basicid from fincart database
        /// </summary>
        /// <param name="basicid"></param>
        /// <param name="strPlanId"></param>
        /// <returns>string</returns>
        public string getTotalUsedSurplus(string basicid, string strPlanId)
        {
            string strCurrentSurplus = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@planId", strPlanId);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_Used_Surplus");
                    Con.Open();
                    strCurrentSurplus = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return strCurrentSurplus;

        }


        /// <summary>
        /// helper function to get calculated current Surplus from fincart database
        /// </summary>
        /// <param name="basicid"></param>
        /// <param name="strPlanId"></param>
        /// <returns>string</returns>
        public string getCurrentSurplus(string basicid, string strPlanId)
        {
            string strCurrentSurplus = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@planId", strPlanId);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_current_Surplus");
                    Con.Open();
                    strCurrentSurplus = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return strCurrentSurplus;

        }

        /// <summary>
        /// helper function to insert fund allocation in fincart database
        /// </summary>
        /// <param name="fundAllocate"></param>
        /// <returns>string</returns>
        public string saveNetCashFlowAllocation(fundAllocate alloc)
        {
            string res = string.Empty;
            try
            {
                DataSet SURPLUS = new DataSet();
                DataTable SURPL = new DataTable();
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", alloc.basicId);
                    cmd.Parameters.AddWithValue("@planId", alloc.planId);
                    cmd.Parameters.AddWithValue("@usergoalid", alloc.usergoalId);
                    cmd.Parameters.AddWithValue("@SchemeID", alloc.schemeId);
                    cmd.Parameters.AddWithValue("@amount", alloc.amount);
                    cmd.Parameters.AddWithValue("@futureCost", alloc.futurecost);
                    cmd.Parameters.AddWithValue("@MFtransType", alloc.mfTrxnType);
                    cmd.Parameters.AddWithValue("@createdby", alloc.createdBy);
                    cmd.Parameters.AddWithValue("@ACTION", "ADD_NETCASHFLOW_TO_SCHEMESELECTION");
                    Con.Open();
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    DA.Fill(SURPLUS);
                    SURPL = SURPLUS.Tables[1];
                    res = SURPL.Rows[0][0].ToString();    
                    //res = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
                
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return res;

        }

        /// <summary>
        /// helper function to get fund allocation in fincart database
        /// </summary>
        /// <param name="fundAllocate"></param>
        /// <returns>string</returns>
        public DataTable getAllGoalAllocationByBasicId(string basicId , string planid)
        {

            DataTable dtLoanOther = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planid", planid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_ALL_GOAL_ALLOCATION_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtLoanOther);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtLoanOther;

        }

        /// <summary>
        /// helper function to remove Fund allocation entry in fincart database
        /// </summary>
        /// <param name="fndAllocId"></param>
        /// <returns>string</returns>
        public string removeNetCashFlowAllocationById(string fndAllocId, string basicid , string planid)
        {
            string res = string.Empty;
            try
            {
                DataSet SURPLUS = new DataSet();
                DataTable SURPL = new DataTable();
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", fndAllocId);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@Planid", planid);
                    cmd.Parameters.AddWithValue("@ACTION", "DELETE_NETCASHFLOW_ALLOC_BY_ID");
                    Con.Open();
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    DA.Fill(SURPLUS);
                    SURPL = SURPLUS.Tables[0];
                    res = SURPL.Rows[0][0].ToString();
                    //res = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return res;

        }

        /// <summary>
        /// helper function to get single fund allocation record by fund allocation id in fincart database
        /// </summary>
        /// <param name="fundAllocate"></param>
        /// <param name="basicId"></param> 
        /// <returns>DataTable</returns>
        public DataTable getNetCashFlowAllocationById(string fndAllocId, string basicId,string planId)
        {
            DataTable dtFundAllocTbl = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", fndAllocId);
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_NETCASHFLOW_SCHEMESELECTION_BY_ID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtFundAllocTbl);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtFundAllocTbl;

        }

        /// <summary>
        /// helper function to update fund allocation entry by ID & BasicId in fincart database
        /// </summary>
        /// <param name="alloc" type="fundAllocate"></param>
        /// 
        /// <returns>string</returns>
        public string updateNetCashFlowAllocation(fundAllocate alloc)
        {
            string res = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID", alloc.Id);
                    cmd.Parameters.AddWithValue("@BasicID", alloc.basicId);
                    cmd.Parameters.AddWithValue("@planId", alloc.planId);
                    cmd.Parameters.AddWithValue("@usergoalid", alloc.usergoalId);
                    cmd.Parameters.AddWithValue("@SchemeID", alloc.schemeId);
                    cmd.Parameters.AddWithValue("@amount", alloc.amount);
                    cmd.Parameters.AddWithValue("@futureCost", alloc.futurecost);
                    cmd.Parameters.AddWithValue("@MFtransType", alloc.mfTrxnType);
                    cmd.Parameters.AddWithValue("@createdby", alloc.createdBy);

                    cmd.Parameters.AddWithValue("@ACTION", "UPDATE_NETCASHFLOW_IN_SCHEMESELECTION");
                    Con.Open();
                    res = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
               
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return res;

        }

        /// <summary>
        /// helper function to get calculated current Surplus from fincart database
        /// </summary>
        /// <param name="basicid"></param>
        /// <returns>string</returns>
        public string getConstantSurplus(string basicid,string strPlanId)
        {
            string strConstantSurplus = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@planid", strPlanId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_CONSTANT_SURPLUS");
                    Con.Open();
                    strConstantSurplus = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return strConstantSurplus;
        }

        /// <summary>
        /// helper function to get all Insurance by basicID from Insurance Table in fincart database
        /// </summary>
        /// <param name="basicId"></param>
        /// <returns>DataTable</returns>
        public DataTable getAllInsuranceByBasicId(string basicId,string planId)
        {

            DataTable dtInsurance = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_INSURANCE_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtInsurance);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtInsurance;

        }

        /// <summary>
        /// helper function to Update cashflow Out entry by basicId and ID in fincart database
        /// </summary>
        /// <param name="CashflowOut"></param>
        /// <returns>bool</returns>
        public bool updateCashflowOutByIdBasicId(CashflowOut cashOut)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicId", cashOut.basicId);
                    cmd.Parameters.AddWithValue("@Id", cashOut.id);
                    cmd.Parameters.AddWithValue("@updatedBy", cashOut.updatedBy);
                    cmd.Parameters.AddWithValue("@exptype", cashOut.expenseType);
                    cmd.Parameters.AddWithValue("@amount", cashOut.amount);
                    cmd.Parameters.AddWithValue("@outflow_type", cashOut.OutflowType);
                    cmd.Parameters.AddWithValue("@outflow_type_name", cashOut.OutflowOthTypeName);
                    cmd.Parameters.AddWithValue("@assettypeid", cashOut.assetId);
                    cmd.Parameters.AddWithValue("@assettype", cashOut.assetType);
                    cmd.Parameters.AddWithValue("@planId", cashOut.planId);
                    cmd.Parameters.AddWithValue("@ACTION", "updatecashoutflow");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }

        public DataTable getclientnamebybasicid(string basicid)
        {

            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_CLIENT_NAME");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;


            


        }

        public DataTable getclientpersonaldedails(string userid)
        {

            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_CLIENT_DETAILS");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);                   
                    Con.Close();
               
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;





        }

        public DataTable getclientNameandSalutation(string basicid)
        {

            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_CLIENT_NAME_WITHSALUTATION");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);

                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;





        }

        public DataTable getclientfeesbybasicid(string basicid)
        {

            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_client_fees_by_basic_id");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);

                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;





        }


        public DataTable getclientgoalbybasicid(string basicid , string planid)
        {

            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicid);
                    cmd.Parameters.AddWithValue("@planid", planid);

                    cmd.Parameters.AddWithValue("@ACTION", "GET_ALL_GOAL_LIST_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;





        }
        public DataTable getclientgoalForPlanGeneratebybasicid(string basicid, string planid)
        {

            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicid);
                    cmd.Parameters.AddWithValue("@planid", planid);

                    cmd.Parameters.AddWithValue("@ACTION", "GET_ALL_GOAL_list_For_plan");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;





        }


         public DataTable getExistingPlanGeneratebybasicid(string basicid)
        {

            DataTable dtCashflowIn = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_existing_plan_list_for_merge");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;





        }
         public DataTable EditExistingPlanGeneratebybasicidandplanid(string basicid , string plaind)
         {

             DataTable dtCashflowIn = new DataTable();
             try
             {
                 using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                 {
                     cmd.CommandType = CommandType.StoredProcedure;
                     cmd.Parameters.AddWithValue("@basicid", basicid);
                     cmd.Parameters.AddWithValue("@ACTION", "Get_existing_plan_list_for_merge");
                     SqlDataAdapter DA = new SqlDataAdapter(cmd);
                     Con.Open();
                     DA.Fill(dtCashflowIn);
                     Con.Close();
                 }
             }
             catch (Exception ex)
             {
                 Con.Close();
             }
             finally
             {
                 //Con.Dispose();
             }
             return dtCashflowIn;





         }



        

        /// <summary>
        /// helper function to get all Mapped Insurance by PlanId,basicId from Insurance Table in fincart database
        /// </summary>
        /// <param name="basicId"></param>
        /// <param name="planId"></param>
        /// <returns>DataTable</returns>
        public DataTable getAllInsuranceMappedById(string basicId, string planId)
        {

            DataTable dtInsurance = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_MAPPED_INSURANCE");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtInsurance);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtInsurance;

    }

        /// <summary>
        /// helper function to get all assets by basicID & planid from fincart database
        /// </summary>
        /// <param name="basicId"></param>
        /// <param name="planId"></param>
        /// <returns>DataTable</returns>
        public DataTable getAllAssetsById(string basicId, string planId)
        {

            DataTable dtAssets = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_INS_AVAL_RES_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtAssets);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtAssets;

        }

    

        /// <summary>
        /// helper function to insert Assets Mapping in AssetMap table in fincart database
        /// </summary>
        /// <param name="Assets"></param>
        /// <returns>DataTable</returns>
        public DataTable MapAssets(Assets assts)
        {
            DataTable dtAssetsMapStatus = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@asset_id", assts.assetId);
                    cmd.Parameters.AddWithValue("@basicid", assts.basicId);
                    cmd.Parameters.AddWithValue("@amount", assts.amount);
                    cmd.Parameters.AddWithValue("@futureCost", assts.futureCost);
                    cmd.Parameters.AddWithValue("@deficit", assts.deficit);
                    cmd.Parameters.AddWithValue("@ror", assts.ror);
                    cmd.Parameters.AddWithValue("@pmt", assts.pmt);
                    cmd.Parameters.AddWithValue("@isMfPool", assts.isMfPool);
                    cmd.Parameters.AddWithValue("@payPeriod", assts.payPeriod);
                    cmd.Parameters.AddWithValue("@ass_type_id", assts.assetType);
                    cmd.Parameters.AddWithValue("@planId", assts.planId);
                    cmd.Parameters.AddWithValue("@usergoalid", assts.usergoalId);
                    cmd.Parameters.AddWithValue("@createdBy", assts.createdBy);
                    cmd.Parameters.AddWithValue("@updatedBy", assts.updatedBy);
                    cmd.Parameters.AddWithValue("@ACTION", "Map_asset_to_dgs");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtAssetsMapStatus);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtAssetsMapStatus;
        }

        /// <summary>
        /// helper function to get all mapped assets by basicID & planid from fincart database
        /// </summary>
        /// <param name="basicId"></param>
        /// <param name="planId"></param>
        /// <returns>DataTable</returns>
        public DataTable getAllMappedAssetsById(string basicId, string planId)
        {

            DataTable dtAssets = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_Mapped_asset_to_dgs");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtAssets);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtAssets;

        }
        
        public DataTable getMappedAssetsById(string Id,string assetID, string basicId, string planId)
        {

            DataTable dtAssets = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", Id);
                    cmd.Parameters.AddWithValue("@asset_id", assetID);
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_MAPPED_ASSET_BY_ID_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtAssets);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtAssets;

        }
        /// <summary>
        /// helper function to remove AssetMapping entry in fincart database
        /// </summary>
        /// <param name="assetMapId"></param>
        /// <returns>bool</returns>
        public bool removeAssetMappingById(string assetMapId,string assetTypeID, string basicid, string planId)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", assetMapId);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@assettypeid", assetTypeID);
                    cmd.Parameters.AddWithValue("@ACTION", "Delete_mapped_asset");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }
        public string addmember(addmember adm )
        {
            DataTable dtAssetsMapStatus = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", adm.basicId);
                    cmd.Parameters.AddWithValue("@gender", adm.gender);
                    cmd.Parameters.AddWithValue("@dob", adm.dob);
                    cmd.Parameters.AddWithValue("@compname", adm.compname);
                    cmd.Parameters.AddWithValue("@designation", adm.designation);
                    cmd.Parameters.AddWithValue("@Relationship_status", adm.Relation);
                    cmd.Parameters.AddWithValue("@marital_status", adm.maritalstatus);
                    cmd.Parameters.AddWithValue("@isinvesting", adm.isinvesting);
                    cmd.Parameters.AddWithValue("@disease_Status", adm.health);
                    cmd.Parameters.AddWithValue("@ACTION", "UPDATE_PERSONAL_DETAILS");
                    Con.Open();
                    cmd.ExecuteNonQuery();
                    Con.Close();
                    
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            return adm.clientname.ToString(); ;
        }

        public DataTable generateplanid(string userid, string basicid , string planname, string empid)
        {

            DataTable getplainid = new DataTable();
            DataSet getpalnid = new DataSet();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@updatedby", empid);
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@createdby", empid);
                    cmd.Parameters.AddWithValue("@empid", empid );
                    cmd.Parameters.AddWithValue("@planname", planname);
                    
                    cmd.Parameters.AddWithValue("@ACTION", "Insert_plan_details_in_master");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(getpalnid);
                    getplainid = getpalnid.Tables[1];
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return getplainid;

        }






        public DataSet getAllAvailres(string basicid, string plainid, string strUserId)
        {

            DataSet dtAssets = new DataSet();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicid);
                    cmd.Parameters.AddWithValue("@userid", strUserId);
                    cmd.Parameters.AddWithValue("@planId", plainid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_availible_resource_for_plan");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtAssets);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtAssets;








        }


        public DataSet getAllRetirement(string basicid, string plainid, string strUserId)
        {

            DataSet dtAssets = new DataSet();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicid);
                    cmd.Parameters.AddWithValue("@planId", plainid);
                    cmd.Parameters.AddWithValue("@ACTION", "Retirment_cal_for_Plan");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtAssets);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtAssets;








        }

        /// <summary>
        /// helper function to get plan details from PlanMaster by ID database
        /// </summary>
        /// <param name="basicId"></param>
        /// <param name="planId"></param>
        /// <returns>DataTable</returns>
        public DataTable getPlanMasterDetailsById(string basicId, string planId)
        {

            DataTable dtPlanMaster = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_PLAN_DETAILS_BY_BASICID_PLANID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtPlanMaster);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtPlanMaster;

        }




        public DataTable getSelected_sceme_by_basicid_for_plan(string basicId, string planId)
        {

            DataTable dtPlanMaster = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_Selected_SCHEME_BY_BASICID_ID_for_plan_gen");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtPlanMaster);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtPlanMaster;

        }


        /// <summary>
        /// helper function to Set LastPage Working by ID database
        /// </summary>
        /// <param name="basicId"></param>
        /// <param name="planId"></param>
        /// <param name="lastPageWorking"></param>
        /// <returns>DataTable</returns>
        public bool SetLastPageWorkingById(string basicId, string planId,string lastPageWorking)
        {

            bool resp = false;
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@lastPageWorking", lastPageWorking);
                    cmd.Parameters.AddWithValue("@ACTION", "UPDATE_PLAN_LASTPAGE_WORKING_BY_PLANID_BASICID");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    resp = true;
                }
                else { resp = false; }
            
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return resp;

        }


        /// <summary>
        /// helper function to fetch Dropdown List for Available Resources from fincart database
        /// </summary>
        /// <param name="basicId"></param>
        /// <returns>List</returns>
        public List<KeyValue> getDropDownListForAvailRes(string basicId)
        {
            List<KeyValue> ddlAvailResList = new List<KeyValue>();

            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_AVAl_RES_DROPDOWN_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    Con.Open();
                    DA.Fill(ds);
                    Con.Close();

                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["assetId"])) && !string.IsNullOrWhiteSpace(Convert.ToString(dr["AssetName"])))
                                    {
                                        KeyValue ObjKeyVal = new KeyValue();
                                        ObjKeyVal.key = Convert.ToString(dr["Assetname"]);
                                        ObjKeyVal.value = Convert.ToString(dr["assetId"]);
                                        ddlAvailResList.Add(ObjKeyVal);
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }

            return ddlAvailResList;
        }

        /// <summary>
        /// helper function to fetch Dropdown List for User Goals list from fincart database
        /// </summary>
        /// <param name="basicId"></param>
        /// <returns>List</returns>
        public List<KeyValue> getDropDownListForGoals(string basicId)
        {
            List<KeyValue> ddlGoalList = new List<KeyValue>();

            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicId);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_All_Client_Goals_Details");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    Con.Open();
                    DA.Fill(ds);
                    Con.Close();

                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["userGoalId"])) && !string.IsNullOrWhiteSpace(Convert.ToString(dr["goalName"])))
                                    {
                                        KeyValue ObjKeyVal = new KeyValue();
                                        ObjKeyVal.key = Convert.ToString(dr["goalname"]);
                                        ObjKeyVal.value = Convert.ToString(dr["userGoalId"]);
                                        ddlGoalList.Add(ObjKeyVal);
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }

            return ddlGoalList;
        }

        /// <summary>
        /// helper function to fetch Individual Available Resource Amount and BalAmount from fincart database
        /// </summary>
        /// <param name="basicId"></param>
        /// <returns>List</returns>
        public DataTable getAvailResBalanceAmount(string basicId,string planid,string assetId)
        {
            DataTable availResBalAmt = new DataTable();
           
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicId);
                    cmd.Parameters.AddWithValue("@planid", planid);
                    cmd.Parameters.AddWithValue("@assetId", assetId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_AVAILRES_AMT_BALAMT_BY_BASICID_PLANID_ASSETID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(availResBalAmt);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return availResBalAmt;
        }

        public bool AddLoanEntry(LoanEntry loan)
        {
            bool resFinal = false;
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", loan.basicId);
                    cmd.Parameters.AddWithValue("@updatedby", loan.updatedBy);
                    cmd.Parameters.AddWithValue("@loan_type", loan.AssetName);
                    cmd.Parameters.AddWithValue("@startdate", loan.startDate);
                    cmd.Parameters.AddWithValue("@varAmount", loan.amount);
                    cmd.Parameters.AddWithValue("@emiamount", loan.emi);
                    cmd.Parameters.AddWithValue("@curn_val", loan.currentValue);
                    cmd.Parameters.AddWithValue("@interest", loan.interest);
                    cmd.Parameters.AddWithValue("@enddate", loan.endDate);
                    cmd.Parameters.AddWithValue("@trxnSource", loan.trxnSource);
                    cmd.Parameters.AddWithValue("@ACTION", "Insert_loan_entry_for_client_dgs");
                    Con.Open();
                    res= cmd.ExecuteNonQuery();
                    Con.Close();

                }

                if (res > 0)
                {
                    resFinal = true;

                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            return resFinal;
        }
        public DataTable getAllLoansById(string basicId)
        {

            DataTable dtLoans = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_all_loan_data_by_basicid");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtLoans);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtLoans;

        }


        public bool removeLoanEntryById(string Id,string basicid)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "delete_loan_entry_for_user_id");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }

        public DataTable getLoanEntryById(string loanId, string basicid)
        {
            DataTable dtLoanEntry = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", loanId);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_loan_entry_by_ID_basicid");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtLoanEntry);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtLoanEntry;

        }

        public bool updateLoanEntryById(LoanEntry loan)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", loan.basicId);
                    cmd.Parameters.AddWithValue("@ID", loan.Id);
                    cmd.Parameters.AddWithValue("@loan_type", loan.AssetName);
                    cmd.Parameters.AddWithValue("@startdate", loan.startDate);
                    cmd.Parameters.AddWithValue("@amount", loan.amount);
                    cmd.Parameters.AddWithValue("@emiamount", loan.emi);
                    cmd.Parameters.AddWithValue("@curn_val", loan.currentValue);
                    cmd.Parameters.AddWithValue("@interest", loan.interest);
                    cmd.Parameters.AddWithValue("@enddate", loan.endDate);
                    cmd.Parameters.AddWithValue("@trxnSource", loan.trxnSource);
                    cmd.Parameters.AddWithValue("@ACTION", "Update_loan_entry_id");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }

        public DataTable getAllPolicyProvidersList()
        {
            DataTable dtPolicyProvdr = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ACTION", "GET_POLICY_PROVIDER_ALL_LIST");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtPolicyProvdr);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtPolicyProvdr;

        }

        public DataTable getAllInsuranceByBasicId(string basicId)
        {

            DataTable dtInsurance = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_ALL_INSURANCE_ENTRY_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtInsurance);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtInsurance;

        }

        public bool AddInsuranceEntry(InsuranceEntry insur)
        {
            bool resFinal = false;
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", insur.basicId);
                    cmd.Parameters.AddWithValue("@updatedby", insur.updatedBy);
                    cmd.Parameters.AddWithValue("@assettypeid", insur.AssetTypeId);
                    cmd.Parameters.AddWithValue("@policyPartnerId", insur.policyPartnerId);
                    cmd.Parameters.AddWithValue("@policyNo", insur.policyNo);
                    cmd.Parameters.AddWithValue("@startdate", insur.policyIssueDt);
                    cmd.Parameters.AddWithValue("@policyTerm", insur.policyTerm);
                    cmd.Parameters.AddWithValue("@policySumAssured", insur.policySumAssured);
                    cmd.Parameters.AddWithValue("@curn_val", insur.currentValue);
                    cmd.Parameters.AddWithValue("@policyPremium", insur.policyPremium);
                    cmd.Parameters.AddWithValue("@assetname", insur.AssetName);
                    cmd.Parameters.AddWithValue("@isActivePolicy", insur.isActivePolicy);
                    cmd.Parameters.AddWithValue("@PolicyOwnerID", insur.policyOwnerBasicId);
                    cmd.Parameters.AddWithValue("@trxnSource", insur.trxnSource);
                    cmd.Parameters.AddWithValue("@lockinDuration", insur.lockInDuration);
                    cmd.Parameters.AddWithValue("@lockinInterest", insur.lockInInterest);
                    cmd.Parameters.AddWithValue("@CafCurrHLV", insur.finalHlv);
                    cmd.Parameters.AddWithValue("@ACTION", "SAVE_INSURANCE_ENTRY");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();

                }

                if (res > 0)
                {
                    resFinal = true;

                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            return resFinal;
        }

        public bool removeInsuranceEntryById(string Id, string basicid)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "REMOVE_INSURANCE_ENTRY_BY_ID_BASICID");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }

        public DataTable getInsuranceEntryById(string insurId, string basicid)
        {
            DataTable dtInsurEntry = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", insurId);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_INSURANCE_ENTRY_BY_ID_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtInsurEntry);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtInsurEntry;

        }

        public bool updateInsuranceEntryById(InsuranceEntry insur)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID", insur.Id);
                    cmd.Parameters.AddWithValue("@BasicID", insur.basicId);
                    cmd.Parameters.AddWithValue("@updatedby", insur.updatedBy);
                    cmd.Parameters.AddWithValue("@assettypeid", insur.AssetTypeId);
                    cmd.Parameters.AddWithValue("@policyPartnerId", insur.policyPartnerId);
                    cmd.Parameters.AddWithValue("@policyNo", insur.policyNo);
                    cmd.Parameters.AddWithValue("@startdate", insur.policyIssueDt);
                    cmd.Parameters.AddWithValue("@policyTerm", insur.policyTerm);
                    cmd.Parameters.AddWithValue("@policySumAssured", insur.policySumAssured);
                    cmd.Parameters.AddWithValue("@curn_val", insur.currentValue);
                    cmd.Parameters.AddWithValue("@policyPremium", insur.policyPremium);
                    cmd.Parameters.AddWithValue("@assetname", insur.AssetName);
                    cmd.Parameters.AddWithValue("@isActivePolicy", insur.isActivePolicy);
                    cmd.Parameters.AddWithValue("@PolicyOwnerID", insur.policyOwnerBasicId);
                    cmd.Parameters.AddWithValue("@trxnSource", insur.trxnSource);
                    cmd.Parameters.AddWithValue("@lockinDuration", insur.lockInDuration);
                    cmd.Parameters.AddWithValue("@lockinInterest", insur.lockInInterest);
                    cmd.Parameters.AddWithValue("@CafCurrHLV", insur.finalHlv);
                    cmd.Parameters.AddWithValue("@ACTION", "UPDATE_INSURANCE_ENTRY_BY_ID_BASICID");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }


        //****** OTHER ASSET ENTRY HELPER FUNCTIONS **********
        public bool AddOtherAssetEntry(LoanEntry loan)
        {
            bool resFinal = false;
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", loan.basicId);
                    cmd.Parameters.AddWithValue("@updatedby", loan.updatedBy);
                    cmd.Parameters.AddWithValue("@assetname", loan.AssetName);
                    cmd.Parameters.AddWithValue("@assettypeid", loan.AssetTypeId);
                    cmd.Parameters.AddWithValue("@curn_val", loan.amount);
                    cmd.Parameters.AddWithValue("@liquidityType", loan.LiquidityType);
                    cmd.Parameters.AddWithValue("@lockinDuration", loan.lockInDuration);
                    cmd.Parameters.AddWithValue("@lockinInterest", loan.lockInInterest);
                    cmd.Parameters.AddWithValue("@trxnSource", loan.trxnSource);
                    cmd.Parameters.AddWithValue("@ACTION", "SAVE_OTHER_ASSET_ENTRY");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();

                }

                if (res > 0)
                {
                    resFinal = true;

                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            return resFinal;
        }
        public bool updateOtherAssetEntryById(LoanEntry loan)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", loan.basicId);
                    cmd.Parameters.AddWithValue("@ID", loan.Id);
                    cmd.Parameters.AddWithValue("@assetname", loan.AssetName);
                    cmd.Parameters.AddWithValue("@curn_val", loan.amount);
                    cmd.Parameters.AddWithValue("@liquidityType", loan.LiquidityType);
                    cmd.Parameters.AddWithValue("@assettypeid", loan.AssetTypeId);
                    cmd.Parameters.AddWithValue("@lockinDuration", loan.lockInDuration);
                    cmd.Parameters.AddWithValue("@lockinInterest", loan.lockInInterest);
                    cmd.Parameters.AddWithValue("@trxnSource", loan.trxnSource);
                    cmd.Parameters.AddWithValue("@ACTION", "UPDATE_OTHER_ASSET_ENTRY_BY_ID_BASICID");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }
        public DataTable getAllOtherAssetByBasicId(string basicId)
        {

            DataTable dtInsurance = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_ALL_OTHER_ASSETS_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtInsurance);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtInsurance;

        }
        public DataTable getOtherAssetEntryById(string loanId, string basicid)
        {
            DataTable dtInsurEntry = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", loanId);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_OTHER_ASSETS_BY_ID_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtInsurEntry);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtInsurEntry;

        }

        //****** OTHER ASSET ENTRY HELPER FUNCTIONS **********

        public bool AddMFEntry(MFEntry mf)
        {
            bool resFinal = false;
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", mf.basicId);
                    cmd.Parameters.AddWithValue("@updatedby", mf.updatedBy);
                    cmd.Parameters.AddWithValue("@transType", mf.transType);
                    cmd.Parameters.AddWithValue("@curn_val", mf.currentValue);
                    cmd.Parameters.AddWithValue("@amount", mf.amount);
                    cmd.Parameters.AddWithValue("@sipMdate", mf.sipMdate);
                    cmd.Parameters.AddWithValue("@assetname", mf.AssetName);
                    cmd.Parameters.AddWithValue("@startdate", mf.sipStartDate);
                    cmd.Parameters.AddWithValue("@folioNo", mf.folioNo);
                    cmd.Parameters.AddWithValue("@SchemeID", mf.schemeId);
                    cmd.Parameters.AddWithValue("@trxnSource", mf.trxnSource);
                    cmd.Parameters.AddWithValue("@ACTION", "SAVE_EXTERNAL_MF_ENTRY");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();

                }

                if (res > 0)
                {
                    resFinal = true;

                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            return resFinal;
        }
        public bool updateMFEntryById(MFEntry mf)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", mf.basicId);
                    cmd.Parameters.AddWithValue("@ID", mf.Id);
                    cmd.Parameters.AddWithValue("@transType", mf.transType);
                    cmd.Parameters.AddWithValue("@SchemeID", mf.schemeId);
                    cmd.Parameters.AddWithValue("@sipMdate", mf.sipMdate);
                    cmd.Parameters.AddWithValue("@curn_val", mf.currentValue);
                    cmd.Parameters.AddWithValue("@startdate", mf.sipStartDate);
                    cmd.Parameters.AddWithValue("@assetname", mf.AssetName);
                    cmd.Parameters.AddWithValue("@amount", mf.amount);
                    cmd.Parameters.AddWithValue("@trxnSource", mf.trxnSource);
                    cmd.Parameters.AddWithValue("@ACTION", "UPDATE_EXTERNAL_MF_ENTRY_BY_ID_BASICID");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }
        public DataTable getAllMFByBasicId(string basicId)
        {

            DataTable dtInsurance = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_ALL_EXTERNAL_MF_ENTRY_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtInsurance);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtInsurance;

        }
        public DataTable getMFEntryById(string mdId, string basicid)
        {
            DataTable dtInsurEntry = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", mdId);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_EXTERNAL_MF_ENTRY_BY_ID_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtInsurEntry);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtInsurEntry;

        }
        public bool removeMFEntryById(string Id, string basicid)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Id", Id);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "REMOVE_EXTERNAL_MF_ENTRY_BY_ID_BASICID");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }

        // ********** HELPER FOR ASSET MAPPING CALC ************
        public DataTable getGoalDetailsByGoalID(string usergoalID)
        {

            DataTable dtGoal = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@usergoalid", usergoalID);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_GOAL_DETAIL_BY_USERGOALID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtGoal);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtGoal;

        }

        public string getTotalMappedAssetFVByGoalID(string usergoalID,string basicid,string planid)
        {

            object totalFV = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@usergoalid", usergoalID);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@planid", planid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_TOTAL_ASSET_MAPPED_FUTURECOST_BY_GOALID_ID");
                    
                    Con.Open();
                    totalFV = cmd.ExecuteScalar();
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToString(totalFV);

        }
        public string getGoalNetDeficitByGoalID(string usergoalID, string basicid, string planid)
        {

            object netDeficit = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@usergoalid", usergoalID);
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@planid", planid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_GOAL_NET_DEFICIT_BY_GOALID_ID");

                    Con.Open();
                    netDeficit = cmd.ExecuteScalar();
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToString(netDeficit);

        }
        public DataTable getSchemeListByTextSearch(string text)
        {
            DataTable dtGoalShemeList = new DataTable();

            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SchemeFilter", text);
                    cmd.Parameters.AddWithValue("@ACTION", "SELECT_TOP_SCHEMES_BY_TERM");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtGoalShemeList);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtGoalShemeList;

        }

        public DataTable getAllGoalDeficitPmtById(string basicId, string planid)
        {

            DataTable dtGoalsDeficitPmt = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planid", planid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_GOALS_DEFICIT_WITH_PMT");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtGoalsDeficitPmt);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtGoalsDeficitPmt;

        }

        public DataTable getAllMappedNetCashflowWithSchemes(string basicId, string planid)
        {

            DataTable dtNestcashflowMapped = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicId);
                    cmd.Parameters.AddWithValue("@planid", planid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_MAPPED_NETCASHFLOW_SCHEME_BY_BASICID_ID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtNestcashflowMapped);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtNestcashflowMapped;

        }


        public string getFutureValueFromDB(Double rate, Double nper, Double pmt, Double pv, Double type)
        {
            object futureCost = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("SELECT round([dbo].UDF_FutureValue(@InterestRate,@Nper,@Pmt,@Pv,@Type),0)", Con))
                {
                    
                    cmd.Parameters.AddWithValue("@InterestRate", rate);
                    cmd.Parameters.AddWithValue("@Nper", nper);
                    cmd.Parameters.AddWithValue("@Pmt", pmt);
                    cmd.Parameters.AddWithValue("@Pv", pv);
                    cmd.Parameters.AddWithValue("@Type", type);
                   
                    Con.Open();
                    futureCost = cmd.ExecuteScalar();
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToString(futureCost);
        }

        public DataTable getAllFamilyMbrDetailsByBasicId(string strBasicid)
        {
            DataTable dtPolicyOwner = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_FAMILYMBR_DETAILS_BY_BASICID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtPolicyOwner);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtPolicyOwner;

        }

        public bool updateGoalPriority(string userGoalId, string priority)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID", userGoalId);
                    cmd.Parameters.AddWithValue("@goalPriority", priority);
                    cmd.Parameters.AddWithValue("@ACTION", "SET_GOAL_PRIORITY");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }

        public string getBalGoalMappedAssetInGoalById(string strBasicId,string strUsergoalId, string strPlanId)
        {
            string strTotalamt = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@planId", strPlanId);
                    cmd.Parameters.AddWithValue("@usergoalid", strUsergoalId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_TOTAL_MAPPEDASSET_BAL_IN_GOAL");
                    Con.Open();
                    strTotalamt = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return strTotalamt;
        }

        public string getBalAddSipDeficitInGoalById(string strBasicId, string strUsergoalId, string strPlanId)
        {
            string strTotalamt = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@planId", strPlanId);
                    cmd.Parameters.AddWithValue("@usergoalid", strUsergoalId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_CALC_ADD_SIP_REQUIRED_IN_GOAL");
                    Con.Open();
                    strTotalamt = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return strTotalamt;
        }

        public string getHumanLifeValueById(string strBasicId, string strPlanId)
        {
            string strTotalamt = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@planId", strPlanId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_CALC_HUMAN_LIFE_VALUE_BY_ID_BASICID");
                    Con.Open();
                    strTotalamt = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return strTotalamt;
        }

        public List<KeyValue> getExpenseNameDdlListByType(string strExpenseType)
        {
            List<KeyValue> ExpenseList = new List<KeyValue>();
            List<KeyValue> sortedList = new List<KeyValue>();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@expSubType", strExpenseType);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_EXPENSELIST_BY_EXPTYPE");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    Con.Open();
                    DA.Fill(ds);
                    Con.Close();

                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["id"])) && !string.IsNullOrWhiteSpace(Convert.ToString(dr["type_name"])))
                                    {
                                        KeyValue ObjKeyVal = new KeyValue();
                                        ObjKeyVal.key = Convert.ToString(dr["type_name"]);
                                        ObjKeyVal.value = Convert.ToString(dr["id"]);
                                        ExpenseList.Add(ObjKeyVal);
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }

            sortedList = ExpenseList.OrderBy(s => int.Parse(s.value)).ToList();
            return sortedList;
        }



        public bool insertgoalfromcallone(callonegoals goalin)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", goalin.basicid);
                    cmd.Parameters.AddWithValue("@goalname", goalin.gname);
                    cmd.Parameters.AddWithValue("@goalcode", goalin.goalcode);
                    cmd.Parameters.AddWithValue("@curn_val", goalin.goalpv);
                    cmd.Parameters.AddWithValue("@goalenddate", goalin.gachdate);
                    cmd.Parameters.AddWithValue("@age", goalin.retirementage);
                    cmd.Parameters.AddWithValue("@ACTION", "Insert_All_Goal_Details_for_calone");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }




        public bool updateplanlasthandeler(updateexixtingplan updateplan)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", updateplan.basicid);
                    cmd.Parameters.AddWithValue("@planid", updateplan.planid);
                    cmd.Parameters.AddWithValue("@updatedby", updateplan.updaterid);
                    cmd.Parameters.AddWithValue("@ACTION", "Update_existing_plan_by_updaterid");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }
        public bool updaterelation(string strbasic , string strrelationmem)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid",strbasic);
                    cmd.Parameters.AddWithValue("@Relationship_status", strrelationmem);
                    cmd.Parameters.AddWithValue("@ACTION", "Update_member_details");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }


        public bool deleteplanby_basic_id_and_plan_id(updateexixtingplan updateplan)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", updateplan.basicid);
                    cmd.Parameters.AddWithValue("@planid", updateplan.planid);
                    cmd.Parameters.AddWithValue("@updatedby", updateplan.updaterid);
                    cmd.Parameters.AddWithValue("@ACTION", "Remove_plan_by_Basicid");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }



        public bool Create_new_planby_basic_id(updateexixtingplan updateplan)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", updateplan.basicid);
                    cmd.Parameters.AddWithValue("@planname", updateplan.planname);
                    cmd.Parameters.AddWithValue("@empid", updateplan.rmid);
                    cmd.Parameters.AddWithValue("@createdby", updateplan.updaterid);
                    cmd.Parameters.AddWithValue("@updatedby", updateplan.updaterid);
                    cmd.Parameters.AddWithValue("@ACTION", "Create_new_plan_by_Basicid");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }



        public bool insertclientinfofromcallone(callonebasicinsert cltbasicinsert)
        {
            int res = 0;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Clientbasicid", cltbasicinsert.Clientbasicid);
                    cmd.Parameters.AddWithValue("@ClientDOB", cltbasicinsert.ClientDOB);
                    cmd.Parameters.AddWithValue("@ClientGender", cltbasicinsert.ClientGender);
                    cmd.Parameters.AddWithValue("@ClientMarital", cltbasicinsert.ClientMarital);
                    cmd.Parameters.AddWithValue("@ClientOccup", cltbasicinsert.ClientOccup);
                    cmd.Parameters.AddWithValue("@ClientDegns", cltbasicinsert.ClientDegns);
                    cmd.Parameters.AddWithValue("@ClientMnthin", cltbasicinsert.ClientMnthin);
                    cmd.Parameters.AddWithValue("@Clientmnthex", cltbasicinsert.Clientmnthex);
                    cmd.Parameters.AddWithValue("@Clientavlres", cltbasicinsert.Clientavlres);
                    cmd.Parameters.AddWithValue("@spousedob", cltbasicinsert.spousedob);
                    cmd.Parameters.AddWithValue("@spouseName", cltbasicinsert.spouseName);
                    cmd.Parameters.AddWithValue("@spousebasicid", cltbasicinsert.spousebasicid);
                    cmd.Parameters.AddWithValue("@spouseocc", cltbasicinsert.spouseocc);
                    cmd.Parameters.AddWithValue("@spousdeg", cltbasicinsert.spousdeg);
                    cmd.Parameters.AddWithValue("@spouseincome", cltbasicinsert.spouseincome);
                    cmd.Parameters.AddWithValue("@chldonebasicid", cltbasicinsert.chldonebasicid);
                    cmd.Parameters.AddWithValue("@chldoneName", cltbasicinsert.chldoneName);
                    cmd.Parameters.AddWithValue("@chldoneDOb", cltbasicinsert.chldoneDOb);
                    cmd.Parameters.AddWithValue("@chldtwobasicid", cltbasicinsert.chldtwobasicid);
                    cmd.Parameters.AddWithValue("@chldtwodob", cltbasicinsert.chldtwodob);
                    cmd.Parameters.AddWithValue("@chldtwoname", cltbasicinsert.chldtwoname);
                    cmd.Parameters.AddWithValue("@fatherbasicid", cltbasicinsert.fatherbasicid);
                    cmd.Parameters.AddWithValue("@fathername", cltbasicinsert.fathername);
                    cmd.Parameters.AddWithValue("@fatherdob", cltbasicinsert.fatherdob);
                    cmd.Parameters.AddWithValue("@motherbasicid", cltbasicinsert.motherbasicid);
                    cmd.Parameters.AddWithValue("@mothername", cltbasicinsert.mothername);
                    cmd.Parameters.AddWithValue("@motherdob", cltbasicinsert.motherdob);
                    cmd.Parameters.AddWithValue("@chldoneGen", cltbasicinsert.chldoneGen);
                    cmd.Parameters.AddWithValue("@chldtwoGen", cltbasicinsert.chldtwoGen);
                    cmd.Parameters.AddWithValue("@ACTION", "Insert_call_one_data");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (res > 0)
                {
                    res = 1;
                }
                else { res = 0; }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return Convert.ToBoolean(res);

        }

        public string getplanIdByBasicId(string strBasicId)
        {
            string strtopplanid = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@ACTION", "get_last_plan_id_by_basic_id");
                    Con.Open();
                    strtopplanid = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return strtopplanid;
        }
        public DataTable GetData(string query)
        {


            using (SqlCommand cmd = new SqlCommand(query, Con))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        return dt;
                    }
                }
            
        }

        public DataSet callonedata(string basicid)
        {
            DataSet dtCashflowIn = new DataSet();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "Get_all_details_for_callone");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowIn);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowIn;

        }

        public DataTable getAllExpenseMFSipById(string strBasicid,string strPlanid)
        {
            DataTable dtExpenseMFSip = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicid);
                    cmd.Parameters.AddWithValue("@planid", strPlanid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_CASHOUT_MFSIP_LIST");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtExpenseMFSip);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtExpenseMFSip;

        }

        public bool checkExpenseMFSipAmount(string strBasicId, string stPlanid, string strAmount)
        {
            object res;
            bool boolRes = false;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@planid", stPlanid);
                    cmd.Parameters.AddWithValue("@amount", strAmount);
                    cmd.Parameters.AddWithValue("@ACTION", "CHECK_MFSIP_AMOUNT_EXIST_IN_CASHOUT_BY_ID");
                    Con.Open();
                    res = cmd.ExecuteScalar();
                    Con.Close();
                }
                if (int.Parse(res.ToString()) > 0)
                {
                    boolRes = true;
                }
                
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return boolRes;

        }

        public string getClientNameByBasicId(string strBasicId)
        {
            string strName = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_CLIENTNAME_BY_BASIC_ID");
                    Con.Open();
                    strName = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return strName;
        }

        public DataTable getAllCashFlowOutByBasicId(string basicid, string planId)
        {

            DataTable dtCashflowOut = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicid);
                    cmd.Parameters.AddWithValue("@planId", planId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_ALL_CASHOUT_DETAILS_BY_BASIC_ID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtCashflowOut);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtCashflowOut;

        }

        public bool toggleCashOutIsContinue(string strBasicId, string stPlanid,string strCashOutId ,bool isContinue)
        {
            object res;
            bool boolRes = false;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@planid", stPlanid);
                    cmd.Parameters.AddWithValue("@Id", strCashOutId);
                    cmd.Parameters.AddWithValue("@isContinue", isContinue);
                    cmd.Parameters.AddWithValue("@ACTION", "TOGGLE_ISCONTINUE_CASHOUT_BY_BASIC_ID");
                    Con.Open();
                    res = cmd.ExecuteScalar();
                    Con.Close();
                }
                if (int.Parse(res.ToString()) > 0)
                {
                    boolRes = true;
                }

            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return boolRes;

        }

        public DataTable CheckGetLockInAssetFutureValue(string strBasicId,string strAssetId,string strAssetPV)
        {
            DataTable dtFinalAmtDuration = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@Id", strAssetId);
                    cmd.Parameters.AddWithValue("@amount", strAssetPV);
                    cmd.Parameters.AddWithValue("@ACTION", "CHECK_GET_LOCKED_IN_ASSET_CALC_BY_ASSETID_ID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtFinalAmtDuration);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtFinalAmtDuration;
        }

        public bool updateCurrentExpenseAmountByID(string strBasicId, string stPlanid, string strCashOutId,string strAmount)
        {
            object res;
            bool boolRes = false;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@planid", stPlanid);
                    cmd.Parameters.AddWithValue("@Id", strCashOutId);
                    cmd.Parameters.AddWithValue("@amount", strAmount);
                    cmd.Parameters.AddWithValue("@ACTION", "UPDATE_CASHOUT_CURRAMOUNT_BY_BASICID_ID");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (int.Parse(res.ToString()) > 0)
                {
                    boolRes = true;
                }

            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return boolRes;

        }

        public string getCafCurrentHLVById(string strBasicId)
        {
            string strTotalamt = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_CURRENT_HLV_FROM_CAFBASIC");
                    Con.Open();
                    strTotalamt = Convert.ToString(cmd.ExecuteScalar());
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return strTotalamt;
        }

        public bool updateCafCurrentHLVByID(string strBasicId, string strAmount)
        {
            object res;
            bool boolRes = false;
            try
            {
                using (SqlCommand cmd = new SqlCommand("DATA_GATHERING", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@BasicID", strBasicId);
                    cmd.Parameters.AddWithValue("@CafCurrHLV", strAmount);
                    cmd.Parameters.AddWithValue("@ACTION", "UPDATE_CURRENT_HLV_CAFBASIC_BY_BASICID_ID");
                    Con.Open();
                    res = cmd.ExecuteNonQuery();
                    Con.Close();
                }
                if (int.Parse(res.ToString()) > 0)
                {
                    boolRes = true;
                }

            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return boolRes;

        }

        public DataTable getCurrentPlanMasterDtlsByBasicId(string basicid)
        {

            DataTable dtPlanDetails = new DataTable();
            try
            {
                using (SqlCommand cmd = new SqlCommand("[DATA_GATHERING]", Con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@basicid", basicid);
                    cmd.Parameters.AddWithValue("@ACTION", "GET_CURRENT_PLAN_DETAILS_BY_BASICID_ID");
                    SqlDataAdapter DA = new SqlDataAdapter(cmd);
                    Con.Open();
                    DA.Fill(dtPlanDetails);
                    Con.Close();
                }
            }
            catch (Exception ex)
            {
                Con.Close();
            }
            finally
            {
                //Con.Dispose();
            }
            return dtPlanDetails;

        }
    }

}