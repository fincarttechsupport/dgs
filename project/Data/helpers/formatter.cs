﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;

    public static class formatter
    {

        public static string IndianCurrency(string strNumber)
        {
            string strFormattedRes = string.Empty;
            if (!string.IsNullOrWhiteSpace(strNumber))
            {
                decimal parsed = decimal.Parse(strNumber, CultureInfo.InvariantCulture);
                CultureInfo hindi = new CultureInfo("hi-IN");
                string text = string.Format(hindi, "{0:c}", parsed);
                strFormattedRes = text.Replace(".00", "");
            }
            return strFormattedRes;
        }

        public static string removeNumberFormating(string strNumber)
        {

            string strFormattedRes = string.Empty;
            if (!string.IsNullOrWhiteSpace(strNumber))
            {
                strFormattedRes = strNumber.Replace("₹", "").Replace(",", "").Replace(".00", "");
            }
            return strFormattedRes;
        }

        public static string enUsDateFormat(string strDate)
        {
            string strFormattedRes = string.Empty;
            if (!string.IsNullOrWhiteSpace(strDate))
            {   
                IFormatProvider culture = new CultureInfo("en-US", true);
                DateTime dtHolder = Convert.ToDateTime(strDate, culture);
                strFormattedRes = dtHolder.ToString("yyyy-MM-dd");
            }
            return strFormattedRes;
        }
}
