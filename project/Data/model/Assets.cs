﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class Assets
    {
        public string amount { get; set; }
        public string assetId { get; set; }
        public string assetType { get; set; }
        public string basicId { get; set; }
        public string id { get; set; }
        public string planId { get; set; }
        public string futureCost { get; set; }
        public string ror { get; set; }
        public string pmt { get; set; }
        public string isMfPool { get; set; }
        public string payPeriod { get; set; }
        public string deficit { get; set; }
        public string usergoalId { get; set; }
        public string createdBy { get; set; }
        public string updatedBy { get; set; }
    }
}