﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class CashflowOut
    {
        public string amount { get; set; }
        public string expenseType { get; set; }
        public string OutflowType { get; set; }
        public string OutflowOthTypeName { get; set; }
        public string assetId { get; set; }
        public string assetType { get; set; }
        public string basicId { get; set; }
        public string id { get; set; }
        public string planId { get; set; }
        public string createdBy { get; set; }
        public string updatedBy { get; set; }

        
    }
}