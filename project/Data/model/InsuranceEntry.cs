﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class InsuranceEntry
    {
        public string Id { get; set; }
        public string basicId { get; set; }
        public string planId { get; set; }
        public string AssetName { get; set; }
        public string AssetTypeId { get; set; }
        public string policyPartnerId { get; set; }
        public string policyNo { get; set; }
        public string policySumAssured { get; set; }
        public string policyPremium { get; set; }
        public string policyIssueDt { get; set; }
        public string policyTerm { get; set; }
        public string policyOwnerBasicId { get; set; }
        public string currentValue { get; set; }
        public int isActivePolicy { get; set; }
        public string updatedBy { get; set; }
        public string trxnSource { get; set; }
        public string lockInDuration { get; set; }
        public string lockInInterest { get; set; }
        public string finalHlv { get; set; }
    }
}