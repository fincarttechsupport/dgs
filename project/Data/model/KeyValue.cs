﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class KeyValue
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}