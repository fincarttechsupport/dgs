﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class LoanEntry
    {
        public string Id { get; set; }
        public string basicId { get; set; }
        public string planId { get; set; }
        public string AssetTypeId { get; set; }
        public string AssetName { get; set; }
        public string amount { get; set; }
        public string interest { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string emi { get; set; }
        public string LiquidityType { get; set; }
        public string currentValue { get; set; }
        public string updatedBy { get; set; }
        public string lockInDuration { get; set; }
        public string lockInInterest { get; set; }
        public string trxnSource { get; set; }

    }
}