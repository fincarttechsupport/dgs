﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class MFEntry
    {
        public string Id { get; set; }
        public string basicId { get; set; }
        public string planId { get; set; }
        public string AssetName { get; set; }
        public string AssetTypeId { get; set; }
        public string transType { get; set; }
        public string schemeId { get; set; }
        public string category { get; set; }
        public string sipMdate { get; set; }
        public string sipStartDate { get; set; }
        public string folioNo { get; set; }
        public string amount { get; set; }
        public string currentValue { get; set; }
        public string updatedBy { get; set; }
        public string trxnSource { get; set; }
    }
}