﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class Schemes
    {
        public string id { get; set; }
        public string schemeName { get; set; }
        public string fundId { get; set; }
        public string objective { get; set; }
        public string SubObjective { get; set; }
        public string OrgSchemeName { get; set; }
    }
}