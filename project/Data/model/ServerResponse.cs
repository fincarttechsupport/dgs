﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class ServerResponse
    {
        public string status { get; set; }
        public string errorCode { get; set; }
        public string msg { get; set; }
        public object data { get; set; }
    }
