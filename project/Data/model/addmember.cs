﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class addmember
    {
        public string useridgroupid { get; set; }

        public string clientname { get; set; }

        public string mobileNumber { get; set; }

	    public string PANNUMBER { get; set; }

		public string dob { get; set; }

		public string email { get; set; }

        public string userid { get; set; }

        public string gender { get; set; }

        public string compname { get; set; }

        public string designation { get; set; }

        public string Relationshipstatus { get; set; }

        public string maritalstatus { get; set; }

        public string isinvesting { get; set; }

        public string diseaseStatus { get; set; }
        public string cropsaddres { get; set; }
        
        public string health { get; set; }
        public string Relation { get; set; }
        public string basicId { get; set; }



    }
}