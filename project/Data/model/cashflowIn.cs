﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class cashflowIn
    {
        public string amount {get;set;}
        public string inflowType { get; set; }
        public string inflowTypeName { get; set; }
        public string frequency { get; set; }
        public string basicId { get; set; }
        public string id { get; set; }
        public string planId { get; set; }
        public string createdBy { get; set; }
        public string updatedBy { get; set; }

    }
}