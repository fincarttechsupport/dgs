﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class fincalresmodel
    {
        public string status { get; set; }
        public string errorCode { get; set; }
        public string msg { get; set; }
        public string data { get; set; }
            public int time { get; set; }
            public int getLumpsum { get; set; }
            public int getSip { get; set; }
            public int investLumpsum { get; set; }
            public int investSip { get; set; }
            public int PMT { get; set; }
            public int ROR { get; set; }
            public int RORL { get; set; }
            public int Inflation { get; set; }
            public string goalName { get; set; }

        
    }
}