﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace data.model
{
    public class fundAllocate
    {
        public string Id { get; set; }
        public string basicId { get; set; }
        public string schemeId { get; set; }
        public string planId { get; set; }
        public string usergoalId { get; set; }
        public float amount { get; set; }
        public float futurecost { get; set; }
        public string mfTrxnType { get; set; }
        public string createdBy { get; set; }
       
    }
}