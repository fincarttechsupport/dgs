﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinFinancialPlan
{
    public class gaolcreatemodel
    {

    public string usergoalId { get; set; }
     public string  basicid { get; set; }
     public string  Relation { get; set; }
    public string   childName { get; set; }
      public string age { get; set; }
     public string  gender { get; set; }
      public string annualIncome { get; set; }
     public string  goalCode { get; set; }
     public string  otherGoalName { get; set; }
     public string  typeCode { get; set; }
     public string  monthlyAmount { get; set; }
     public string  presentValue { get; set; }
      public string risk { get; set; }
     public string  goal_StartDate { get; set; }
     public string  goal_EndDate { get; set; }
     public string  inflationRate { get; set; }
     public string  ror { get; set; }
     public string  PMT { get; set; }
     public string  downPaymentRate { get; set; }
     public string  trnx_Type { get; set; }
     public string  getAmount { get; set; }
      public string retirementAge { get; set; }
      public string investAmount { get; set; }

      public string  people  { get; set; }
    }
}