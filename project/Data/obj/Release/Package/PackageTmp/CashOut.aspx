﻿<%@ Page Language="C#" AutoEventWireup="true"   CodeBehind="CashOut.aspx.cs" Inherits="FinFinancialPlan.CashOut" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
   <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"/>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>



    <link href="css/tabs.css" rel="stylesheet" />
    <script src="js/tabCustom.js"></script>
    <script src="js/validation.js"></script>
    <link href="css/repeaterstype.css" rel="stylesheet" />
    <script src="js/session.js"></script>
</head>
<body>

    <div class="container">
    <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-1" type="button" class="btn btn-default btn-circle" disabled="disabled">1</a>
                    <p><small>Basic Details</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p><small>Income</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-3" type="button" class="btn btn-default btn-success btn-circle" >3</a>
                    <p><small>Expense</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p><small>NetAssets Entry</small></p>
                </div>
                 <div class="stepwizard-step col-xs-2">
                    <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                    <p><small>Goals Summary</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                    <p><small>NetAssets Allocation</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-7" type="button" class="btn btn-default btn-circle" disabled="disabled">7</a>
                    <p><small>NetCashFlow Allocation</small></p>
                </div>
            </div>
        </div>




 <form id="form1" runat="server" role="form">
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

         <div class="panel panel-primary setup-content" id="step-3">
             <div class="panel-heading"><h3 class="panel-title">CashOut Flow</h3></div>
              
             <div class="panel-body">
                 <div class="col-lg-12">
                      <div class="col-lg-4">
                          
                          <div class="form-group">
                            <label class="control-label">Expense Type</label>
                              <asp:DropDownList runat="server" ID="ddlExpenseType" AutoPostBack="true" OnSelectedIndexChanged="ddlExpenseType_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                          </div>
                          
                          <div class="form-group">
                            <label class="control-label">Amount</label>
                              <asp:TextBox runat="server" CssClass="form-control" ID="txtAmount"  MaxLength="10"></asp:TextBox>   
                          </div>

                          <div class="form-group">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                   <label class="control-label">Which Expense is it? </label>
                                   <asp:DropDownList runat="server" ID="ddlExpTypName" OnSelectedIndexChanged="ddlExpTypName_SelectedIndexChanged" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>


                                    <asp:Panel ID="pnlOtherOutflow" runat="server" >
                                     <label class="control-label">Name to Identify(Expense)</label>
                                     <asp:TextBox runat="server" ID="txtExpTypOthName" CssClass="form-control"></asp:TextBox> 

                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                          </div>
                          
                          
                          <asp:Button Text="Save" OnClick="btnSave_Click" runat="server" id ="btnSave" />
                          <asp:Button Text="Update" OnClick="btnUpdate_Click" runat="server" id ="btnUpdate" Visible="false" />
                          <asp:Label ID="lblMsg" Visible="false" Font-Size="Small" runat="server"></asp:Label>

                      </div>
                     
                     
                     <div>
                         <div id="rightDv" >
                            <div >
                                <div id="invoice" class="col-lg-8">
                                    <div style="overflow-x:scroll">
                                    <asp:Repeater ID="rptrCashFlowOut" runat="server" >
                                        <HeaderTemplate>
                                           <table id="ulRptOverflowx">
                                                <tr>
                                                    
                                                        <th>Expense</th>
                                                        <th>Type</th>
                                                        <th>Outflow</th>
                                                        <th>Outflow Name</th>
                                                        <th>Delete</th>
                                                        <th>Edit</th>
                                                    
                                                </tr>
                                                
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                 
                                                     <td style="text-align:right;"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.expense")))%></td>
                                                     <td><%#DataBinder.Eval(Container,"DataItem.expence_type_name")%></td>
                                                     <td><%#DataBinder.Eval(Container,"DataItem.out_flow_type_name")%></td>
                                                     <td><%#DataBinder.Eval(Container,"DataItem.outflow_type_name")%></td>
                                
                                                     <td><asp:LinkButton ID="lnkDelete" Text="Delete" runat="server" OnClick="lnkDelete_Click" OnClientClick="return confirm('Do you want to delete this row?');"  /></td>
                                                     <td><asp:LinkButton ID="lnkUpdate" Text="Edit" runat="server" OnClick="lnkUpdate_Click" /></td>
                                                     <asp:Label ID="lblId" runat="server" Text='<%# Eval("id") %>' Visible = "false" />
                                                </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                           </table>
                                        </FooterTemplate>    
                    
                                    </asp:Repeater>
                                        </div>
                                </div>
                            </div>
        
                         </div>
                     </div>
                 </div>
                 
           

                  <div class="col-lg-12">
                     <asp:Button Text="Next >>" OnClick="btnNext_Click" runat="server" id ="btnNext" class="btn btn-primary nextBtn pull-right" type="button" />
                      <asp:Button Text="<< Back" runat="server" id ="prevToCashIn" OnClick="prevToCashIn_Click" class="btn btn-primary nextBtn pull-right"  type="button" />
                </div>
              </div>
        </div>
</form>
</div>



    

</body>
</html>

