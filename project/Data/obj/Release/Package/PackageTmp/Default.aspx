﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FinFinancialPlan.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link href="css/tabs.css" rel="stylesheet" />
    <link href="css/repeaterstype.css" rel="stylesheet" />
    <link href="css/timeout-dialog.css" rel="stylesheet" />
    <script src="js/timeout-dialog.js"></script>
    <script src="js/tabCustom.js"></script>
    <script src="js/session.js"></script>


</head>
<body>
   
     
   



    <div class="container">
    <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-1" type="button" class="btn btn-default btn-success btn-circle" >1</a>
                    <p><small>Basic Details</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p><small>Income</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p><small>Expense</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p><small>NetAssets Entry</small></p>
                </div>
                 <div class="stepwizard-step col-xs-2">
                    <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                    <p><small>Goals Summary</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                    <p><small>NetAssets Allocation</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-7" type="button" class="btn btn-default btn-circle" disabled="disabled">7</a>
                    <p><small>NetCashFlow Allocation</small></p>
                </div>
            </div>
        </div>

    
     <form id="form1" runat="server" role="form">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
 </asp:ScriptManager>
        <div class="panel panel-primary setup-content" id="step-1">
            <div class="panel-heading">
                 <h3 class="panel-title">Basic Details</h3>
            </div>
            <div class="panel-body">

              <asp:Repeater ID ="member" runat="server">
<HeaderTemplate>
                        <table style="width: 100%;">
                            <tr>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Relation</th>
                                <th>Date Of Birth</th>
                                <th>Pan Number</th>
                                <th>Designation</th>
                                <th>Company Name</th>
                             
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
            
                          <tr>
                              
                              <td><%#DataBinder.Eval(Container,"DataItem.Name")%></td>
                              <td><%#DataBinder.Eval(Container,"DataItem.Gender")%></td>
                              <td><%#DataBinder.Eval(Container,"DataItem.Relation")%></td>
                              <td><%#DataBinder.Eval(Container,"DataItem.Date Of Birth","{0:dd/MM/yyyy}")%></td>
                              <td><%#DataBinder.Eval(Container,"DataItem.PANNUMBER")%></td>                           
                              <td><%#DataBinder.Eval(Container,"DataItem.Designation")%></td>
                              <td><%#DataBinder.Eval(Container,"DataItem.Company Name")%></td>
                             

                          </tr>

                    </ItemTemplate>


             
                    <FooterTemplate>   </table>
                    </FooterTemplate>              </asp:Repeater>
                <div class="col-lg-12">
                      <asp:Button Text="Next >>" runat="server" id ="Nexttocashin" OnClick="Nexttocashin_Click" class="btn btn-primary nextBtn pull-right" type="button"/>
                </div>
               

               
               
            </div>
        </div>
        
       
    </form>
</div>


   
</body>
</html>
