﻿<%@ Page Language="C#" AutoEventWireup="true"   CodeBehind="Goalalocation.aspx.cs" Inherits="FinFinancialPlan.Goalalocation"  %>
 
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>
      <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.8.13/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />

    <link href="css/timeout-dialog.css" rel="stylesheet" />
    <script src="js/timeout-dialog.js"></script>
    <link href="css/tabs.css" rel="stylesheet" />
    <link href="css/repeaterstype.css" rel="stylesheet" />
     <script src="js/tabCustom.js"></script>
     <script src="js/session.js"></script>
         <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
     <script src="js/validation.js"></script>

 <script type="text/javascript">
     
     $(function () {
         SetAutoComplete();
     });

    
     
     function SetAutoComplete() {
         $("#<%=txtSchemeName.ClientID %>").autocomplete({
             source: function (request, response) {
                 $.ajax({
                     url: 'Goalalocation.aspx/getSchemes',
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     data: "{'text':'" + document.getElementById("<%=txtSchemeName.ClientID %>").value + "'}",
                     success: function (data) {
                         debugger;
                         if (!data.d) {
                             var result = [
                                 {
                                     label: 'No matches found',
                                     value: response.term
                                 }
                             ];
                             $("#<%=hdnSchemeid.ClientID %>").val("");
                             response(result);
                         }
                         else {
                             
                             var dtParsed = $.parseJSON(data.d);
                             response($.map(dtParsed, function (item) {
                                 var itm = item;
                                
                                 return {
                                     label: item.schemeName,
                                     value: item.schemeName,
                                     id: item.id
                                 };
                             }));
                         }
                     },

                 })
             },
             select: function (event, ui) {
                 debugger;
                 $("#<%=hdnSchemeid.ClientID %>").val(ui.item.id);

             }
         });
     }
 </script>
</head>
<body>
  

 <div class="container">
    <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-1" type="button" class="btn btn-default btn-circle" disabled="disabled">1</a>
                    <p><small>Basic Details</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p><small>Income</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p><small>Expense</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p><small>NetAssets Entry</small></p>
                </div>
                 <div class="stepwizard-step col-xs-2">
                    <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                    <p><small>Goals Summary</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-6" type="button" class="btn btn-default btn-circle" disabled="disabled">6</a>
                    <p><small>NetAssets Allocation</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-7" type="button" class="btn btn-default btn-success btn-circle">7</a>
                    <p><small>NetCashFlow Allocation</small></p>
                </div>
            </div>
        </div>

      <form id="form1" runat="server" role="form">
          <input type="hidden" id="hdnSchemeid" runat="server" value="" />
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

         <div class="panel panel-primary setup-content" id="step-7">
             <div class="panel-heading"><h3 class="panel-title">NetCashFlow Allocation & Scheme Selection</h3></div>
              
             <div class="panel-body">
                 <div class="col-lg-12"><asp:Label ID="lblMsg" Visible="false" Font-Size="Small" runat="server"></asp:Label></div>

                 <div class="col-lg-12">
                     
                    <div class="col-lg-4">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                            <ContentTemplate>
                             <div class="form-group">
                                    <label class="control-label">Goal Name</label>
                                  <asp:DropDownList runat="server" ID="ddlGoalName" AutoPostBack="true" OnSelectedIndexChanged="ddlGoalName_SelectedIndexChanged" CssClass="form-control">
                                  </asp:DropDownList>  
                             </div>
                            
                             <div class="form-group">
                                  <label class="control-label">Transaction Type</label>
                                  <asp:DropDownList runat="server" ID="ddlTrxnType" OnSelectedIndexChanged="ddlTrxnType_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                  <asp:ListItem Value="0" Selected="True">-Select-</asp:ListItem>
                                  <asp:ListItem Value="S">SIP</asp:ListItem>
                                  <asp:ListItem Value="L">Lumpsum</asp:ListItem>
                                  </asp:DropDownList>  
                             </div>
                             <div class="form-group">
                                    <label class="control-label">Scheme Selection</label>
                                    <asp:TextBox ID="txtSchemeName"  runat="server" CssClass="form-control"></asp:TextBox>
                             </div>

                              <div class="form-group">
                                    <label class="control-label" runat="server" id="lblAllotedSurpAmt">Allocate Amount</label>
                                   <asp:TextBox ID="txtAllotedSurpAmt" MaxLength="10"  runat="server" CssClass="form-control"></asp:TextBox>
                             </div>

                             <div class="form-group" runat="server" id="divSurpWas" visible="false">
                                    <label class="control-label">Surplus Was</label>
                                 <asp:Label id="lblConstSurplus" Text="0" runat="server" CssClass="labelBlue"></asp:Label>
                             </div>
                             <div class="form-group" runat="server" id="divSurpLeft" visible="false">
                                    <label class="control-label">Surplus Left</label>
                                    <asp:Label id="lblSurplusLeft" Text="0" runat="server" CssClass="labelBlue"></asp:Label>
                             </div>
                            <div class="form-group" runat="server" id="divBalMarkedAsset"  visible="false">
                                    <label class="control-label">Balance in (Marked Assets in Goal)</label>
                                    <asp:Label id="lblBalMarketAsset" Text="0" runat="server" CssClass="labelBlue"></asp:Label>
                             </div>
                             <div class="form-group" runat="server" id="divBalAddSip" visible="false">
                                    <label class="control-label">Balance in (Additional Sip)</label>
                                    <asp:Label id="lblBalAddSip" Text="0" runat="server" CssClass="labelBlue"></asp:Label>
                             </div>
                            </ContentTemplate>
                      </asp:UpdatePanel>
                     <asp:Button Text="Save" OnClick="btnSave_Click" runat="server" id ="btnSave" />
                     <asp:Button Text="Update" OnClick="btnUpdate_Click" runat="server" id ="btnUpdate" Visible="false" />
                    </div>


                     <div>
                          <%--<%---------------RIGHT DIV------------------%>
                                <div class="col-lg-6" style="width:60%">
                                   

                                        <h4>Expense to Change</h4>
                                     <div style="overflow-x:scroll" >
                                        <asp:Repeater ID="rptrCashflowChange" runat="server" >
                            <HeaderTemplate>
                                <table id="ulRptOverflowx">
                                    <tr>
                                       
                                            <th>Expense Name</th>
                                            <th>Amount</th>
                                            <th>Type</th>
                                            <th>Outflow</th>
                                           <th>Status</th>
                                        <th>Edit</th>
                                            
                                        
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    
                                         <td ><%#DataBinder.Eval(Container,"DataItem.outflow_type_name")%></td>
                                         <td style="text-align:right" >
                                             <asp:Label ID="lblExpenseAmt" runat="server" Text='<%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.Expense"))) %>'></asp:Label>
                                             <asp:TextBox ID="txtExpenseToEdit" MaxLength="10" runat="server" Width="80" Text='<%# Eval("Expense") %>' Visible="false" />
                                         </td>
                                         <td ><%#DataBinder.Eval(Container,"DataItem.subType")%></td>
                                         <td><%#DataBinder.Eval(Container,"DataItem.type_name")%></td>
                                        <td >
                                            <asp:LinkButton ID="lnkbtnContinue" CssClass="redlnk" runat="server" OnClick="lnkbtnContinue_Click" Visible='<%# !Convert.ToBoolean(DataBinder.Eval(Container,"DataItem.isContinue"))%>'>Continue</asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnDiscontinue" CssClass="greenlnk" runat="server" OnClick="lnkbtnDiscontinue_Click" Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container,"DataItem.isContinue"))%>'>Discontinue</asp:LinkButton>

                                        </td>
                                    <td>
                                        <asp:LinkButton ID="lblbtnEditExpense" runat="server" OnClick="lblbtnEditExpense_Click" >Edit</asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnUpdateExpense" Text="Update" runat="server" Visible="false" OnClick="lnkbtnUpdateExpense_Click" />
                                    </td>
                                        <asp:Label ID="lblCashOutExpId" runat="server" Text='<%#Eval("id")%>' Visible = "false" />
                                     
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                               </table>
                            </FooterTemplate>    
                    
                        </asp:Repeater>
                                         </div>

                                        <h4>Goals Deficit & Required Sip,Lumpsum</h4>
                                     <div style="overflow-x:scroll" >   
                                    <asp:Repeater ID="rptrGoalsDeficitPmt" runat="server" >
                            <HeaderTemplate>
                                <table id="ulRptOverflowx">
                                    <tr>
                                        
                                            <th>Goal</th>
                                            <th>Duration</th>
                                            <th>FutureCost</th>
                                            <th>NetDeficit</th>
                                            <th>Additional SIP</th>
                                            <th>SIP Tenure</th>
                                            <th>Lumpsum (Marked)</th>
                                            
                                       
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                     
                                         <td><%#DataBinder.Eval(Container,"DataItem.goalname")%></td>
                                         <td><%#DataBinder.Eval(Container,"DataItem.duration")%></td>
                                         <td style="text-align:right"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.futurecost")))%></td>
                                         <td style="text-align:right"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.netDeficit")))%></td>
                                         <td style="text-align:right"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.addSip")))%></td>
                                         <td style="text-align:right"><%# Convert.ToString(DataBinder.Eval(Container,"DataItem.siptenure"))%></td>
                                         <td style="text-align:right"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.LumpsumMF")))%></td>
                                       
                                     
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                              </table>
                            </FooterTemplate>    
                    
                        </asp:Repeater>
</div>
                                        <h4>Selected Schemes</h4>
                                     <div style="overflow-x:scroll" >
                                        <asp:Repeater ID="rptrAllocatedNetCashflow" runat="server" >
                              <HeaderTemplate>
                                <table id="ulRptOverflowx">
                                    <tr>
                                        
                                            <th >Scheme</th>
                                            <th >Objective</th>
                                            <th >Type</th>
                                            <th >Amount</th>
                                            <th>Goal</th>
                                            <th>Delete</th>
                                           <th>Edit</th>
                                            
                                            
                                        
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    
                                         <td ><%# DataBinder.Eval(Container,"DataItem.SchemeName")%></td>
                                         <td><%# DataBinder.Eval(Container,"DataItem.objective")%></td>
                                         <td ><%# DataBinder.Eval(Container,"DataItem.txnTypeName")%></td>
                                         <td style="text-align:right"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.txnAmount")))%></td>
                                         <td ><%# DataBinder.Eval(Container,"DataItem.goalname")%></td>
                                         <td><asp:LinkButton ID="lnkNetCashflowDelete" Text="Delete" runat="server" OnClick="lnkNetCashflowDelete_Click" OnClientClick="return confirm('Do you want to delete this row?');"  /></td>
                                         <asp:Label ID="lblNetCashFlowId" runat="server" Text='<%#Eval("id")%>' Visible = "false" />
                                           <td><asp:LinkButton ID="lnkFndAllocEdit" Text="Edit" runat="server" OnClick="lnkFndAllocEdit_Click" /></td>
                                    
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                           </table>
                            </FooterTemplate>    
                    
                        </asp:Repeater>
                                    </div>
                                </div>
                     </div>
                 </div>
                 

                  <div class="col-lg-12">
                    &nbsp;
                </div>

                  <div class="col-lg-12">
                     <asp:Button Text="Generate Plan" OnClick="btnPlanGenerate_Click" runat="server" id ="btnPlanGenerate" class="btn btn-primary nextBtn pull-right" type="button" />
                         <asp:Button Text="<< Back" runat="server" id ="prevToAvailRes" OnClick="prevToAvailRes_Click" class="btn btn-primary nextBtn pull-right"  type="button" />
                  </div>
              </div>
         </div>
    </form>
</div>
      
    <script type="text/javascript">
        //On UpdatePanel Refresh.
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    SetAutoComplete();
                }
            });
        };
    </script>
</body>
</html>