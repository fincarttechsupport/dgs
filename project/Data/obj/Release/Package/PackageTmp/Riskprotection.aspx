﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Riskprotection.aspx.cs" Inherits="FinFinancialPlan.Riskprotection" MasterPageFile="~/Planmasterpage.Master" %>

<asp:Content ID="cover" ContentPlaceHolderID="masterpagecontent" runat="server">

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>
        <title>Fincart</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/" />

        <!-- Bootstrap core CSS -->
        <link href="css/planpage2.css" rel="stylesheet" />
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- Custom styles for this template -->
        <link href="css/pagedashboard.css" rel="stylesheet" />
        		
						      <script src="js/session.js"></script>
        <style>
            .background {
                background-image: url('imagess/Backforplan.png');
                background-repeat: no-repeat;
                /* Full height */
                height: 100%;
                /* Center and scale the image nicely */
                background-repeat: no-repeat;
                background-size: cover;
            }

            .heading {
                text-align: center;
                color: black;
                border-bottom: medium;
                border-color: black;
            }

            .heading2 {
                text-align: center;
                color: black;
                border: 10px;
            }




            .img {
                margin-left: 350px;
                border: 0px;
                height: 620px;
            }


            table {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border: 1px solid #ddd;
                width: 100%;
            }

                table tr td {
                    border: 1px solid #ddd;
                    padding: 0px;
                    width: 10px;
                    text-align: left;
                    color: black;
                }
                   table  td {
                    border: 1px solid #ddd;
                    padding: 0px;
                    width: 10px;
                    text-align: left;
                    color: black;
                }

            tr th {
                border: 1px solid #ddd;
                padding: 0px;
                width: 10%;
                background-color: #4CAF50;
                color: white;
                text-align: center;
            }

            table tr th {
                border: 1px solid #ddd;
                padding: 0px;
                width: 10%;
                background-color: #4CAF50;
                color: white;
                text-align: center;
            }
        </style>
   </head>
   <body>
      
         <div>
            <h1 class="heading2">Risk Protection</h1>
        
          <table >
               <tr>
                  <td>
                     <h1 class="heading" >What You Have</h1>
                  </td>
               </tr>
                  <tr>
                  <td>
                     <h3 class="heading" >Health Insurance</h3>
                  </td>
               </tr>
            </table>
                   
            <asp:Repeater ID="Aldheth" runat="server" >
               <HeaderTemplate>
               <table>
                           <tr>
                       <th>
                           Policy Name
                       </th>
                       <th>
                           Insurared Person
                       </th>
                       <th>
                           Cover
                       </th>

                       <th>
                           Premium Amount
                       </th>
                   </tr>
                


               </HeaderTemplate>
               <ItemTemplate>
             
                     <tr>
                        <td><%#DataBinder.Eval(Container,"DataItem.AssetName")%></td>
                         <td style="text-align:center;"><%#DataBinder.Eval(Container,"DataItem.clientname")%></td>
                         <td style="text-align:right;"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.currValuation")))%></td>
                         <td style="text-align:right; "><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.PremiumAmount")))%></td>
                     </tr>
                 
               </ItemTemplate>
               <FooterTemplate>
                  </table>
               </FooterTemplate>
            </asp:Repeater>
                <table >
             
                  <tr>
                  <td>
                     <h3 class="heading" >Life Insurance</h3>
                  </td>
               </tr>
            </table>
                   
            <asp:Repeater ID="Aldlife" runat="server" >
               <HeaderTemplate>
                           <tr>
                    <table> 
                           <tr>
                       <th>
                           Policy Name
                       </th>
                       <th>
                           Insurared Person
                       </th>
                       <th>
                           Cover
                       </th>

                       <th>
                           Premium Amount
                       </th>
                   </tr>
                     


               </HeaderTemplate>
               <ItemTemplate>
               
                     <tr>
                        <td><%#DataBinder.Eval(Container,"DataItem.AssetName")%></td>
                         <td style="text-align:center;"><%#DataBinder.Eval(Container,"DataItem.clientname")%></td>
                         <td style="text-align:right;"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.currValuation")))%></td>
                         <td style="text-align:right;"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.PremiumAmount")))%></td>
                     </tr>
                
               </ItemTemplate>
               <FooterTemplate>
                  </table>
               </FooterTemplate>
            </asp:Repeater>




            <%--=====================RISK PROTECTION===============================--%>
          <table 
               <tr>
                  <td class="auto-style2">
                     <h1 class="heading">What We Suggest</h1>
                  </td>
               </tr>
               <tr>
    
                  <td>
                     <h3 class="heading" >Life Insurance</h3>
                  </td>
               </tr>
            </table>
            <asp:Repeater ID="suglife" runat="server" >
               <HeaderTemplate>
                 <table> 
                           <tr>
                       <th>
                           Policy Name
                       </th>
                       <th>
                           Insurared Person
                       </th>
                       <th>
                           Cover
                       </th>

                       <th>
                           Premium Amount
                       </th>
                   </tr>
                      
               </HeaderTemplate>
               <ItemTemplate>
                
                     <tr>
                       <td><%#DataBinder.Eval(Container,"DataItem.AssetName")%></td>
                         <td style="text-align:center;"><%#DataBinder.Eval(Container,"DataItem.clientname")%></td>
                         <td style="text-align:right;"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.currValuation")))%></td>
                         <td style="text-align:right;"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.PremiumAmount")))%></td>
                     </tr>
                 
               </ItemTemplate>
               <FooterTemplate>
                  </table>
               </FooterTemplate>
            </asp:Repeater>


             <table> 
            
               <tr>
                  <td>
                     <h3 class="heading" >Heath Insurance</h3>
                  </td>
               </tr>
            </table>
            <asp:Repeater ID="Sugheth" runat="server" >
               <HeaderTemplate>
  <table>
                           <tr>
                       <th>
                           Policy Name
                       </th>
                       <th>
                           Insurared Person
                       </th>
                       <th>
                           Cover
                       </th>

                       <th>
                           Premium Amount
                       </th>
                   </tr>
                    

               </HeaderTemplate>
               <ItemTemplate>
                
                     <tr>
                       <td><%#DataBinder.Eval(Container,"DataItem.AssetName")%></td>
                         <td style="text-align:center;"><%#DataBinder.Eval(Container,"DataItem.clientname")%></td>
                         <td style="text-align:right;"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.currValuation")))%></td>
                         <td style="text-align:right;"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.PremiumAmount")))%></td>
                     </tr>
                    <tr><td><asp:Label runat="server"></asp:Label></td></tr>
                 
               </ItemTemplate>
               <FooterTemplate>
                  </table>
               </FooterTemplate>
            </asp:Repeater>

             </div>
         
            
   </body>
</html>
    </asp:Content>