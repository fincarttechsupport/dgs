﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Suggested_Schemes.aspx.cs" Inherits="FinFinancialPlan.Suggested_Schemes" MasterPageFile="~/Planmasterpage.Master" %>

<asp:Content ID="cover" ContentPlaceHolderID="masterpagecontent" runat="server">

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>
        <title>Fincart</title>

        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/" />
         <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- Bootstrap core CSS -->
        <link href="css/planpage2.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="css/pagedashboard.css" rel="stylesheet" />
        <script src="js/session.js"></script>
<style>
     .background
{
    background-image:url('imagess/Backforplan.png');
    background-repeat:no-repeat;
     /* Full height */
  height: 100%;

  /* Center and scale the image nicely */

  background-repeat: no-repeat;
  background-size: cover
}

.heading
{
    text-align:center;
    color:black;


}



.img{


    margin-left:350px;
    border:0px;
    height:620px;

}

    #rptrLinkInvst {
     font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border: 1px solid #ddd;
                width: 100%;
    }

        #rptrLinkInvst tr {
        border: 1px solid #ddd;
                    padding: 0px;
                
                    text-align: left;
                    color: black;
                    text-align: center;
        }

            #rptrLinkInvst tr th {
           border: 1px solid #ddd;
                        padding: 0px;
                     
                       width:10%;
                       background-color: #4CAF50;
                        color: white;
                        text-align: center;
            }

            #rptrLinkInvst tr td {
            border: 1px solid #ddd;
                        padding: 0px;
                  
                        text-align: left;
                        color: black;
                        text-align: center;
            }

</style>
</head>
<body>





        <div>
           
        
           <asp:Repeater ID="rptrLinkInvstm" runat="server">
                <HeaderTemplate>
                    <table id="rptrLinkInvst">
                          <tr>
                            <th colspan="5">&nbsp;&nbsp;&nbsp;
                                SCHEME SELECTIONS</th>
                        </tr>
                        <tr>
                            <th>Scheme Name</th>
                            <th>Type</th>
                            <th>LumpSum</th>
                            
                            <th>Monthly SIP</th>
                            <th>Goals</th>
                            
                        </tr>
                      
                </HeaderTemplate>
                <ItemTemplate>

                    <tr>

                        <td><%#DataBinder.Eval(Container,"DataItem.Org_SchemeName")%></td>
                        <td><%#DataBinder.Eval(Container,"DataItem.Objective")%></td>
                        <td><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.Lumpsum")))%></td>
                         <td><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.SIP")))%></td>
                        <td><%#DataBinder.Eval(Container,"DataItem.Gname")%></td>

                    </tr>


                </ItemTemplate>




                <FooterTemplate>
                    </table>
                </FooterTemplate>

            </asp:Repeater>


            </div>

     
</body>
</html>
   </asp:Content>

