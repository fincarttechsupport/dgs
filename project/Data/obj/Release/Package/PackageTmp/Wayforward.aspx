﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Wayforward.aspx.cs" Inherits="FinFinancialPlan.Wayforward" MasterPageFile="~/Planmasterpage.Master" %>

<asp:Content ID="cover" ContentPlaceHolderID="masterpagecontent" runat="server">

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>
        <title>Fincart</title>
        <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/dashboard/" />
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- Bootstrap core CSS -->
        <link href="css/planpage2.css" rel="stylesheet" />

        <!-- Custom styles for this template -->
        <link href="css/pagedashboard.css" rel="stylesheet" />
        <script src="js/session.js"></script>
        <style>
            .background {
                background-image: url('imagess/Backforplan.png');
                background-repeat: no-repeat;
                /* Full height */
                height: 100%;
                /* Center and scale the image nicely */
                background-repeat: no-repeat;
                background-size: cover;
            }

            .heading {
                text-align: left;
                color: black;
            }

            .heading2 {
                text-align: center;
                color: black;
            }


            .img {
                margin-left: 350px;
                border: 0px;
                height: 620px;
            }

            table {
                font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
                border: 1px solid #ddd;
                width: 100%;
            }

                table tr td {
                    border: 1px solid #ddd;
                        padding: 0px;
                        width:10%;
                        text-align: left;
                        color: black;
                        text-align: center;
                }


                table th {
                        border: 1px solid #ddd;
                        padding: 0px;
                        padding-top: 12px;
                        width:10%;
                        text-align: left;
                        background-color: #4CAF50;
                        color: white;
                        text-align: center;
                }
        </style>
    </head>
    <body>

        <div>
            <h1 class="heading2">WAY FORWARD</h1>

            <table>
                <tr>
                    <td>
                        <h2 class="heading">E-Emergency</h2>
                    </td>
                </tr>
            </table>

            <asp:Repeater ID="emergencyrpt" runat="server">
                <HeaderTemplate>
                    <table>
                        <tr>
                        <td>Asset Name</td>
                        <td>Amount</td>
                        <td>Monthly Investment Required</td>
                    </tr>
                            </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td style="text-align:left"><%#DataBinder.Eval(Container,"DataItem.assetname")%></td>
                            <td style="text-align:right"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.asset")))%></td>
                            <td style="text-align:right"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.surplusalloted")))%></td>
                        </tr>
                    </table>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <%--=====================RISK PROTECTION===============================--%>
            <table>
                <tr>
                    <td>
                        <h2 class="heading">R-Risk</h2>
                    </td>
                </tr>
                <tr>

                    <td>
                        <h3 class="heading">Heath Insurance</h3>
                    </td>
                </tr>
            </table>
            <asp:Repeater ID="Sugheth" runat="server">
                <HeaderTemplate>

                    <table>
                        <th>Policy Name</th>
                         <th>Client Name</th>
                        <th>Cover </th>
                        <th> Premium</th>
                    </table>


                </HeaderTemplate>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td style="text-align:left"><%#DataBinder.Eval(Container,"DataItem.AssetName")%></td>
                            <td style="text-align:center"><%#DataBinder.Eval(Container,"DataItem.clientname")%></td>
                            <td style="text-align:right"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.currValuation")))%></td>
                            <td style="text-align:right"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.PremiumAmount")))%></td>
                        </tr>
                    </table>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <%--=====================RISK PROTECTION LIFE===============================--%>
            <table>
                <tr>

                    <td>
                        <h3 class="heading">Life Insurance</h3>
                    </td>
                </tr>
            </table>
            <asp:Repeater ID="suglife" runat="server">
                <HeaderTemplate>

                    <table>

                        <th>Policy Name</th>
                        <th>Client Name</th>
                        <th>Cover </th>
                        <th> Premium</th>
                    </table>


                </HeaderTemplate>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td style="text-align:left"><%#DataBinder.Eval(Container,"DataItem.AssetName")%></td>
                            <td style="text-align:center"><%#DataBinder.Eval(Container,"DataItem.clientname")%></td>
                            <td style="text-align:right"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.currValuation")))%></td>
                            <td style="text-align:right"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.PremiumAmount")))%></td>

                        </tr>
                    </table>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <%--=====================GOALS===============================--%>
            <table>
                <tr>
                    <td>
                        <h2 class="heading">G-Goals</h2>
                    </td>
                </tr>

            </table>
            <asp:Repeater ID="Goalsrpt" runat="server">
                <HeaderTemplate>
                    <table>

                        <th>Goal Name</th>
                        <th>Asset Name</th>
                        <th>Lumpsum </th>
                         <th>Monthly Investment Required</th>

                    </table>


                </HeaderTemplate>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td style="text-align:center"><b><%#DataBinder.Eval(Container,"DataItem.Gname")%></b></td>
                            <td style="text-align:center"><%#DataBinder.Eval(Container,"DataItem.assetname")%></td>
                            <td style="text-align:right"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.asset" )))%></td>
                           
                            <td style="text-align:right"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.surplusalloted" )))%></td>
                        </tr>
                    </table>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <%--=====================Retirement===============================--%>
            <table>
                <tr>
                    <td>
                        <h2 class="heading">R-Retirement</h2>
                    </td>
                </tr>

            </table>
            <asp:Repeater ID="Ritirement" runat="server">
                <HeaderTemplate>
                    <table>

                        <th>Asset Name</th>
                        <th>Lumpsum </th>
                        <th>Monthly Investment Required</th>

                    </table>

                </HeaderTemplate>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td style="text-align:left"><%#DataBinder.Eval(Container,"DataItem.assetname")%></td>
                            <td style="text-align:right"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.asset")))%></td>
                            <td style="text-align:right"><%#formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.surplusalloted")))%></td>
                        </tr>
                    </table>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>

            <%--=====================Estate===============================--%>
            <table>
                <tr>
                    <td>
                        <h2 class="heading">E-Estate</h2>
                    </td>
                </tr>

            </table>
            <asp:Repeater ID="Estate" runat="server">
                <HeaderTemplate>
                </HeaderTemplate>
                <ItemTemplate>
                    <table>
                        <tr>
                            <td style="text-align:left"><%#DataBinder.Eval(Container,"DataItem.Retirment_age")%></td>
                        </tr>
                    </table>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>


        </div>

    </body>
    </html>
</asp:Content>
