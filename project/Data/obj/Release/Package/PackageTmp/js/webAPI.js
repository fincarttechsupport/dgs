﻿/**
* webApi.js
*/

(function ($) {
    'use strict';
    $.webApi = function (settings) {

        applyDefaultsToSettings();

        if (typeof (settings.url) == "undefined") {
            throw "Invalid $.webApi call, missing value for parameter: url.";
        }

        var data = settings.params;
            //(settings.type == 'POST') ?
            //JSON.stringify(settings.params) :
           // settings.params;

        debugger;

        $.ajax({
            beforeSend: function () {
                if (settings.showLoadingMessage == false) return;
                if (typeof ($.mobile) != "undefined") {
                    try {
                        $.mobile.showPageLoadingMsg();
                    } catch (e) {
                    }
                }
                else if (typeof (settings.loadingMessageSelector) != "undefined") {
                    $(settings.loadingMessageSelector).show();
                }
            },
            complete: function () {
                if (settings.showLoadingMessage == false) return;
                if (typeof ($.mobile) != "undefined") {
                    try {
                        $.mobile.hidePageLoadingMsg();
                    } catch (e) {
                    }
                }
                else if (typeof (settings.loadingMessageSelector) != "undefined") {
                    $(settings.loadingMessageSelector).hide();
                }
            },
            type: settings.type,
            url: settings.url,
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                if (typeof (settings.success) != "undefined") settings.success(msg);
            },
            error: function (e) {
                var message = e.status + " - " + e.statusText;
                if (e.responseText != null) {
                    try {
                        var oError = JSON.parse(e.responseText);
                        var errorDetails = (typeof (oError.ExceptionMessage) == "undefined") ? oError.Message : oError.ExceptionMessage;
                        if (message == '500 - Internal Server Error' && typeof (errorDetails) != "undefined" && errorDetails != '') {
                            message = errorDetails;
                        }
                        else {
                            message += '. ' + errorDetails;
                        }
                    }
                    catch (innerError) {
                        if (e.responseText.indexOf("<pre>") > 0) {
                            message = e.responseText.substring(e.responseText.indexOf("<pre>"));
                            message = message.substring(message.indexOf("[") + 1);
                            message = message.substring(0, message.indexOf("]"));
                        } else if (e.responseText.indexOf("\"Message\":\"") > 0) {
                            message = e.responseText.substring(e.responseText.indexOf("\"Message\":\"") + "\"Message\":\"".length, e.responseText.indexOf("\",\"Stack"));
                            message = message.replace(/\\u0027/g, "");
                        }
                    }
                }
                if (typeof (settings.error) != "undefined") settings.error(message);
                else if (typeof (settings.errorMessageSelector) != "undefined") $(settings.errorMessageSelector).text(message);
                else if (settings.showAlertOnError) alert(message);
            },
        });

        return false;

        function applyDefaultsToSettings() {
            if (typeof (settings.showAlertOnError) == "undefined") settings.showAlertOnError = true;
            if (typeof (settings.params) == "undefined") settings.params = {};
           // if (typeof (settings.preventCaching) == "undefined") settings.preventCaching = true;
            //if (settings.preventCaching == true) settings.params.webApiCacheBuster = new Date().getTime();
            if (typeof (settings.type) == "undefined") settings.type = "GET";
          //  if (typeof (settings.showLoadingMessage) == "undefined") settings.showLoadingMessage = true;
        }

    };

 

})(jQuery);