﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeBehind="mapAssets.aspx.cs" Inherits="FinFinancialPlan.mapAssets" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"/>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<link href="css/timeout-dialog.css" rel="stylesheet" />
<script src="js/timeout-dialog.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="css/repeaterstype.css" rel="stylesheet" />
    <link href="css/tabs.css" rel="stylesheet" />
    <script src="js/tabCustom.js"></script>
    <script src="js/validation.js"></script>
          <script src="js/session.js"></script>
    <script type="text/javascript">
        function invoke() {
            debugger;
            
            var ctrlAmountfv = document.getElementById("<%= lblAmountFV.ClientID %>");
            var ctrlDeficit = document.getElementById("<%= lblDeficit.ClientID %>");
            var ctrlLblmsg = document.getElementById("<%= lblMsg.ClientID %>");
            var ctrlGoals = document.getElementById("<%= ddlGoalsToFund.ClientID %>");
            var ctrlAssets = document.getElementById("<%= ddlAvailRes.ClientID %>");
            var ctrlAmountToFund = document.getElementById("<%= txtAmountToFund.ClientID %>");

            

            if (ctrlGoals.options[ctrlGoals.selectedIndex].value != "0") {
                if (ctrlAssets.options[ctrlAssets.selectedIndex].value != "0") {
                    if (ctrlAmountToFund.value != "") {
                        $.ajax({
                            method: 'POST',
                            url: 'mapAssets.aspx/GetFundAmountFV',
                            contentType: 'application/json',
                            data: '{amount:' + ctrlAmountToFund.value + ',usergoalid:' + ctrlGoals.options[ctrlGoals.selectedIndex].value + '}'


                        }).done(function (resp) {
                            debugger;
                            if (resp != null && resp.d != null) {
                                var data = resp.d;
                                data = $.parseJSON(data);
                                if (!data.error) {
                                    ctrlAmountfv.innerHTML = data.amountFV;
                                    ctrlDeficit.innerHTML = data.deficit;
                                }
                                else {
                                    ctrlLblmsg.innerHTML = data.error;
                                }
                            }

                        }).fail(function (err) {

                            document.getElementById('<%= lblMsg.ClientID %>').innerHTML = err;
                        });

                    } else { alert("please enter amount to fund..");}
                }
                else { alert("please select available asset from fund to.."); ctrlAmountToFund.value = ""; }
            }
            else { alert("please select goal to fund.."); ctrlAmountToFund.value = ""; }





        }
        
    </script>
   
</head>
<body>

<div class="container">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-1" type="button" class="btn btn-default btn-circle" disabled="disabled">1</a>
                    <p><small>Basic Details</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                    <p><small>Income</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p><small>Expense</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p><small>NetAssets Entry</small></p>
                </div>
                 <div class="stepwizard-step col-xs-2">
                    <a href="#step-5" type="button" class="btn btn-default btn-circle" disabled="disabled">5</a>
                    <p><small>Goals Summary</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-6" type="button" class="btn btn-default btn-success btn-circle" >6</a>
                    <p><small>NetAssets Allocation</small></p>
                </div>
                <div class="stepwizard-step col-xs-2">
                    <a href="#step-7" type="button" class="btn btn-default btn-circle" disabled="disabled">7</a>
                    <p><small>NetCashFlow Allocation</small></p>
                </div>
            </div>
        </div>

        
      <form id="form1" runat="server" role="form">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>

            <div class="panel panel-primary setup-content" id="step-6">
             <div class="panel-heading"><h3 class="panel-title">Available Resources Allocation</h3></div>
              
             <div class="panel-body">
                 <div class="col-lg-12">
                      
                      <asp:Label ID="lblMsg" Visible="false" Font-Size="Small" runat="server"></asp:Label>

                     <div class="col-lg-6">
                         <asp:UpdatePanel runat="server" ID="updatepanel1">
                             <ContentTemplate>

                        
                         <div class="form-group">
                            <label class="control-label">Goal to Fund</label>
                             <asp:DropDownList runat="server" ID="ddlGoalsToFund" AutoPostBack="true" OnSelectedIndexChanged="ddlGoalsToFund_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>  
                          </div>
                         <div class="form-group">
                            <label class="control-label">Asset Fund From</label>
                             <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlAvailRes" OnSelectedIndexChanged="ddlAvailRes_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>  
                          </div>

                          <div class="form-group">
                            <label class="control-label">Total Amount(in Selected Asset) :</label>
                             <asp:Label runat="server" ID="lblTotalAssetAmount" CssClass="labelBlue" Text="0"></asp:Label>  
                          </div>
                         <div class="form-group">
                            <label class="control-label">Balance Amount(in Selected Asset) :</label>
                             <asp:Label runat="server" ID="lblBalAvailResAmt" CssClass="labelBlue" Text="0"></asp:Label>  
                          </div>

                        <div class="form-group">
                            <label class="control-label">Future Cost(Selected Goal) :</label>
                             <asp:Label runat="server" ID="lblGoalFV" CssClass="labelBlue" Text="0"></asp:Label>  
                          </div>

                        <div class="form-group" runat="server" id="divLockInDuration" visible="false">
                            <label class="control-label">LockIn Duration :</label>
                             <asp:Label runat="server" ID="lblLockInDuration" CssClass="labelBlue" Text="0"></asp:Label>  
                          </div>
                        <div class="form-group" runat="server" id="divLockInInterest" visible="false">
                            <label class="control-label">LockIn Interest :</label>
                             <asp:Label runat="server" ID="lblLockInInterest" CssClass="labelBlue" Text="0"></asp:Label>  
                          </div>


                        <div class="form-group">
                            <label class="control-label">Rate of Return(% per annum)</label>
                              <asp:TextBox runat="server" CssClass="form-control" ID="txtRor" MaxLength="4" ></asp:TextBox>
                           
                          </div>
                         <div class="form-group">
                            <label class="control-label">PMT (per annum)</label>
                              <asp:TextBox runat="server" CssClass="form-control" ID="txtPMT" Text="0" MaxLength="10" ></asp:TextBox>
                           
                          </div>
                         <div class="form-group">
                            <label class="control-label">Period Type</label>
                              <asp:DropDownList ID="ddlPayPeriodType" runat="server" CssClass="form-control" >
                                  <asp:ListItem Value="0">-select-</asp:ListItem>
                                  <asp:ListItem Value="BEGIN_OF_PERIOD">Begin of Period</asp:ListItem>
                                  <asp:ListItem Value="END_OF_PERIOD">End of Period</asp:ListItem>
                              </asp:DropDownList>
                           
                          </div>
                         <div class="form-group">
                            <label class="control-label">Invest in Lumpsum(MF)</label>
                              <asp:DropDownList ID="ddlInvestToMF" runat="server" CssClass="form-control" >
                                  <asp:ListItem Value="-1">-select-</asp:ListItem>
                                  <asp:ListItem Value="1">Yes, Invest in MF</asp:ListItem>
                                  <asp:ListItem Value="0">No</asp:ListItem>
                              </asp:DropDownList>
                           
                          </div>
                         <div class="form-group">
                            <label class="control-label">Amount Allocate to Goal</label>
                              <asp:TextBox runat="server" CssClass="form-control" ID="txtAmountToFund" MaxLength="10" ></asp:TextBox>
                            <asp:Button text="Calculate" id="btnCalculate" class="btn btn-primary px-3" runat="server" OnClick="btnCalculate_Click" />
                          </div>
                           <div class="form-group">
                            <label class="control-label">Expected Amount Future Cost :</label>
                             <asp:Label runat="server" ID="lblAmountFV" CssClass="labelBlue" Text="0"></asp:Label>  
                          </div>
                        <div class="form-group">
                            <label class="control-label">Net Deficit</label>
                             <asp:Label runat="server" ID="lblDeficit" CssClass="labelBlue" Text="0"></asp:Label>  
                          </div>
                                   </ContentTemplate>
                        </asp:UpdatePanel>

                              
                       <asp:Button Text="Save" OnClick="btnSave_Click" runat="server" id ="btnSave" />
                       <asp:Button Text="Update" OnClick="btnUpdate_Click" runat="server" id ="btnUpdate" Visible="false" />
                       <asp:Button Text="Clear" OnClick="btnClear_Click" runat="server" id ="btnClear" />
                     </div>
                     <br />

                     <div class="col-lg-6" >
                         <div style="overflow-x:scroll">
                         <br />
                        <asp:Repeater ID="rptrMappedAssets" runat="server" >
                            <HeaderTemplate>
                                <table id="ulRptOverflowx">
                                    <tr>
                                       
                                            <th >AssetName</th>
                                            <th>Funded</th>
                                            <th >Goal</th>
                                            <th >Net Deficit</th>
                                            <th >Goal FV</th>
                                            <th >Lumpsum(MF)</th>
                                            <th>Delete</th>
                                        
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                     
                                         <td ><%#DataBinder.Eval(Container,"DataItem.assetname")%></td>
                                         <td style="text-align:right"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.amount")))%></td>
                                         <td><%#DataBinder.Eval(Container,"DataItem.goalName")%></td>
                                           <td style="text-align:right"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.netDeficit")))%></td>
                                          <td style="text-align:right"><%# formatter.IndianCurrency(Convert.ToString(DataBinder.Eval(Container,"DataItem.GoalFV")))%></td>
                                          <td ><%#DataBinder.Eval(Container,"DataItem.isMfPool")%></td>
                                
                                         <td><asp:LinkButton ID="lnkAssetMapDelete" Text="Delete" runat="server" OnClick="lnkAssetMapDelete_Click" OnClientClick="return confirm('Do you want to delete this row?');"  /></td>
                                        
                                         <asp:Label ID="lblId" runat="server" Text='<%# Eval("id")+"|"+ Eval("Asset_type_id")%>' Visible = "false" />
                                       
                                     
                                    </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                               </table>
                            </FooterTemplate>    
                    
                        </asp:Repeater>
                             </div>
                     </div>

                     </div>
                 
                 
                 <div class="col-lg-12">
                     &nbsp;
                 </div>
                 
                 <div class="col-lg-12">
                      <asp:Button Text="Next >>" OnClick="btnNext_Click1" runat="server" id ="btnNext" class="btn btn-primary nextBtn pull-right"  type="button"  />
                       <asp:Button Text="<< Back" runat="server" id ="prevToCashIn" OnClick="prevToCashIn_Click" class="btn btn-primary nextBtn pull-right"  type="button" />
                 </div>

             </div>
            </div>
      </form>
</div>
   
</body>
</html>
