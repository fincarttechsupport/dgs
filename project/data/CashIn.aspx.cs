﻿using data.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{
    public partial class CashIn : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
        DataTable dtCashInFlow;
      
        Dictionary<string, string> clintdic;

        public string strBasicId = string.Empty; 
        public string strUserId = string.Empty;
        public string strCreatedByEmpId = string.Empty;
        public string strPlanId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];

                strPlanId = clintdic["planid"];
                strUserId = clintdic["userid"];
                strCreatedByEmpId = clintdic["createdby"];
                strBasicId = clintdic["basicid"];

                // ** SET LAST WORKING PAGE **
                data.SetLastPageWorkingById(clintdic["basicid"], clintdic["planid"], "CashIn.aspx");
                


            } else { Response.Redirect("default.aspx"); }



            if (!IsPostBack)
            {
               
                        ddlInflowType.DataSource = data.getDropDownListByName("InflowTypeName", "inflow_id");
                        ddlInflowType.DataTextField = "key";
                        ddlInflowType.DataValueField = "value";
                        ddlInflowType.DataBind();
                        ddlInflowType.Items.Insert(0, new ListItem("--Select--", "0"));

                        ddlFrequency.DataSource = data.getDropDownListByName("Fre_Name", "fre_id");
                        ddlFrequency.DataTextField = "key";
                        ddlFrequency.DataValueField = "value";
                        ddlFrequency.DataBind();
                        ddlFrequency.Items.Insert(0, new ListItem("--Select--", "0"));

                        bindCashInFlowExist(strUserId);
                        this.addControlAttributes();

                      
            
            
            }

            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);

        }
        protected void Save_Click(object sender, EventArgs e)
        {
            
            if (!string.IsNullOrWhiteSpace(strPlanId) && !string.IsNullOrWhiteSpace(strBasicId))
            {
                bool othrFlag = false;
                bool resp = false;

                cashflowIn cflModel = new cashflowIn();
                if (txtAmount.Text != "")
                {
                    if (ddlInflowType.SelectedValue != "0")
                    {
                        if (ddlInflowType.Text == "25")
                        {
                            othrFlag = true;
                        }

                        if (ddlFrequency.SelectedValue != "0")
                        {

                            cflModel.amount = txtAmount.Text.Trim();
                            cflModel.frequency = ddlFrequency.Text;
                            cflModel.inflowType = ddlInflowType.SelectedValue;
                            cflModel.inflowTypeName = txtInflowName.Text.Trim();
                            cflModel.basicId = strBasicId;
                            cflModel.planId = strPlanId;
                            cflModel.createdBy = strCreatedByEmpId;
                            cflModel.updatedBy = strCreatedByEmpId;

                            if (othrFlag && txtInflowName.Text != "")
                            {
                                bool res = data.saveCashflowIn(cflModel);
                                if (res)
                                {
                                    bindCashInFlowExist(strUserId);
                                    showMessage("Income recorded successfully..!!", "success");

                                    this.ClearAllFields();
                                }
                                else
                                {
                                    showMessage("Error recording data..!!", "error");
                                   
                                }
                            }
                            else
                            {
                                if (!othrFlag)
                                {
                                    bool res = data.saveCashflowIn(cflModel);
                                    if (res)
                                    {
                                        bindCashInFlowExist(strUserId);
                                        showMessage("Income recorded successfully..!!", "success");
                                      
                                        this.ClearAllFields();
                                    }
                                    else
                                    {
                                        showMessage("Error recording data..!!", "error");
                                    }

                                }
                                else { showMessage("Please enter Other Expense Name","error"); }
                            }


                        }
                        else { showMessage("Please select frequency", "error");  }
                    }
                    else { showMessage("Please select Income Type", "error");  }
                }
                else { showMessage("Please enter amount", "error"); }
            }
            else { Response.Redirect("default.aspx"); }
        }
        protected void ddlInflowType_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        protected void OnEdit(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string cashflowInId = Convert.ToString((item.FindControl("lblId") as Label).Text);
            DataTable dtCashflowIn = data.getCashflowInById(cashflowInId, strBasicId,strPlanId);
            if (dtCashflowIn != null)
            {
                if (dtCashflowIn.Rows.Count > 0)
                    {
                        txtAmount.Text = Convert.ToString(dtCashflowIn.Rows[0]["income"]);
                        txtInflowName.Text = Convert.ToString(dtCashflowIn.Rows[0]["inflow_type_name"]);
                        ddlFrequency.SelectedValue = Convert.ToString(dtCashflowIn.Rows[0]["frequency"]);
                        ddlInflowType.SelectedValue = Convert.ToString(dtCashflowIn.Rows[0]["inflow_type"]);
                        txtInflowName.Text = Convert.ToString(dtCashflowIn.Rows[0]["Inflow_type_name"]);
                        
                        Session["cashflowId"] = cashflowInId;
                        btnSave.Visible = false;
                        btnUpdate.Visible = true;
                        if (ddlInflowType.SelectedValue == "25")
                        {
                            pnlOtherInflow.Visible = true;
                        }
                        

                    }
                
            }
           
        }
        protected void OnDelete(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string cashflowInId = Convert.ToString((item.FindControl("lblId") as Label).Text);
            bool res = data.removeCashflowInById(cashflowInId,strBasicId,strPlanId);
            showMessage("Income deleted successfully..!!", "success");
            
            this.bindCashInFlowExist(strUserId);
        }
        void bindCashInFlowExist(string userid)
        {
            
                dtCashInFlow = data.getAllCashFlowInByUserId(userid, strPlanId);
                rptrCashFlowIn.DataSource = dtCashInFlow;
                rptrCashFlowIn.DataBind();
            
        }
        void ClearAllFields()
        {
            txtAmount.Text = "";
            txtInflowName.Text = "";
            ddlFrequency.SelectedIndex = 0;
            ddlInflowType.SelectedIndex = 0;
            btnSave.Visible = true;
            btnUpdate.Visible = false;
        }
        void addControlAttributes()
        {
            txtAmount.Attributes.Add("onkeyup", "integersOnly(this);");
            
        }
        protected void Update_Click(object sender, EventArgs e)
        {
            bool othrFlag = false;
            bool resp = false;

            cashflowIn cflModel = new cashflowIn();
            if (txtAmount.Text != "")
            {
                if (ddlInflowType.SelectedValue != "0")
                {
                    if (ddlInflowType.Text == "25")
                    {
                        othrFlag = true;
                    }

                    if (ddlFrequency.SelectedValue != "0")
                    {

                        cflModel.amount = txtAmount.Text.Trim();
                        cflModel.frequency = ddlFrequency.Text;
                        cflModel.inflowType = ddlInflowType.SelectedValue;
                        cflModel.inflowTypeName = txtInflowName.Text.Trim();
                        cflModel.basicId = strBasicId;
                        cflModel.id = Convert.ToString(Session["cashflowId"]);
                        cflModel.updatedBy = strCreatedByEmpId;
                        cflModel.planId = strPlanId;

                        if (othrFlag && txtInflowName.Text != "")
                        {
                            bool res = data.updateCashflowInById(cflModel);
                            if (res)
                            {
                                bindCashInFlowExist(strUserId);
                                showMessage("Income updated successfully..!!", "success");
                             
                                this.ClearAllFields();
                            }
                            else
                            {
                                showMessage("Error recording data..!!", "error");
                              
                            }
                        }
                        else
                        {
                            if (!othrFlag)
                            {
                                bool res = data.updateCashflowInById(cflModel);
                                if (res)
                                {
                                    bindCashInFlowExist(strUserId);
                                    showMessage("Income updated successfully..!!", "success");
                                 
                                    this.ClearAllFields();
                                }
                                else
                                {
                                    showMessage("Error recording data..!!", "error");
                                }

                            }
                            else { showMessage("Please enter Other Income Name", "error");  }
                        }


                    }
                    else { showMessage("Please select frequency", "error"); }
                }
                else { showMessage("Please select Income Type", "error"); }
            }
            else { showMessage("Please enter amount", "error");  }
        }
        void bindPersonalExist(string userid)
        {
            dtCashInFlow = data.getAllCashFlowInByUserId(userid, strPlanId);
            rptrCashFlowIn.DataSource = dtCashInFlow;
            rptrCashFlowIn.DataBind();
        }

        protected void Nexttocashout_Click(object sender, EventArgs e)
        {
            Response.Redirect("CashOut.aspx");
        }

        protected void prevToBasicDetails_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }

        void showMessage(string msg, string type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "swal({text: '" + msg + "',icon: '" + type + "'});", true);
        }
    }
}