﻿using data.model;
using FinFinancialPlan;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.Configuration;

namespace FinFinancialPlan
{
    public partial class CashOut : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
        DataTable dtCashOutFlow;
        DataTable dtInvestments;
        
        Dictionary<string, string> clintdic;
        public string strBasicId;
        public string strUserId;
        public string strCreatedByEmpId;
        public string strPlanId;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];

                strPlanId = clintdic["planid"];
                strUserId = clintdic["userid"];
                strCreatedByEmpId = clintdic["createdby"];
                strBasicId = clintdic["basicid"];
                // ** SET LAST WORKING PAGE **
                data.SetLastPageWorkingById(clintdic["basicid"], clintdic["planid"], "CashOut.aspx");



            }
            else { Response.Redirect("default.aspx"); }

            if (!IsPostBack)
                {
                    ddlExpenseType.DataSource = data.getDropDownListByName("expensetypename", "expencetypeid");
                    ddlExpenseType.DataTextField = "key";
                    ddlExpenseType.DataValueField = "value";
                    ddlExpenseType.DataBind();
                    ddlExpenseType.Items.Insert(0, new ListItem("--Select--", "0"));
                    this.addControlAttributes();
                    this.bindCashOutFlowExist(strUserId);
                     
                }
            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);


            //Session["Reset"] = true;
            //Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            //SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            //int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            //ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);
        }

       

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool othrFlag = false;
            bool resp = false;

            CashflowOut cflModel = new CashflowOut();
            if (!string.IsNullOrWhiteSpace(strPlanId) && !string.IsNullOrWhiteSpace(strBasicId))
            {
                if (txtAmount.Text != "")
                {
                    if (ddlExpenseType.SelectedValue != "0")
                    {
                        if (ddlExpTypName.Text == "6" || ddlExpTypName.Text=="45")
                        {
                            othrFlag = true;
                        }

                        if (ddlExpTypName.SelectedValue != "0")
                        {

                            cflModel.amount = txtAmount.Text.Trim();
                            cflModel.expenseType = ddlExpenseType.SelectedValue;
                            cflModel.OutflowType = ddlExpTypName.SelectedValue;
                            cflModel.OutflowOthTypeName = txtExpTypOthName.Text.Trim();
                            cflModel.basicId = strBasicId;
                            cflModel.planId = strPlanId;
                            cflModel.createdBy = strCreatedByEmpId;
                            cflModel.updatedBy = strCreatedByEmpId;

                            if (othrFlag && txtExpTypOthName.Text != "")
                            {
                                bool res = data.saveCashflowOut(cflModel);
                                if (res)
                                {
                                    bindCashOutFlowExist(strUserId);
                                    showMessage("Expense recorded successfully..!!", "success");
                                    this.ClearAllFields();
                                }
                                else
                                {
                                    showMessage("Error recording data..!!", "error");
                                  
                                }
                            }
                            else
                            {
                                if (!othrFlag)
                                {
                                    bool res = data.saveCashflowOut(cflModel);
                                    if (res)
                                    {
                                        bindCashOutFlowExist(strUserId);
                                        showMessage("Expense recorded successfully..!!", "success");
                                       
                                        this.ClearAllFields();
                                    }
                                    else
                                    {
                                        showMessage("Error recording data..!!", "error");
                                       
                                    }

                                }
                                else { showMessage("Please enter Other Expense Name", "error"); }
                            }


                        }
                        else { showMessage("Please select Expense name", "error"); }
                    }
                    else { showMessage("Please select Expense Type", "error"); }
                }
                else { showMessage("Please enter amount", "error");   }
            }
            else { Response.Redirect("default.aspx"); }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            bool othrFlag = false;
            bool resp = false;

            CashflowOut cflModel = new CashflowOut();
            if (txtAmount.Text != "")
            {
                if (ddlExpenseType.SelectedValue != "0")
                {
                    if (ddlExpTypName.SelectedValue == "6")
                    {
                        othrFlag = true;
                    }

                    if (ddlExpTypName.SelectedValue != "0")
                    {

                        cflModel.amount = txtAmount.Text.Trim();
                        cflModel.expenseType = ddlExpenseType.SelectedValue;
                        cflModel.OutflowType = ddlExpTypName.SelectedValue;
                        cflModel.OutflowOthTypeName = txtExpTypOthName.Text.Trim();
                        cflModel.basicId = strBasicId;
                        cflModel.id = Convert.ToString(Session["cashflowId"]);
                        cflModel.updatedBy = strCreatedByEmpId;
                        cflModel.planId = strPlanId;

                        if (othrFlag && txtExpTypOthName.Text != "")
                        {
                            bool res = data.updateCashflowOutByIdBasicId(cflModel);
                            if (res)
                            {
                                bindCashOutFlowExist(strUserId);
                                showMessage("Expense updated successfully..!!", "success");
                                this.ClearAllFields();
                            }
                            else
                            {
                                showMessage("Error recording data..!!", "error");
                              
                            }
                        }
                        else
                        {
                            if (!othrFlag)
                            {
                                bool res = data.updateCashflowOutByIdBasicId(cflModel);
                                if (res)
                                {
                                    bindCashOutFlowExist(strUserId);
                                    showMessage("Expense updated successfully..!!", "success");
                                    this.ClearAllFields();
                                }
                                else
                                {
                                    showMessage("Error recording data..!!", "error");
                                }
                            }
                            else { showMessage("Please enter Other CashflowOut Name", "error"); }
                        }


                    }
                    else { showMessage("Please select Expense name", "error");  }
                }
                else { showMessage("Please select Expense Type", "error"); }
            }
            else { showMessage("Please enter amount", "error"); }
        }

        protected void lnkDelete_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string cashflowOutId = Convert.ToString((item.FindControl("lblId") as Label).Text);
            bool res = data.removeCashflowOutById(cashflowOutId, strBasicId,strPlanId);
            showMessage("Expense deleted successfully..!!", "success");
            this.bindCashOutFlowExist(strUserId);
        }

        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string cashflowOutId = Convert.ToString((item.FindControl("lblId") as Label).Text);
            DataTable dtCashflowOut = data.getCashflowOutById(cashflowOutId, strBasicId,strPlanId);
            if (dtCashflowOut != null)
            {
                if (dtCashflowOut.Rows.Count > 0)
                {
                    txtAmount.Text = Convert.ToString(dtCashflowOut.Rows[0]["expense"]);
                    txtExpTypOthName.Text = Convert.ToString(dtCashflowOut.Rows[0]["Outflow_Type_name"]);

                    ddlExpenseType.SelectedValue = Convert.ToString(dtCashflowOut.Rows[0]["Expense_type"]);
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dtCashflowOut.Rows[0]["outflow_type"])))
                    {
                        
                            ddlExpTypName.DataSource = data.getExpenseNameDdlListByType(ddlExpenseType.SelectedItem.Text);
                            ddlExpTypName.DataTextField = "key";
                            ddlExpTypName.DataValueField = "value";
                            ddlExpTypName.DataBind();
                            ddlExpTypName.Items.Insert(0, new ListItem("--Select--", "0"));
                            ddlExpTypName.SelectedValue = Convert.ToString(dtCashflowOut.Rows[0]["outflow_type"]);

                    }
                    
                    

                    Session["cashflowId"] = cashflowOutId;
                    
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                }

            }
        }

        protected void ddlExpenseType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlExpenseType.SelectedValue != "0")
            {
                ddlExpTypName.DataSource = data.getExpenseNameDdlListByType(ddlExpenseType.SelectedItem.Text);
                ddlExpTypName.DataTextField = "key";
                ddlExpTypName.DataValueField = "value";
                ddlExpTypName.DataBind();
                ddlExpTypName.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }

        protected void ddlExpTypName_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }





        // *********** CLEAR/RESET CONTROLS *********************
        void ClearAllFields()
        {
            txtAmount.Text = "";
            txtExpTypOthName.Text = "";
            ddlExpenseType.SelectedIndex = 0;
            ddlExpTypName.SelectedIndex = 0;
            btnSave.Visible = true;
            btnUpdate.Visible = false;
        }


        // *********** ALL BINDING FUNCTIONS *********************
        void bindCashOutFlowExist(string userid)
        {
            dtCashOutFlow = data.getAllCashFlowOutByUserId(userid, strPlanId);
            rptrCashFlowOut.DataSource = dtCashOutFlow;
            rptrCashFlowOut.DataBind();
        }




        protected void btnNext_Click(object sender, EventArgs e)
        {
            Response.Redirect("CashOutMap.aspx");
        }

        protected void prevToCashIn_Click(object sender, EventArgs e)
        {
            Response.Redirect("CashIn.aspx");
        }

        void addControlAttributes()
        {
            txtAmount.Attributes.Add("onkeyup", "integersOnly(this);");
            

        }
        void showMessage(string msg, string type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "swal({text: '" + msg + "',icon: '" + type + "'});", true);
        }

    }
}