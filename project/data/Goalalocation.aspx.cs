﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FinFinancialPlan;
using data.model;
using System.Data;
using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Services;
using System.Configuration;
using System.Web.Configuration;


namespace FinFinancialPlan
{

    public partial class Goalalocation : System.Web.UI.Page
    {
        DataAccess data = new DataAccess();
        DataTable dtFundAlloc;
        DataTable dtCashOutChange;
        DataTable dtGoalDeficitPmt;
        Dictionary<string, string> clintdic;
        public string strBasicId;
        public string strUserId;
        public string strCreatedByEmpId;
        public string strPlanId;


        public string strFinalSurplus = string.Empty;
        public string strSurpAllotedAmt = string.Empty;
        public string strSurpAllotedPrcnt = string.Empty;
        public float parsedFltCurrSurp = 0;
        public int parsedIntCurrSurp = 0;
        public float parsedConstSurplus = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];

                strPlanId = clintdic["planid"];
                strUserId = clintdic["userid"];
                strCreatedByEmpId = clintdic["createdby"];
                strBasicId = clintdic["basicid"];

                // ** SET LAST WORKING PAGE **
                data.SetLastPageWorkingById(clintdic["basicid"], clintdic["planid"], "GoalAlocation.aspx");
            }
            else { Response.Redirect("default.aspx"); }

            if (!IsPostBack)
            {
                ddlGoalName.DataSource = data.getDropDownListForGoals(strBasicId);
                ddlGoalName.DataTextField = "key";
                ddlGoalName.DataValueField = "value";
                ddlGoalName.DataBind();
                ddlGoalName.Items.Insert(0, new ListItem("--Select--", "0"));
                strFinalSurplus = data.getCurrentSurplus(strBasicId, strPlanId);
                lblSurplusLeft.Text = formatter.IndianCurrency(!string.IsNullOrWhiteSpace(strFinalSurplus) ? strFinalSurplus : "0");
                this.BindAllNetCashFlow();
                this.BindAllGoalDeficitPmt();
                this.BindAllCashOutForChange();
                this.GetConstantSurplus();
                this.addControlAttributes();

            }

            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);


            //Session["Reset"] = true;
            //Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            //SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            //int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            //ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);
        }

        protected void ddlGoalName_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (hdnSchemeid.Value != "")
            {
                if (ddlTrxnType.SelectedValue != "0")
                {

                    if (float.Parse(formatter.removeNumberFormating(lblSurplusLeft.Text)) > 0.0)
                    {
                        if (ddlGoalName.SelectedValue != "0")
                        {
                            if (txtAllotedSurpAmt.Text != "")
                            {
                                double allotedSurpAmt = Convert.ToDouble(txtAllotedSurpAmt.Text.Trim());
                                
                                if (ddlTrxnType.SelectedValue == "S")
                                {
                                    double totAddSipReq = Convert.ToDouble(formatter.removeNumberFormating(lblBalAddSip.Text.Trim()));

                                    if (allotedSurpAmt > totAddSipReq)
                                    {
                                        showMessage("Could not allocate more than Additional Sip required amount", "error");
                                        return;
                                    }
                                }
                                if (ddlTrxnType.SelectedValue == "L")
                                {
                                    double totLumpSumReq = Convert.ToDouble(formatter.removeNumberFormating(lblBalMarketAsset.Text.Trim()));
                                    if (allotedSurpAmt > totLumpSumReq)
                                    {
                                        showMessage("Could not allocate more than lumpsum market to invest.", "error");
                                        return;
                                    }
                                }


                                fundAllocate alloc = new fundAllocate();
                                    alloc.basicId = strBasicId;
                                    alloc.usergoalId = ddlGoalName.SelectedValue;
                                    alloc.schemeId = hdnSchemeid.Value;
                                    alloc.mfTrxnType = ddlTrxnType.SelectedValue;
                                    alloc.amount = float.Parse(txtAllotedSurpAmt.Text.Trim());
                                    alloc.futurecost = 0;
                                    alloc.createdBy = strCreatedByEmpId;
                                    alloc.planId = strPlanId;

                                    string strResp = data.saveNetCashFlowAllocation(alloc);
                                    if (!string.IsNullOrWhiteSpace(strResp))
                                    {
                                        if (float.TryParse(strResp, out parsedFltCurrSurp))
                                        {
                                            lblSurplusLeft.Text = formatter.IndianCurrency(Convert.ToString(parsedFltCurrSurp));
                                            showMessage("Fund Allocated successfully..!!", "success");
                                            this.ClearAllFields();
                                            this.BindAllNetCashFlow();
                                        }
                                    }
                               
                            }
                            else { showMessage("Please enter amount of netcashflow to allocate", "error"); }
                        }
                        else
                        {
                            showMessage("Please select goal name.", "error");
                        }
                    }
                    else { showMessage("No surplus left to allocate", "error");}

                }
                else { showMessage("please select transaction type", "error"); }

            }
            else { showMessage("please select scheme to allocate fund to", "error");  }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            string currSurplus = data.getTotalUsedSurplus(strBasicId, strPlanId);
            float surplusavail = 0;
            if (!string.IsNullOrWhiteSpace(currSurplus))
            {
                surplusavail = float.Parse(currSurplus);
                surplusavail = surplusavail + float.Parse(txtAllotedSurpAmt.Text);
            }

            if (surplusavail < float.Parse(Convert.ToString(Session["constSurplus"])))
            {
                if (txtSchemeName.Text != "" && hdnSchemeid.Value != "")
                {
                    if (ddlTrxnType.SelectedValue != "0")
                    {

                        if (float.Parse(formatter.removeNumberFormating(lblSurplusLeft.Text)) > 0.0)
                        {
                            if (ddlGoalName.SelectedValue != "0")
                            {
                                if (txtAllotedSurpAmt.Text != "")
                                {
                                    double allotedSurpAmt = Convert.ToDouble(txtAllotedSurpAmt.Text.Trim());

                                    if (ddlTrxnType.SelectedValue == "S")
                                    {
                                        double totAddSipReq = Convert.ToDouble(formatter.removeNumberFormating(lblBalAddSip.Text.Trim()));

                                        if (allotedSurpAmt > totAddSipReq)
                                        {
                                            showMessage("Could not allocate more than Additional Sip required amount", "error");
                                            return;
                                        }
                                    }
                                    if (ddlTrxnType.SelectedValue == "L")
                                    {
                                        double totLumpSumReq = Convert.ToDouble(formatter.removeNumberFormating(lblBalMarketAsset.Text.Trim()));
                                        if (allotedSurpAmt > totLumpSumReq)
                                        {
                                            showMessage("Could not allocate more than lumpsum market to invest.", "error");
                                            return;
                                        }
                                    }


                                    fundAllocate alloc = new fundAllocate();
                                    alloc.basicId = strBasicId;
                                    alloc.usergoalId = ddlGoalName.SelectedValue;
                                    alloc.schemeId = hdnSchemeid.Value;
                                    alloc.mfTrxnType = ddlTrxnType.SelectedValue;
                                    alloc.amount = float.Parse(txtAllotedSurpAmt.Text.Trim());
                                    alloc.futurecost = 0;
                                    alloc.createdBy = strCreatedByEmpId;
                                    alloc.planId = strPlanId;
                                    string strResp = data.updateNetCashFlowAllocation(alloc);
                                    if (!string.IsNullOrWhiteSpace(strResp))
                                    {
                                        if (float.TryParse(strResp, out parsedFltCurrSurp))
                                        {
                                            lblSurplusLeft.Text = formatter.IndianCurrency(Convert.ToString(parsedFltCurrSurp));
                                            showMessage("Fund updated successfully..!!", "success");
                                            this.ClearAllFields();
                                            this.BindAllNetCashFlow();
                                            this.GetCurrentSurplus();
                                        }
                                    }
                                }
                                else { showMessage("Please enter amount of netcashflow to allocate", "error"); }
                            }
                            else
                            {
                                showMessage("Please select goal name", "error");
                            }
                        }
                        else { showMessage("No surplus left to allocate.", "error");}

                    }
                    else { showMessage("please select transaction type", "error");}

                }
                else { showMessage("please select scheme to allocate fund to", "error"); }
            }
            else { showMessage("No surplus left to allocate", "error"); }
        }

        void ClearAllFields()
        {

            txtAllotedSurpAmt.Text = "";
            ddlGoalName.SelectedValue = "0";
            txtSchemeName.Text = "";
            hdnSchemeid.Value = "";
            ddlTrxnType.SelectedValue = "0";
            btnSave.Visible = true;
            btnUpdate.Visible = false;
            divSurpLeft.Visible = false;
            divSurpWas.Visible = false;
            divBalMarkedAsset.Visible = false;
            divBalAddSip.Visible = false;
            
        }
        protected double AllotedSurpPrcnt(string amount)
        {
            //***** CALC SURPLUS ALLOCATION *******
            double surpPercent = 0;
            if (!string.IsNullOrWhiteSpace(amount))
            {
                if (float.TryParse(amount, out parsedFltCurrSurp))
                {
                    if (float.TryParse(Convert.ToString(Session["constSurplus"]), out parsedConstSurplus))
                    {
                        if (parsedConstSurplus > 0)
                        {
                            surpPercent = double.Parse(Convert.ToString((parsedFltCurrSurp / parsedConstSurplus) * 100));
                            surpPercent = Math.Round(surpPercent, 2);

                        }
                    }
                }
                else { showMessage("Only decimal value allowed.", "error");  }
            }

            return surpPercent;
        }

        
        protected void lnkFndAllocEdit_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string strfundAllocId = Convert.ToString((item.FindControl("lblNetCashFlowId") as Label).Text);
            DataTable dtFundAlloc = data.getNetCashFlowAllocationById(strfundAllocId, strBasicId, strPlanId);
            if (dtFundAlloc != null)
            {
                if (dtFundAlloc.Rows.Count > 0)
                {
                    ddlGoalName.SelectedValue = Convert.ToString(dtFundAlloc.Rows[0]["userGoalId"]);
                    ddlTrxnType.SelectedValue = Convert.ToString(dtFundAlloc.Rows[0]["txnType"]);
                    txtSchemeName.Text = Convert.ToString(dtFundAlloc.Rows[0]["SchemeName"]); 
                    hdnSchemeid.Value = Convert.ToString(dtFundAlloc.Rows[0]["Scheme_Id"]); 
                    txtAllotedSurpAmt.Text = Convert.ToString(dtFundAlloc.Rows[0]["txnAmount"]);
                    toggleLabelTrxnTypeControl(Convert.ToString(dtFundAlloc.Rows[0]["txnType"]));

                    Session["fundAllocId"] = strfundAllocId;
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                }
            }
        }
        void BindAllNetCashFlow()
        {

            dtFundAlloc = data.getAllMappedNetCashflowWithSchemes(strBasicId, strPlanId);
            rptrAllocatedNetCashflow.DataSource = dtFundAlloc;
            rptrAllocatedNetCashflow.DataBind();
        }
        void BindAllCashOutForChange()
        {

            dtCashOutChange = data.getAllCashFlowOutByBasicId(strBasicId, strPlanId);
            rptrCashflowChange.DataSource = dtCashOutChange;
            rptrCashflowChange.DataBind();
        }
        void BindAllGoalDeficitPmt()
        {

            dtGoalDeficitPmt = data.getAllGoalDeficitPmtById(strBasicId, strPlanId);
            rptrGoalsDeficitPmt.DataSource = dtGoalDeficitPmt;
            rptrGoalsDeficitPmt.DataBind();
        }
        void GetConstantSurplus()
        {
            Session["constSurplus"] = data.getConstantSurplus(strBasicId, strPlanId);
            lblConstSurplus.Text = formatter.IndianCurrency(Convert.ToString(Session["constSurplus"]));
        }
        void GetCurrentSurplus()
        {
            strFinalSurplus = data.getCurrentSurplus(strBasicId, strPlanId);
            lblSurplusLeft.Text = formatter.IndianCurrency(!string.IsNullOrWhiteSpace(strFinalSurplus) ? strFinalSurplus : "0");
        }
        void toggleLabelTrxnTypeControl(string strTxnType)
        {
            if (strTxnType == "S")
            {
                divSurpLeft.Visible = true;
                divSurpWas.Visible = true;
                divBalAddSip.Visible = true;
                divBalMarkedAsset.Visible = false;
            }
            else if (strTxnType == "L")
            {
                divSurpLeft.Visible = false;
                divSurpWas.Visible = false;
                divBalAddSip.Visible = false;
                divBalMarkedAsset.Visible = true;
            }
        }

        protected void btnPlanGenerate_Click(object sender, EventArgs e)
        {
            Response.Redirect("Coverpageofplan.aspx");
        }


        protected void prevToAvailRes_Click(object sender, EventArgs e)
        {
            Response.Redirect("mapAssets.aspx");
        }

        protected void ddlTrxnType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlTrxnType.SelectedValue == "L")
            {
                lblBalMarketAsset.Text = formatter.IndianCurrency(data.getBalGoalMappedAssetInGoalById(strBasicId, ddlGoalName.SelectedValue, strPlanId));
            }
            else if (ddlTrxnType.SelectedValue == "S")
            {
                lblBalAddSip.Text = formatter.IndianCurrency(data.getBalAddSipDeficitInGoalById(strBasicId, ddlGoalName.SelectedValue, strPlanId));
            }

            toggleLabelTrxnTypeControl(ddlTrxnType.SelectedValue);
        }

        [System.Web.Services.WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string getSchemes(string text)
        {
            List<Schemes> schemeList = new List<Schemes>();
            DataAccess datalayer = new DataAccess();
            DataTable dtSchemes = datalayer.getSchemeListByTextSearch(text);

            if (dtSchemes != null)
            {
                if (dtSchemes.Rows.Count > 0)
                {
                    schemeList = dtSchemes.AsEnumerable().Select(dr => new Schemes
                    {
                        id = Convert.ToString(dr["scheme_Id"]),
                        fundId = Convert.ToString(dr["fund_id"]),
                        schemeName = Convert.ToString(dr["Org_SchemeName"])

                    }).ToList();
                }
            }
            
            return JsonConvert.SerializeObject(schemeList);
        }

        protected void lnkNetCashflowDelete_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string fundAllocId = Convert.ToString((item.FindControl("lblNetCashFlowId") as Label).Text);
            string strResp = data.removeNetCashFlowAllocationById(fundAllocId, strBasicId, strPlanId);
            if (!string.IsNullOrWhiteSpace(strResp))
            {
                if (float.TryParse(strResp, out parsedFltCurrSurp))
                {
                    lblSurplusLeft.Text = formatter.IndianCurrency(Convert.ToString(parsedFltCurrSurp));
                    showMessage("Fund Allocation deleted successfully..!!", "success");
                    this.BindAllNetCashFlow();
                    this.GetCurrentSurplus();
                }
                else
                {
                    showMessage("error converting surplus to numeric value..!!", "error");
                  
                }
            }
            else
            {
                showMessage("error in removing fund allocation entry.", "error");
             
            }
        }

        protected void lnkbtnContinue_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string strCashOutExpId = Convert.ToString((item.FindControl("lblCashOutExpId") as Label).Text);
            
            data.toggleCashOutIsContinue(strBasicId,strPlanId, strCashOutExpId,true);
            this.BindAllCashOutForChange();
            this.GetCurrentSurplus();
        }

        protected void lnkbtnDiscontinue_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string strCashOutExpId = Convert.ToString((item.FindControl("lblCashOutExpId") as Label).Text);

            data.toggleCashOutIsContinue(strBasicId, strPlanId, strCashOutExpId, false);
            this.BindAllCashOutForChange();
            this.GetCurrentSurplus();

        }

        void addControlAttributes()
        {
            txtAllotedSurpAmt.Attributes.Add("onkeyup", "integersOnly(this);");
        }
        void showMessage(string msg, string type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "swal({text: '" + msg + "',icon: '" + type + "'});", true);
        }

        protected void lblbtnEditExpense_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            this.toggleExpenseChangeControl(item, true);
        }

        protected void lnkbtnUpdateExpense_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            Int32 cashOutId = Int32.Parse((item.FindControl("lblCashOutExpId") as Label).Text);
            string amount = (item.FindControl("txtExpenseToEdit") as TextBox).Text.Trim();
            if (!string.IsNullOrWhiteSpace(amount))
            {
                bool res = data.updateCurrentExpenseAmountByID(strBasicId, strPlanId, cashOutId.ToString(), amount);
                this.BindAllCashOutForChange();
                if (res)
                {
                    showMessage("expense amount updated successfully", "success");
                }
            }
            else { 
                showMessage("please enter amount to change in cash expense", "error"); 
            }
            this.toggleExpenseChangeControl(item, false);


        }

        void toggleExpenseChangeControl(RepeaterItem item, bool isEdit)
        {
            
            item.FindControl("lblbtnEditExpense").Visible = !isEdit;
            item.FindControl("lnkbtnUpdateExpense").Visible = isEdit;
            item.FindControl("txtExpenseToEdit").Visible = isEdit;
            item.FindControl("lblExpenseAmt").Visible = !isEdit;

        }
    }
}