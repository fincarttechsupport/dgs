﻿using FinFinancialPlan;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using data.model;
using Newtonsoft.Json;
using FincartApi;
using System.Configuration;
using System.Web.Configuration;
namespace FinFinancialPlan
{
    public partial class mapAssets : System.Web.UI.Page
    {

        DataAccess data = new DataAccess();
        DataTable dtAssets;
        DataTable dtGoals;
        Dictionary<string, string> clintdic;
        public string strBasicId;
        public string strUserId;
        public string strCreatedByEmpId;
        public string strPlanId;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["clientdata"] != null)
            {

                clintdic = (Dictionary<string, string>)Session["clientdata"];

                strPlanId = clintdic["planid"];
                strUserId = clintdic["userid"];
                strCreatedByEmpId = clintdic["createdby"];
                strBasicId = clintdic["basicid"];

                // ** SET LAST WORKING PAGE **
                data.SetLastPageWorkingById(clintdic["basicid"], clintdic["planid"], "mapAssets.aspx");
                ddlPayPeriodType.SelectedValue = "BEGIN_OF_PERIOD";


            }
            else { Response.Redirect("default.aspx"); }


            if (!IsPostBack)
            {

                this.BindAllAssetsToMap();
                this.BindAllMappedAssets();
                this.addControlAttributes();

            }
            
            // SESSION POPUP
            int _displayTimeInMiliSec = (Session.Timeout - 1) * 60000;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(),
                "message",
                "<script type=\"text/javascript\" language=\"javascript\">Timer('" + _displayTimeInMiliSec + "');</script>",
                false);


            //Session["Reset"] = true;
            //Configuration config = WebConfigurationManager.OpenWebConfiguration("~/Web.Config");
            //SessionStateSection section = (SessionStateSection)config.GetSection("system.web/sessionState");
            //int timeout = (int)section.Timeout.TotalMinutes * 1000 * 60;
            //ClientScript.RegisterStartupScript(this.GetType(), "SessionAlert", "SessionExpireAlert(" + timeout + ");", true);
        }

        protected void lnkMapAsset_Click(object sender, EventArgs e)
        {

            //Reference the Repeater Item using Button.
            RepeaterItem itemParent = (sender as LinkButton).Parent as RepeaterItem;

            //Reference the DropDownList.
            DropDownList ddlGoals = itemParent.FindControl("ddlGoals") as DropDownList;

            string goalid = ddlGoals.SelectedItem.Value;

            if (goalid != "0" && !string.IsNullOrWhiteSpace(goalid))
            {
                Assets asst = new Assets();
                DataTable dtResp;

                //Find the reference of the Repeater Item.
                string strAssetId = Convert.ToString((itemParent.FindControl("lblAssetId") as Label).Text);
                string strAssetTypeId = Convert.ToString((itemParent.FindControl("lblAssetTypeId") as Label).Text);
                string strAmount = Convert.ToString((itemParent.FindControl("lblAmount") as Label).Text);

                asst.amount = strAmount;
                asst.basicId = strBasicId;
                asst.assetId = strAssetId;
                asst.assetType = strAssetTypeId;
                asst.planId = strPlanId;
                asst.usergoalId = goalid;
                asst.createdBy = strCreatedByEmpId;
                asst.updatedBy = strCreatedByEmpId;

                dtResp = data.MapAssets(asst);
                if (dtResp != null)
                {
                    this.BindAllAssetsToMap();
                    this.BindAllMappedAssets();
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Please select goal to map..!!');", true);
            }



        }

        protected void lnkAssetMapDelete_Click(object sender, EventArgs e)
        {
            //Find the reference of the Repeater Item.
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string assetMapId = Convert.ToString((item.FindControl("lblId") as Label).Text);
            string[] arrIDType = assetMapId.Split('|');
            bool res = data.removeAssetMappingById(arrIDType[0], arrIDType[1], strBasicId, strPlanId);
            
            showMessage("Asset mapping removed successfully..!!", "success");

            this.BindAllAssetsToMap();
            this.BindAllMappedAssets();
            resetControls();
        }


        void BindAllAssetsToMap()
        {
            //dtAssets = data.getAllAssetsById(strBasicId, strPlanId);


            ddlAvailRes.DataSource = data.getDropDownListForAvailRes(strBasicId);
            ddlAvailRes.DataTextField = "key";
            ddlAvailRes.DataValueField = "value";
            ddlAvailRes.DataBind();
            ddlAvailRes.Items.Insert(0, new ListItem("--Select--", "0"));

            ddlGoalsToFund.DataSource = data.getDropDownListForGoals(strBasicId);
            ddlGoalsToFund.DataTextField = "key";
            ddlGoalsToFund.DataValueField = "value";
            ddlGoalsToFund.DataBind();
            ddlGoalsToFund.Items.Insert(0, new ListItem("--Select--", "0"));


        }
        void BindAllMappedAssets()
        {
            dtAssets = data.getAllMappedAssetsById(strBasicId, strPlanId);
            rptrMappedAssets.DataSource = dtAssets;
            rptrMappedAssets.DataBind();

        }
        void showMessage(string msg, string type)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "Popup", "swal({text: '"+msg+"',icon: '"+type+"'});", true);
        }
        protected void rptrAssetsList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Find the DropDownList in the Repeater Item.
                DropDownList ddlGoals = (e.Item.FindControl("ddlGoals") as DropDownList);
                ddlGoals.DataSource = data.getUserGoalsByUserid(strUserId);
                ddlGoals.DataTextField = "key";
                ddlGoals.DataValueField = "value";
                ddlGoals.DataBind();

                //Add Default Item in the DropDownList.
                ddlGoals.Items.Insert(0, new ListItem("--Select--", "0"));


            }
        }


        protected void btnNext_Click1(object sender, EventArgs e)
        {
            Response.Redirect("Goalalocation.aspx");
        }

        protected void prevToCashIn_Click(object sender, EventArgs e)
        {
            Response.Redirect("goalsToFund.aspx");
        }

        protected void ddlAvailRes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(strPlanId) && !string.IsNullOrWhiteSpace(strBasicId))
            {
                if (ddlAvailRes.SelectedValue != "0")
                {

                    string strAssetId = ddlAvailRes.SelectedValue;
                    DataTable dtBalAmt = data.getAvailResBalanceAmount(strBasicId, strPlanId, strAssetId);
                    if (dtBalAmt.Rows.Count > 0)
                    {
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(dtBalAmt.Rows[0]["lockInDuration"]))
                            && !string.IsNullOrWhiteSpace(Convert.ToString(dtBalAmt.Rows[0]["lockInInterest"])))
                        {
                            lblLockInDuration.Text = Convert.ToString(dtBalAmt.Rows[0]["lockInDuration"]) + " Year";
                            lblLockInInterest.Text = Convert.ToString(dtBalAmt.Rows[0]["lockInInterest"]) + "%";
                            divLockInDuration.Visible = true;
                            divLockInInterest.Visible = true;
                        }
                        else 
                        {
                            divLockInDuration.Visible = false;
                            divLockInInterest.Visible = false;
                        }

                        lblTotalAssetAmount.Text = formatter.IndianCurrency(Convert.ToString(dtBalAmt.Rows[0]["amount"]));
                        lblBalAvailResAmt.Text = formatter.IndianCurrency(Convert.ToString(dtBalAmt.Rows[0]["balAmount"]));
                    }
                    


                }
            }

            // CLEAR WHEN USER CHANGE ASSET TYPE
            this.ClearFVAndDeficit();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            float amtToAlloc = 0;
            float amtBal = 0;

            if (ddlAvailRes.SelectedValue != "0")
            {
                if (ddlGoalsToFund.SelectedValue != "0")
                {
                    if (txtRor.Text.Trim() != "")
                    {
                        if (txtAmountToFund.Text.Trim() != string.Empty)
                        {
                            if (ddlInvestToMF.SelectedValue != "-1")
                            {
                                if (float.TryParse(txtAmountToFund.Text.Trim(), out amtToAlloc))
                                {
                                    float.TryParse(formatter.removeNumberFormating(lblBalAvailResAmt.Text.Trim()), out amtBal);

                                    if (amtToAlloc <= amtBal)
                                    {
                                        if (formatter.removeNumberFormating(lblAmountFV.Text.Trim()) != "0")
                                        {
                                            Assets asst = new Assets();
                                            DataTable dtResp;

                                            // SPLIT ASSETID & TYPE ID
                                            string[] arrAssetID = ddlAvailRes.SelectedValue.Split('|');

                                            string strAssetId = arrAssetID[0];
                                            string strAssetTypeId = arrAssetID[1];
                                            string strAmount = txtAmountToFund.Text.Trim();

                                            asst.amount = strAmount;
                                            asst.futureCost = formatter.removeNumberFormating(lblAmountFV.Text);
                                            asst.ror = txtRor.Text.Trim();
                                            asst.pmt = txtPMT.Text.Trim();
                                            asst.isMfPool = ddlInvestToMF.SelectedValue;
                                            asst.payPeriod = ddlPayPeriodType.SelectedValue;
                                            asst.deficit = formatter.removeNumberFormating(lblDeficit.Text);
                                            asst.basicId = strBasicId;
                                            asst.assetId = strAssetId;
                                            asst.assetType = strAssetTypeId;
                                            asst.planId = strPlanId;
                                            asst.usergoalId = ddlGoalsToFund.SelectedValue;
                                            asst.createdBy = strCreatedByEmpId;
                                            asst.updatedBy = strCreatedByEmpId;

                                            dtResp = data.MapAssets(asst);
                                            if (dtResp != null)
                                            {
                                                BindAllMappedAssets();
                                                resetControls();
                                                
                                                showMessage("Amount allocated successfully..!!", "success");

                                            }
                                        }
                                        else { Response.Write("<script>alert('please calculate deficit by clicking calculate button first.');</script>"); }
                                    }
                                    else { Response.Write("<script>alert('could not allocate more than balance amount');</script>"); }

                                }
                                else { Response.Write("<script>alert('Please enter valid amount to fund goal.');</script>"); }

                            }
                            else { Response.Write("<script>alert('Please select invest to mutual fund or not');</script>"); }

                        }
                        else { Response.Write("<script>alert('Please enter amount to fund goal.');</script>"); }
                    }
                    else { Response.Write("<script>alert('Please enter rate of return.');</script>"); }

                }
                else { Response.Write("<script>alert('Please select goal to fund.');</script>"); }
            }
            else { Response.Write("<script>alert('Please select available resource.');</script>"); }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            float amtToAlloc = 0;
            float amtBal = 0;

            if (ddlAvailRes.SelectedValue != "0")
            {
                if (ddlGoalsToFund.SelectedValue != "0")
                {
                    if (txtRor.Text.Trim() != "")
                    {
                        if (txtAmountToFund.Text.Trim() != string.Empty)
                        {
                            if (ddlInvestToMF.SelectedValue != "-1")
                            {
                                if (float.TryParse(txtAmountToFund.Text.Trim(), out amtToAlloc))
                                {
                                    float.TryParse(formatter.removeNumberFormating(lblBalAvailResAmt.Text.Trim()), out amtBal);

                                    if (amtToAlloc <= amtBal)
                                    {
                                        if (formatter.removeNumberFormating(lblAmountFV.Text.Trim()) != "0")
                                        {
                                            Assets asst = new Assets();
                                            DataTable dtResp;

                                            // SPLIT ASSETID & TYPE ID
                                            string[] arrAssetID = ddlAvailRes.SelectedValue.Split('|');

                                            string strAssetId = arrAssetID[0];
                                            string strAssetTypeId = arrAssetID[1];
                                            string strAmount = txtAmountToFund.Text.Trim();

                                            asst.amount = strAmount;
                                            asst.futureCost = formatter.removeNumberFormating(lblAmountFV.Text);
                                            asst.ror = txtRor.Text.Trim();
                                            asst.pmt = txtPMT.Text.Trim();
                                            asst.isMfPool = ddlInvestToMF.SelectedValue;
                                            asst.payPeriod = ddlPayPeriodType.SelectedValue;
                                            asst.deficit = formatter.removeNumberFormating(lblDeficit.Text);
                                            asst.basicId = strBasicId;
                                            asst.assetId = strAssetId;
                                            asst.assetType = strAssetTypeId;
                                            asst.planId = strPlanId;
                                            asst.usergoalId = ddlGoalsToFund.SelectedValue;
                                            asst.createdBy = strCreatedByEmpId;
                                            asst.updatedBy = strCreatedByEmpId;

                                            dtResp = data.MapAssets(asst);
                                            if (dtResp != null)
                                            {
                                                BindAllMappedAssets();
                                                resetControls();
                                                
                                                showMessage("Amount allocation updated successfully..!!", "success");
                                            }
                                        }
                                        else { Response.Write("<script>alert('please calculate deficit by clicking calculate button first.');</script>"); }
                                    }
                                    else { Response.Write("<script>alert('could not allocate more than balance amount');</script>"); }
                                }
                                else { Response.Write("<script>alert('Please enter valid amount to fund goal.');</script>"); }
                            }
                            else { Response.Write("<script>alert('Please select invest to mutual fund or not');</script>"); }
                        }
                        else { Response.Write("<script>alert('Please enter amount to fund goal.');</script>"); }
                    }
                    else { Response.Write("<script>alert('Please enter rate of return.');</script>"); }

                }
                else { Response.Write("<script>alert('Please select goal to fund.');</script>"); }
            }
            else { Response.Write("<script>alert('Please select available resource.');</script>"); }
        }

        protected void ddlGoalsToFund_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlGoalsToFund.SelectedValue != "0")
            {
                dtGoals = data.getGoalDetailsByGoalID(ddlGoalsToFund.SelectedValue);
                if (dtGoals.Rows.Count > 0)
                {
                    Session["goalData"] = dtGoals;
                    lblGoalFV.Text = formatter.IndianCurrency(Convert.ToString(dtGoals.Rows[0]["getAmount"]));
                    lblDeficit.Text = formatter.IndianCurrency(Convert.ToString(data.getGoalNetDeficitByGoalID(ddlGoalsToFund.SelectedValue, strBasicId, strPlanId)));
                }
            }

            // CLEAR WHEN USER CHANGE GOAL
            this.ClearFVAndDeficit();
        }

        public void GetFundAmountFV(string amount, string usergoalid, string ror, string strPmt, string strPeriodType,string assetID)
        {
            string strAmountFV = string.Empty;
            string strDeficit = string.Empty;
            string strMapAssetFV = string.Empty;
            string strError = string.Empty;
            DataAccess dataCaller = new DataAccess();
            DataTable dtLockInAsset;
            Financial calc = new Financial();

            if (amount != string.Empty)
            {
                
                    if (HttpContext.Current.Session["goalData"] != null && HttpContext.Current.Session["clientdata"] != null)
                    {
                        Dictionary<string, string> clintdic;
                        //strPlanId = clintdic["planid"];
                        //strUserId = clintdic["userid"];
                        //strCreatedByEmpId = clintdic["createdby"];
                        //strBasicId = clintdic["basicid"];
                        clintdic = (Dictionary<string, string>)HttpContext.Current.Session["clientdata"];
                        DataTable dtGoals = (DataTable)HttpContext.Current.Session["goalData"];
                        string strDuration = Convert.ToString(dtGoals.Rows[0]["Duration"]);
                        string strGoalFV = Convert.ToString(dtGoals.Rows[0]["getAmount"]);
                        string strLockedInAssetFV = string.Empty;

                        double lockDuration = 0;
                        double rate = 0;
                        double nper = 0;
                        double pmt = 0;
                        double pv = 0;
                        double type = 0;

                        int years;

                        rate = double.Parse(ror);
                        pmt = double.Parse(strPmt);
                        if (strPeriodType == "BEGIN_OF_PERIOD")
                        { type = 1; }
                        else { type = 0; }


                        // ****** CALC FV *******

                        if (strDuration != string.Empty)
                        {
                            int i;
                            double result = 0;

                            if (strDuration == "0")
                            {
                                result = Convert.ToDouble(amount);
                            }
                            else
                            {
                                dtLockInAsset = data.CheckGetLockInAssetFutureValue(strBasicId, assetID.Split('|')[0], amount);
                                if (dtLockInAsset != null)
                                {
                                    if (dtLockInAsset.Rows.Count > 0)
                                    {
                                        strLockedInAssetFV = formatter.removeNumberFormating(Convert.ToString(dtLockInAsset.Rows[0]["finalAmount"]));
                                        lockDuration = double.Parse(Convert.ToString(dtLockInAsset.Rows[0]["duration"]));
                                    }
                                }

                                result = double.Parse(data.getFutureValueFromDB(rate, double.Parse(strDuration) - lockDuration, pmt, Convert.ToDouble(strLockedInAssetFV) * -1, type));
                                //result = Math.Round(Convert.ToDouble(amount) * Math.Pow((1 + rate / 100.0), Convert.ToInt32(strDuration)), 0);
                            }

                            strAmountFV = Convert.ToString(result);

                        }
                        if (strGoalFV != string.Empty && strAmountFV != string.Empty)
                        {
                            strMapAssetFV = dataCaller.getTotalMappedAssetFVByGoalID(usergoalid, clintdic["basicid"], clintdic["planid"]);

                            strDeficit = Convert.ToString(Math.Round(double.Parse(strGoalFV) - double.Parse(strAmountFV) - double.Parse(strMapAssetFV), 0));
                        }
                    }
                    else { strError = "no goal found"; }
                
            }
            else 
            {

            }

            lblAmountFV.Text = formatter.IndianCurrency(strAmountFV);
            lblDeficit.Text = formatter.IndianCurrency(strDeficit);
            //return JsonConvert.SerializeObject(new { amountFV = strAmountFV, deficit = strDeficit, error= strError });

        }
        void resetControls()
        {
            txtAmountToFund.Text = "";
            ddlAvailRes.SelectedValue = "0";
            ddlGoalsToFund.SelectedValue = "0";
            lblBalAvailResAmt.Text = "0";
            lblTotalAssetAmount.Text = "0";
            lblAmountFV.Text = "0";
            lblDeficit.Text = "0";
            lblGoalFV.Text = "0";
            txtRor.Text = "";
            txtPMT.Text = "0";
            ddlInvestToMF.SelectedValue = "-1";
            ddlPayPeriodType.SelectedValue = "BEGIN_OF_PERIOD";
            divLockInDuration.Visible = false;
            divLockInInterest.Visible = false;
        }
        void ClearFVAndDeficit()
        {
            txtAmountToFund.Text = formatter.IndianCurrency("0");
            lblDeficit.Text = formatter.IndianCurrency("0");
            lblAmountFV.Text = formatter.IndianCurrency("0");
        }
        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            if (txtRor.Text != "")
            {
                if (txtPMT.Text.Trim() != string.Empty)
                {
                    if (txtAmountToFund.Text != "")
                    {
                        if (ddlGoalsToFund.SelectedValue != "0")
                        {
                            if (ddlAvailRes.SelectedValue != "0")
                            {
                                if (ddlPayPeriodType.SelectedValue != "0")
                                {

                                    if (float.Parse(formatter.removeNumberFormating(lblBalAvailResAmt.Text)) > 0)
                                    {
                                        if (float.Parse(formatter.removeNumberFormating(lblBalAvailResAmt.Text)) >= float.Parse(formatter.removeNumberFormating(txtAmountToFund.Text)))
                                        {
                                            this.GetFundAmountFV(txtAmountToFund.Text.Trim(), ddlGoalsToFund.SelectedValue,
                                                txtRor.Text.Trim(), txtPMT.Text.Trim(), ddlPayPeriodType.SelectedValue, ddlAvailRes.SelectedValue);
                                        }
                                        else { ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('No overlimit fund allotment allowed.')", true); }

                                    }
                                    else { ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('no balance amount left to allocate or calculate')", true); }
                                }
                                else { ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('please select period type to calculate fund future value')", true); }
                            }
                            else { ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('please select available asset.')", true); }
                        }
                        else { ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('please select goal.')", true); }
                    }
                    else { ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('please enter amount to fund.')", true); }
                }
                else { showMessage("please enter 0 or negative amount in pmt", "error"); }
            }
            else { ScriptManager.RegisterStartupScript(this, this.GetType(), "alertMessage", "alert('please enter rate of return')", true); }

        }

        protected void lnkAssetMapEdit_Click(object sender, EventArgs e)
        {
            RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
            string assetMapId = Convert.ToString((item.FindControl("lblId") as Label).Text);
            string[] arrIDType = assetMapId.Split('|');

            DataTable dtFundAlloc = data.getMappedAssetsById(arrIDType[0], arrIDType[1], strBasicId, strPlanId);

            if (dtFundAlloc != null)
            {
                if (dtFundAlloc.Rows.Count > 0)
                {
                    ddlGoalsToFund.SelectedValue = Convert.ToString(dtFundAlloc.Rows[0]["userGoalId"]);
                    ddlAvailRes.SelectedValue = Convert.ToString(dtFundAlloc.Rows[0]["id"]) + "|" + Convert.ToString(dtFundAlloc.Rows[0]["Asset_id"]);
                    txtRor.Text = Convert.ToString(dtFundAlloc.Rows[0]["ror"]);
                    txtAmountToFund.Text = Convert.ToString(dtFundAlloc.Rows[0]["amount"]);
                    lblAmountFV.Text = formatter.IndianCurrency(Convert.ToString(dtFundAlloc.Rows[0]["futureCost"]));
                    lblDeficit.Text = formatter.IndianCurrency(Convert.ToString(dtFundAlloc.Rows[0]["deficit"]));
                    lblTotalAssetAmount.Text = formatter.IndianCurrency(Convert.ToString(dtFundAlloc.Rows[0]["assetTotamt"]));
                    lblBalAvailResAmt.Text = formatter.IndianCurrency(Convert.ToString(dtFundAlloc.Rows[0]["assetBalAmt"]));
                    
                    Session["fundAllocId"] = arrIDType[0];
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                }
            }
        }

        void addControlAttributes()
        {
            txtAmountToFund.Attributes.Add("onkeyup", "integersOnly(this);");
            txtPMT.Attributes.Add("onkeyup", "integersOnly(this);");
            txtRor.Attributes.Add("onkeyup", "integersOnly(this);");
           
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            resetControls();
        }
    }
}
